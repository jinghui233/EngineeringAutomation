import numpy as np
class ArcBase:
    def __init__(self, start, end, center, radius, direction):
        self._start = start
        self._end = end
        self._center=center
        self._radius = radius
        self._direction=direction
        self._I = None
        self._J = None
        self._start_angle = None
        self._end_angle = None
        self._Arc_angle = None

    def _changed(self):
        self._I = None
        self._J = None
        self._start_angle = None
        self._end_angle = None
        self._Arc_angle = None

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, value):
        self._changed()
        self._start = value

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        self._changed()
        self._end = value

    @property
    def radius(self):
        return self._radius

    @property
    def center(self):
        return self._center

    @center.setter
    def center(self, value):
        self._changed()
        self._center = value

    @property
    def direction(self):
        return self._direction
    @property
    def I(self):
        if self._I is None:
            self._I = self.center[0]-self.start[0]
        return self._I

    @property
    def J(self):
        if self._J is None:
            self._J = self.center[1]-self.start[1]
        return self._J

    def angle_between(self,n1, n2):
        cos_angle = n1.dot(n2) / np.linalg.norm(n1) / np.linalg.norm(n2)
        k = np.sign(np.cross(n1, n2))
        angle = k * np.arccos(cos_angle)
        return angle

    @property
    def start_angle(self):
        if self._start_angle is None:
            n0 = np.array([1, 0])
            n1 = np.array([self._center[0] - self._start[0], self._center[1] - self._start[1]])
            self._start_angle = self.angle_between(n0, n1)
        return self._start_angle

    @property
    def end_angle(self):
        if self._end_angle is None:
            n0 = np.array([1, 0])
            n2 = np.array([self._center[0] - self._end[0], self._center[1] - self._end[1]])
            self._end_angle = self.angle_between(n0, n2)
        return self._end_angle

    @property
    def Arc_angle(self):
        if self._Arc_angle is None:
            n1 = np.array([self._center[0] - self._start[0], self._center[1] - self._start[1]])
            n2 = np.array([self._center[0] - self._end[0], self._center[1] - self._end[1]])
            angle = self.angle_between(n1, n2)
            if np.sign(angle)>=0:Fx='counterclockwise'
            else:Fx='clockwise'
            if Fx==self._direction:self._Arc_angle=angle
            else:self._Arc_angle=3.141592-abs(angle)

        return self._Arc_angle