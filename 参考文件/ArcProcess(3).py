import random
import scipy as sp
import matplotlib.pyplot as plt
from numpy import sqrt
from numpy.linalg import norm
from scipy import optimize
import numpy as np
import gerber
import math
from ProcessService.RoutLineProcess.GKOGerberProcess2.LinePiceSet import LinePice as LinePice_sd, LineSet as LineSet_sd
import copy
from gerber.gerber_statements import CoordStmt
from ProcessService.RoutLineProcess.GKOGerberProcess_new.ArcSet import ArcBase
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
class ArcProcess:
    def __init__(self, gong_lines):
        self.gong_lines = gong_lines
        self.arclines=[]
        self.LinesSet=[]
        self.Arclines=[]
        self.Arc_list=[]
        self.otherlines=[]

    def PreProc(self):
        self.Find_arclines(self.gong_lines)
        self.Lineclassify(self.arclines)
        self.CutArc1(self.LinesSet)
        self.TextArc_1(self.LinesSet)
        self.CutArc2(self.LinesSet)
        self.TextArc_1(self.LinesSet)
        # self.CutArc3(self.LinesSet)
        self.TextArc_1(self.LinesSet)
        self.addArclines()
        self.combineSet()
        self.TransArc(self.Arclines)
        self.combineArc(self.Arc_list)
        self.CollectOtherlines()



    def gradient(self,p1, p2):
        if p2[0] - p1[0] == 0:
            return 100
        else:
            return (p2[1] - p1[1]) / (p2[0] - p1[0])

    def dist(self,p1, p2):
        if p1 != None and p2 != None:
            return ((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2) ** (0.5)
        else:
            return 0

    def fortheta(self,a, b):
        a /= norm(a)
        b /= norm(b)
        # 夹角cos值
        cos_ = np.dot(a, b) / (norm(a) * norm(b))
        # 夹角sin值
        sin_ = np.cross(a, b) / (norm(a) * norm(b))
        arctan2_ = np.arctan2(sin_, cos_)
        return arctan2_

    def Find_arclines(self,gong_lines):
        # print(gong_lines)
        for line in gong_lines:
            if abs(self.gradient(line[0], line[1])) >= 100 or abs(self.gradient(line[0], line[1])) <= 0.01:
                l_n = 0.04
            else:
                l_n = 0.04
            # l_n = 0.06
            if 0.001 < math.sqrt(
                    (line[1][1] - line[0][1]) ** 2 + (line[1][0] - line[0][0]) ** 2) < l_n:
                self.arclines.append(line)
            elif l_n <= math.sqrt(
                (line[1][1] - line[0][1]) ** 2 + (line[1][0] - line[0][0]) ** 2):
                self.otherlines.append(line)

    def Lineclassify(self,lines):
        j = 1
        NewLineList = lines
        LinesSet = []
        while NewLineList != []:
            # for i in range(k):
            set = []
            PointsList = []
            all_nodes = []
            PointsList.append(NewLineList[0][0])
            PointsList.append(NewLineList[0][1])
            all_nodes.append(NewLineList[0][0])
            all_nodes.append(NewLineList[0][1])
            # print(NewLineList[0])
            set.append(NewLineList[0])

            a = 0.0039
            # pp=0
            while j < len(NewLineList):
                # print(i,j)
                if self.dist(PointsList[0], NewLineList[j][0]) < a:
                    PointsList[0] = NewLineList[j][1]
                    set_first = [[NewLineList[j][1], NewLineList[j][0], NewLineList[j][2], NewLineList[j][3]]]
                    set_first.extend(set)
                    set = set_first
                    NewLineList.pop(j)
                    j = 1
                elif self.dist(PointsList[0], NewLineList[j][1]) < a:
                    PointsList[0] = NewLineList[j][0]
                    set_first = [NewLineList[j]]
                    set_first.extend(set)
                    set = set_first
                    NewLineList.pop(j)
                    j = 1
                # print(0)
                elif self.dist(PointsList[1], NewLineList[j][0]) < a:
                    PointsList[1] = NewLineList[j][1]
                    set.append(NewLineList[j])
                    NewLineList.pop(j)
                    j = 1
                elif self.dist(PointsList[1], NewLineList[j][1]) < a:
                    PointsList[1] = NewLineList[j][0]
                    set.append([NewLineList[j][1], NewLineList[j][0], NewLineList[j][2], NewLineList[j][3]])
                    NewLineList.pop(j)
                    j = 1
                else:
                    j += 1
            j = 1
            LinesSet.append(set)
            if NewLineList != []: NewLineList.pop(0)
        for lines in LinesSet:
            if len(lines)==2 and lines[0][0]==lines[1][1]:
                # print(lines)
                lines.remove(lines[1])
                # print(lines)
        self.LinesSet=LinesSet

    def CutArc1(self,LinesSet):
        Set_1 = []
        for Lines in LinesSet:
            if len(Lines) < 3:
                Set_1.append(Lines)
                continue
            Arc = []
            Arc.append(Lines[0])
            for i in range(1, len(Lines)):
                n_i = np.array([Lines[i][1][0]-Lines[i][0][0],Lines[i][1][1]-Lines[i][0][1]])
                n_c = np.array([Lines[i-1][1][0]-Lines[i-1][0][0],Lines[i-1][1][1]-Lines[i-1][0][1]])
                # print(Lines[i-1],Lines[i],n_c,n_i,sum(n_i.T*n_c)<0)

                if sum(n_i.T*n_c)<0:
                    Set_1.append(Arc)
                    Arc = []
                    Arc.append(Lines[i])
                else:
                    Arc.append(Lines[i])
            if Arc != []: Set_1.append(Arc)
        self.LinesSet = Set_1

    def TextArc_1(self,LinesSet):
        Arclines_0 = copy.deepcopy(LinesSet)
        i = 0
        for lines in Arclines_0:
            i += 1
            if len(lines) == 1:
                continue
            elif len(lines) >= 3:
                Xi = []
                Yi = []
                for line in lines:
                    Xi.append(line[0][0])
                    Xi.append(line[1][0])
                    Yi.append(line[0][1])
                    Yi.append(line[1][1])
                x_m = np.mean(Xi)
                y_m = np.mean(Yi)
                def calc_R(xc, yc):
                    return sqrt((Xi - xc) ** 2 + (Yi - yc) ** 2)
                def f_2(c):
                    Ri = calc_R(*c)
                    return Ri - Ri.mean()
                center_estimate = x_m, y_m
                center_2, _ = optimize.leastsq(f_2, center_estimate)
                xc_2, yc_2 = center_2
                center = [xc_2, yc_2]
                Ri_2 = calc_R(xc_2, yc_2)
                # 拟合圆的半径
                R_2 = Ri_2.mean()
                residu_2 = sum((Ri_2 - R_2) ** 2)
                if residu_2 < 0.00005:#误差
                    self.Arclines.append(lines)
                    LinesSet.remove(lines)
        self.LinesSet=LinesSet
    def CutArc2(self,LinesSet):
        Arclines = []
        for Lines in LinesSet:
            if len(Lines) <= 3:
                Arclines.append(Lines)
                continue
            Arc = []
            Arc.append(Lines[0])
            # Arc.append(Lines[1])
            # k_1 = self.gradient(Lines[0][0], Lines[0][1])
            # k_2 = self.gradient(Lines[1][0], Lines[1][1])
            # gradient_l = k_2 - k_1
            # k_c = k_2
            for i in range(1, len(Lines)):
                k_i = self.gradient(Lines[i][0], Lines[i][1])
                n_i = np.array([Lines[i][1][0] - Lines[i][0][0], Lines[i][1][1] - Lines[i][0][1]])
                n_c = np.array([Lines[i - 1][1][0] - Lines[i - 1][0][0], Lines[i - 1][1][1] - Lines[i - 1][0][1]])
                n_f = np.array([Lines[i - 2][1][0] - Lines[i - 2][0][0], Lines[i - 2][1][1] - Lines[i - 2][0][1]])
                # if gradient_l == None:
                #     gradient_l = k_i - k_c
                if np.cross(n_f, n_c) * np.cross(n_c, n_i) < 0 \
                        or self.dist(Lines[i][0], Lines[i][1]) < self.dist(Lines[i - 1][0], Lines[i - 1][1]) / 2 \
                        or self.dist(Lines[i][0], Lines[i][1]) > self.dist(Lines[i - 1][0], Lines[i - 1][1]) * 2:
                    Arclines.append(Arc)
                    Arc = []
                    # gradient_l = None
                    k_c = k_i
                    Arc.append(Lines[i])
                    i = i + 1
                else:
                    Arc.append(Lines[i])
                    # gradient_l = k_i - k_c
                    k_c = k_i
            if Arc != []: Arclines.append(Arc)
        self.LinesSet = Arclines

    def CutArc3(self,LinesSet):
        Arclines = []
        for Lines in LinesSet:
            if len(Lines) <= 3:
                Arclines.append(Lines)
                continue
            Arc = []
            Arc.append(Lines[0])
            Arc.append(Lines[1])
            k_1 = self.gradient(Lines[0][0], Lines[0][1])
            k_2 = self.gradient(Lines[1][0], Lines[1][1])
            gradient_l = k_2 - k_1
            k_c = k_2
            for i in range(2, len(Lines)):
                k_i = self.gradient(Lines[i][0], Lines[i][1])
                n_i = np.array([Lines[i][1][0] - Lines[i][0][0], Lines[i][1][1] - Lines[i][0][1]])
                n_c = np.array([Lines[i - 1][1][0] - Lines[i - 1][0][0], Lines[i - 1][1][1] - Lines[i - 1][0][1]])
                n_f = np.array([Lines[i - 2][1][0] - Lines[i - 2][0][0], Lines[i - 2][1][1] - Lines[i - 2][0][1]])
                if gradient_l == None:
                    gradient_l = k_i - k_c
                if abs(k_i - k_c - gradient_l) > 2 and (k_i - k_c)*gradient_l>0:
                    print(100)
                    Arclines.append(Arc)
                    Arc = []
                    gradient_l = None
                    k_c = k_i
                    Arc.append(Lines[i])
                    i = i + 1
                else:

                    Arc.append(Lines[i])
                    gradient_l = k_i - k_c
                    k_c = k_i
            if Arc != []: Arclines.append(Arc)
        self.LinesSet = Arclines


    def addArclines(self):
        for lines in self.LinesSet:
            if len(lines) < 3:
                self.Arclines.append(lines)
                self.LinesSet.remove(lines)

    def combineSet(self):
        for set1 in self.Arclines:
            if len(set1) < 3:continue
            for set2 in self.Arclines:
                if set1==set2 :continue
                elif len(set2) == 1:
                    Set = []
                    if self.dist(set2[0][0],set1[0][0])<0.02 and self.dist(set2[0][0],set1[0][0])<self.dist(set2[0][1],set1[0][0]):
                        n1=np.array([set2[0][1][0]-set2[0][0][0],set2[0][1][1]-set2[0][0][1]])
                        n2=np.array([set1[0][1][0]-set1[0][0][0],set1[0][1][1]-set1[0][0][1]])
                        if n1.dot(n2) > 0 or 1-abs(n1.dot(n2) / np.linalg.norm(n1) / np.linalg.norm(n2))>0.01:continue
                        Set = [[set2[0][1],set2[0][0],set2[0][2],set2[0][3]]]
                        Set.extend(set1)
                    elif self.dist(set2[0][1],set1[0][0])<0.02 and self.dist(set2[0][1],set1[0][0])<self.dist(set2[0][0],set1[0][0]):
                        n1 = np.array([set2[0][1][0] - set2[0][0][0], set2[0][1][1] - set2[0][0][1]])
                        n2 = np.array([set1[0][1][0] - set1[0][0][0], set1[0][1][1] - set1[0][0][1]])
                        if n1.dot(n2) < 0 or 1-abs(n1.dot(n2) / np.linalg.norm(n1) / np.linalg.norm(n2))>0.01: continue
                        Set = copy.deepcopy(set2)
                        Set.extend(set1)
                    elif self.dist(set2[0][0],set1[-1][1])<0.02 and self.dist(set2[0][0],set1[-1][1])<self.dist(set2[0][1],set1[-1][1]):
                        n1 = np.array([set2[0][1][0] - set2[0][0][0], set2[0][1][1] - set2[0][0][1]])
                        n2 = np.array([set1[-1][1][0]-set1[-1][0][0] , set1[-1][1][1]-set1[-1][0][1]])
                        if n1.dot(n2) < 0 or 1-abs(n1.dot(n2) / np.linalg.norm(n1) / np.linalg.norm(n2))>0.01: continue
                        Set = copy.deepcopy(set1)
                        Set.extend(set2)
                    elif self.dist(set2[0][1], set1[-1][1]) < 0.02 and self.dist(set2[0][1], set1[-1][1]) < 0.02<self.dist(set2[0][0], set1[-1][1]) < 0.02:
                        n1 = np.array([set2[0][1][0] - set2[0][0][0], set2[0][1][1] - set2[0][0][1]])
                        n2 = np.array([set1[-1][1][0]-set1[-1][0][0], set1[-1][1][1]-set1[-1][0][1]])
                        if n1.dot(n2) > 0 or 1-abs(n1.dot(n2) / np.linalg.norm(n1) / np.linalg.norm(n2))>0.01: continue
                        Set = copy.deepcopy(set1)
                        Set.extend([[set2[0][1], set2[0][0], set2[0][2], set2[0][3]]])
                    if len(Set) == 1 or Set==[]:
                        continue
                    elif len(Set) >= 3:
                        Xi = []
                        Yi = []
                        for line in Set:
                            Xi.append(line[0][0])
                            Xi.append(line[1][0])
                            Yi.append(line[0][1])
                            Yi.append(line[1][1])
                        x_m = np.mean(Xi)
                        y_m = np.mean(Yi)
                        def calc_R(xc, yc):
                            return sqrt((Xi - xc) ** 2 + (Yi - yc) ** 2)
                        def f_2(c):
                            Ri = calc_R(*c)
                            return Ri - Ri.mean()
                        center_estimate = x_m, y_m
                        center_2, _ = optimize.leastsq(f_2, center_estimate)
                        xc_2, yc_2 = center_2
                        center = [xc_2, yc_2]
                        Ri_2 = calc_R(xc_2, yc_2)
                        # 拟合圆的半径
                        R_2 = Ri_2.mean()
                        residu_2 = sum((Ri_2 - R_2) ** 2)
                        if residu_2 < 0.00002 and len(Set)>len(set1):
                            # print(Set)
                            # print(1000,set1)
                            # set1=Set
                            self.Arclines.append(Set)
                            self.Arclines.remove(set1)
                            self.Arclines.remove(set2)
                            # print(len(set2))
                            break
    def TransArc(self,Arclines1):
        for lines in Arclines1:
            if len(lines) == 1:
                Arc = ArcBase(lines[0][0], lines[0][1], None, None, None)  # continue
                self.Arc_list.append(Arc)

            elif len(lines) >= 2:
                Xi = []
                Yi = []
                for line in lines:
                    Xi.append(line[0][0])
                    Xi.append(line[1][0])
                    Yi.append(line[0][1])
                    Yi.append(line[1][1])
                x_m = np.mean(Xi)
                y_m = np.mean(Yi)

                def calc_R(xc, yc):
                    return sqrt((Xi - xc) ** 2 + (Yi - yc) ** 2)

                def f_2(c):
                    Ri = calc_R(*c)
                    return Ri - Ri.mean()

                center_estimate = x_m, y_m
                center_2, _ = optimize.leastsq(f_2, center_estimate)
                xc_2, yc_2 = center_2
                center = [xc_2, yc_2]
                if self.dist(lines[0][0], lines[1][1]) > self.dist(lines[0][1], lines[1][1]):
                    start = lines[0][0]
                else:
                    start = lines[0][1]
                if self.dist(lines[-1][0], lines[-2][1]) > self.dist(lines[-1][1], lines[-2][1]):
                    end = lines[-1][0]
                else:
                    end = lines[-1][1]
                if lines[1][1] != start and lines[1][1] != end:
                    datum_point = lines[1][1]
                else:
                    datum_point = lines[1][0]
                if len(lines) > 10:
                    datum_point = lines[5][1]
                n0=np.array([1,0])
                n1 = np.array([center[0] - start[0], center[1] - start[1]])
                n2 = np.array([center[0] - end[0], center[1] - end[1]])
                n3 = np.array([center[0] - datum_point[0], center[1] - datum_point[1]])

                # print(((n1[0]*n2[1]-n1[1]*n2[0])<0 and (n1[0]*n3[1]-n1[1]*n3[0])<0))
                if ((n1[0] * n2[1] - n1[1] * n2[0]) <= 0 and (n1[0] * n3[1] - n1[1] * n3[0]) <= 0) \
                        or ((n1[0] * n2[1] - n1[1] * n2[0]) >= 0 and (n1[0] * n3[1] - n1[1] * n3[0]) <= 0):
                    direction = 'clockwise'
                else:
                    direction = 'counterclockwise'
                radius = self.dist(center, start)
                # print(datum_point[0],datum_point[1])
                # print((start, end, center, radius, direction))
                Arc = ArcBase(start, end, center, radius, direction)
                self.Arc_list.append(Arc)

    # def combineArc(self,Arc_list):
    #     for Arc_1 in Arc_list:
    #         nn = 0.005
    #         # print(Arc_1.start, Arc_1.end)
    #         for Arc_2 in Arc_list:
    #             if Arc_1 == Arc_2 or [Arc_1.start, Arc_1.end] == [Arc_2.end, Arc_2.start] or self.dist(Arc_2.start, Arc_2.end) > self.dist(Arc_1.start, Arc_1.end): continue
    #             if Arc_1.center == None or self.dist(Arc_1.start, Arc_1.end) < self.dist(Arc_2.start, Arc_2.end):
    #                 continue
    #             elif Arc_2.center == None:continue
    #             #     if self.dist(Arc_1.end, Arc_2.start) < nn:
    #             #         if self.dist(Arc_1.start, Arc_2.end) > self.dist(Arc_1.start, Arc_1.end):
    #             #             Arc_1.end = Arc_2.end
    #             #         else:
    #             #             Arc_1.end = Arc_2.start
    #             #         Arc_list.remove(Arc_2)
    #             #     elif self.dist(Arc_2.end, Arc_1.start) < nn:
    #             #         if   self.dist(Arc_1.end, Arc_1.start)>self.dist(Arc_1.end, Arc_2.start):
    #             #             Arc_1.start = Arc_2.end
    #             #         else:
    #             #             Arc_1.start = Arc_2.start
    #             #         Arc_list.remove(Arc_2)
    #             #     elif self.dist(Arc_1.start, Arc_2.start) < nn:
    #             #         if self.dist(Arc_1.end, Arc_2.end) > self.dist(Arc_1.start, Arc_1.end):
    #             #             Arc_1.start = Arc_2.end
    #             #         else:
    #             #             Arc_1.start = Arc_2.start
    #             #         Arc_list.remove(Arc_2)
    #             #     elif self.dist(Arc_2.end, Arc_1.end) < nn:
    #             #         if self.dist(Arc_1.start, Arc_2.start) > self.dist(Arc_1.start, Arc_1.end):
    #             #             Arc_1.end = Arc_2.start
    #             #         else:
    #             #             Arc_1.end = Arc_1.end
    #             #         Arc_list.remove(Arc_2)
    #             #     continue
    #             # print((Arc_1.center,Arc_2.center,Arc_1.radius))
    #             if self.dist(Arc_1.center, Arc_2.center) < (Arc_1.radius + Arc_2.radius) / 4 and self.dist(Arc_1.start,
    #                                                                                              Arc_1.end) > self.dist(
    #                     Arc_2.start, Arc_2.end):
    #                 # print(Arc_1.start, Arc_1.end, Arc_2.start, Arc_2.end)
    #                 mm = 0.02
    #                 if self.dist(Arc_1.end, Arc_2.start) < mm:
    #                     if self.dist(Arc_1.start, Arc_2.end) > self.dist(Arc_1.start, Arc_2.start):
    #                         Arc_1.end = Arc_2.end
    #                     else:
    #                         Arc_1.end = Arc_2.start
    #                     Arc_1.center = [(Arc_1.center[0] + Arc_2.center[0]) / 2,
    #                                     (Arc_1.center[1] + Arc_2.center[1]) / 2]
    #                     Arc_list.remove(Arc_2)
    #                 elif self.dist(Arc_2.end, Arc_1.start) < mm:
    #                     # Arc_2.end = Arc_1.start
    #                     if self.dist(Arc_1.start, Arc_2.end) > self.dist(Arc_1.start, Arc_2.start):
    #                         Arc_1.start = Arc_2.end
    #                     else:
    #                         Arc_1.start = Arc_2.start
    #                     # Arc_1.start=Arc_2.start
    #                     Arc_1.center = [(Arc_1.center[0] + Arc_2.center[0]) / 2,
    #                                     (Arc_1.center[1] + Arc_2.center[1]) / 2]
    #                     Arc_list.remove(Arc_2)
    #                 elif self.dist(Arc_1.start, Arc_2.start) < mm:
    #                     if self.dist(Arc_1.start, Arc_2.end) > self.dist(Arc_1.start, Arc_2.start):
    #                         Arc_1.start = Arc_2.end
    #                     else:
    #                         Arc_1.start = Arc_2.start
    #                     # Arc_1.start=Arc_2.end
    #                     Arc_1.center = [(Arc_1.center[0] + Arc_2.center[0]) / 2,
    #                                     (Arc_1.center[1] + Arc_2.center[1]) / 2]
    #                     Arc_list.remove(Arc_2)
    #                 elif self.dist(Arc_2.end, Arc_1.end) < mm:
    #                     # Arc_1.end = Arc_2.start
    #                     if self.dist(Arc_1.start, Arc_2.end) > self.dist(Arc_1.start, Arc_2.start):
    #                         Arc_1.end = Arc_2.end
    #                     else:
    #                         Arc_1.end = Arc_2.start
    #                     Arc_1.center = [(Arc_1.center[0] + Arc_2.center[0]) / 2,
    #                                     (Arc_1.center[1] + Arc_2.center[1]) / 2]
    #                     Arc_list.remove(Arc_2)
    #         for arc in Arc_list:
    #             if arc.center == None:
    #                 self.LinesSet.append([[arc.start, arc.end]])
    def combineArc(self,Arc_list):
        for Arc_1 in Arc_list:
            nn = 0.005
            # print(Arc_1.start, Arc_1.end)
            for Arc_2 in Arc_list:
                if Arc_1 == Arc_2 or [Arc_1.start, Arc_1.end] == [Arc_2.end, Arc_2.start] or self.dist(Arc_2.start, Arc_2.end) > self.dist(Arc_1.start, Arc_1.end): continue
                if Arc_1.center == None or self.dist(Arc_1.start, Arc_1.end) < self.dist(Arc_2.start, Arc_2.end):
                    continue
                elif Arc_2.center == None:continue
                if self.dist(Arc_1.center, Arc_2.center) < (Arc_1.radius + Arc_2.radius) / 4 and abs(Arc_1.Arc_angle)>abs(Arc_2.Arc_angle):
                    # print(Arc_1.start, Arc_1.end, Arc_2.start, Arc_2.end)
                    mm = 0.02

                    if self.dist(Arc_1.end, Arc_2.start) < mm:
                        Arc_1.center = [(Arc_1.center[0] + Arc_2.center[0]) / 2,(Arc_1.center[1] + Arc_2.center[1]) / 2]
                        n1 = np.array([Arc_1.start[0]-Arc_1.center[0],Arc_1.start[1]-Arc_1.center[1]])
                        n2 = np.array([Arc_1.end[0] - Arc_1.center[0], Arc_1.end[1] - Arc_1.center[1]])
                        n3 = np.array([Arc_2.end[0] - Arc_1.center[0], Arc_2.end[1] - Arc_1.center[1]])
                        if np.cross(n2,n1)*np.cross(n2,n3)<0:
                            Arc_1.end = Arc_2.end
                        else:
                            Arc_1.end = Arc_2.start

                        Arc_list.remove(Arc_2)
                    elif self.dist(Arc_2.end, Arc_1.start) < mm:
                        Arc_1.center = [(Arc_1.center[0] + Arc_2.center[0]) / 2,(Arc_1.center[1] + Arc_2.center[1]) / 2]
                        n1 = np.array([Arc_1.start[0] - Arc_1.center[0], Arc_1.start[1] - Arc_1.center[1]])
                        n2 = np.array([Arc_1.end[0] - Arc_1.center[0], Arc_1.end[1] - Arc_1.center[1]])
                        n3 = np.array([Arc_2.start[0] - Arc_1.center[0], Arc_2.start[1] - Arc_1.center[1]])
                        if np.cross(n1,n2)*np.cross(n1,n3)<0:
                            Arc_1.start = Arc_2.start
                        else:
                            Arc_1.start = Arc_2.end
                        Arc_list.remove(Arc_2)
                    elif self.dist(Arc_1.start, Arc_2.start) < mm:
                        Arc_1.center = [(Arc_1.center[0] + Arc_2.center[0]) / 2,(Arc_1.center[1] + Arc_2.center[1]) / 2]
                        n1 = np.array([Arc_1.start[0] - Arc_1.center[0], Arc_1.start[1] - Arc_1.center[1]])
                        n2 = np.array([Arc_1.end[0] - Arc_1.center[0], Arc_1.end[1] - Arc_1.center[1]])
                        n3 = np.array([Arc_2.end[0] - Arc_1.center[0], Arc_2.end[1] - Arc_1.center[1]])
                        if np.cross(n1,n2)*np.cross(n1,n3)<0:
                            Arc_1.start = Arc_2.end
                        else:
                            Arc_1.start = Arc_2.start
                        Arc_list.remove(Arc_2)
                    elif self.dist(Arc_2.end, Arc_1.end) < mm:
                        Arc_1.center = [(Arc_1.center[0] + Arc_2.center[0]) / 2,(Arc_1.center[1] + Arc_2.center[1]) / 2]
                        n1 = np.array([Arc_1.start[0] - Arc_1.center[0], Arc_1.start[1] - Arc_1.center[1]])
                        n2 = np.array([Arc_1.end[0] - Arc_1.center[0], Arc_1.end[1] - Arc_1.center[1]])
                        n3 = np.array([Arc_2.start[0] - Arc_1.center[0], Arc_2.start[1] - Arc_1.center[1]])
                        if np.cross(n2,n1)*np.cross(n2,n3)<0:
                            Arc_1.end = Arc_2.start
                        else:
                            Arc_1.end = Arc_2.end

                        Arc_list.remove(Arc_2)
            for arc in Arc_list:
                if arc.center == None:
                    self.LinesSet.append([[arc.start, arc.end]])




    def CollectOtherlines(self):

        self.otherlines=[self.otherlines]
        self.otherlines+=self.LinesSet



def ToGerberFile(sets):
    lineSets = sets
    writestr = "*\n%FSLAX26Y26*%\n%MOIN*%\n%ADD10C,0.007874*%\n%IPPOS*%\n%LNgko11.gbr*%\n%LPD*%\nG75*\nG54D10*\n"
    for lines in lineSets:
        for line in lines:
            # print(lines[0][1] * 10 ** 6)
            # print(int(lines[0][1] * 10 ** 6))
            line_start_X = str(int(line[0][0] * 10 ** 6))
            line_start_Y = str(int(line[0][1] * 10 ** 6))
            line_end_X = str(int(line[1][0] * 10 ** 6))
            line_end_Y = str(int(line[1][1] * 10 ** 6))
            writestr += 'X' + line_start_X + 'Y' + line_start_Y + 'D02*' \
                        + 'X' + line_end_X + 'Y' + line_end_Y + 'D01*\n'
    # print(writestr)
    return writestr
def ToGerber(Arclist):
    writestr = "*\n%FSLAX26Y26*%\n%MOIN*%\n%SFA1.000B1.000*%\n%MIA0B0*%\n%IPPOS*%\n%ADD91C,0.007874*%\n%LNgko1*%\n%LPD*%\nG54D91*\nG75*\n"
    for Arc in Arclist:
    # Arc=Arclist[45]
    # if 1:
        if Arc.center==None:continue
        Arc_start_X = str(int(Arc.start[0] * 10 ** 6))
        Arc_start_Y = str(int(Arc.start[1] * 10 ** 6))
        Arc_end_X = str(int(Arc.end[0] * 10 ** 6))
        Arc_end_Y = str(int(Arc.end[1] * 10 ** 6))
        Arc_end_I = str(int(Arc.I * 10 ** 6))
        Arc_end_J = str(int(Arc.J * 10 ** 6))
        if Arc.direction == 'clockwise':direction='G02'
        else:direction='G03'
        writestr+='X'+Arc_start_X+'Y'+Arc_start_Y+'D02*\n'\
                  +direction+'X'+Arc_end_X+'Y'+Arc_end_Y+'I'+Arc_end_I+'J'+Arc_end_J+'D01*\n'
    writestr+='M02*\nG74*'

    return writestr
# path = r"C:\Users\ZH\Desktop\槽连线\jp-2w2447732"
# with open(f"{path}\gko", "r") as f4:  # JP-2W2097778#JP-1W1747554 #JP-1w2449161#JP-2W1664147#JP-2W2184263#JP-2W2184929#JP-2W2317889#jp-2w2447243
#     # with open(f"C:\\Users\\ZH\Desktop\gko1", "r") as f4:
#     data4 = f4.read()
#     gerberLayer_gko = gerber.loads(data4, 'gko')
path = r"C:\Users\ZH\Desktop\槽连线"#ALL-4C2096202
drs = os.listdir(path)
# v = 0

for v in range(67,68):
    with open("{}\\{}\\gko".format(path, drs[v]), "r") as f4:
        data4 = f4.read()
        gerberLayer_gko = gerber.loads(data4, 'gko')
    all_lines = []
    arc_lines = []
    long_lines = []
    i = 0
    C = gerberLayer_gko.size[1]
    lenth_arc = C / 3 * 0.03
    for primitive in gerberLayer_gko.primitives:
        if type(primitive) == gerber.primitives.Line:
            all_line = [primitive.start, primitive.end, primitive.angle, i]
            all_lines.append(all_line)
            i += 1

    ArcProcess1=ArcProcess(all_lines)
    ArcProcess1.PreProc()
    # print(len(ArcProcess.Arc_list))



    writestr1=ToGerber(ArcProcess1.Arc_list)
    writestr2=ToGerberFile(ArcProcess1.otherlines)
    writestr=writestr1+writestr2
    with open("{}\\{}\\rout3".format(path, drs[v]), 'w') as f:
        f.write(writestr)



