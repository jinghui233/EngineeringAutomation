import gerber
import math


def drl_find(gerberLayer_drl):
    new_circles = []
    circles = []
    circles_r = []
    circles_core_points = []
    for drl in gerberLayer_drl.primitives:
        if type(drl) == gerber.primitives.Circle:
            circles.append([drl.position, drl.radius])
            circles_r.append(drl.radius)
            circles_core_points.append(drl.position)
    for circle1 in circles:
        m = 0
        for circle2 in circles:
            if (circle1 != circle2) and (
                    (math.sqrt((circle1[0][1] - circle2[0][1]) ** 2 + (circle1[0][0] - circle2[0][0]) ** 2)) < 4 * min(
                    circle1[1], circle2[1])):
                m = 1
        if m == 0:
            circle1.append('q')
        else:
            circle1.append('l')
    for i in range((len(circles) - 1), -1, -1):
        if circles[i][-1] == 'q':
            circles.pop(i)
        else:
            circles[i].pop()
    for circle in circles:
        circle.append('o')
    u = []
    for i in range(len(circles)):
        u.append(i)
    a = 1
    label = 0
    while a == 1:
        for i in u:
            for j in range(len(circles)):
                if type(circles[i][-1]) != int and type(circles[j][-1]) != int:
                    if (circles[i] != circles[j]) and (math.sqrt((circles[i][0][1] - circles[j][0][1]) ** 2 + (
                            circles[i][0][0] - circles[j][0][0]) ** 2) < 4 * min(circles[i][1], circles[j][1])):
                        circles[i].append(label)
                        circles[j].append(label)
                        continue
                elif type(circles[i][-1]) == int and type(circles[j][-1]) != int:
                    if (circles[i] != circles[j]) and (math.sqrt((circles[i][0][1] - circles[j][0][1]) ** 2 + (
                            circles[i][0][0] - circles[j][0][0]) ** 2) < 4 * min(circles[i][1], circles[j][1])):
                        circles[j].append(circles[i][-1])
                        continue
            label += 1
        labs = []
        for circle in circles:
            labs.append(circle[-1])
        if 'o' in labs:
            a = 1
        else:
            a = 0
    labels = []
    for circle in circles:
        labels.append(circle[-1])
    labels = list(set(labels))
    for lab in labels:
        z_circles = []
        for circle in circles:
            if circle[-1] == lab:
                z_circles.append(circle)
        z_x = []
        z_y = []
        for z in z_circles:
            z_x.append(z[0][0])
            z_y.append(z[0][1])
        for x in range((len(z_x) - 1), 0, -1):
            if abs(z_x[x] - z_x[x - 1]) < 0.01:
                z_x.pop(x)
        for y in range((len(z_y) - 1), 0, -1):
            if abs(z_y[y] - z_y[y - 1]) < 0.01:
                z_y.pop(y)
        if len(z_x) > 3:
            # print(1)
            for z in range((len(z_circles) - 1), -1, -1):
                if z_circles[z][0][0] == min(z_x) or z_circles[z][0][0] == max(z_x):
                    z_circles.pop(z)
        if len(z_y) > 3:
            # print(2)
            for z in range((len(z_circles) - 1), -1, -1):
                if z_circles[z][0][1] == min(z_y) or z_circles[z][0][1] == max(z_y):
                    z_circles.pop(z)
        for z in z_circles:
            new_circles.append(z)

    return new_circles