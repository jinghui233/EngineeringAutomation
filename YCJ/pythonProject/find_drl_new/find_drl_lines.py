import gerber
import math
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

with open(f"E:\\GerberFile\\ALL-1W2308512\\drl", "r") as f6:
    data6 = f6.read()
    gerberLayer_drl = gerber.loads(data6, 'drl')


def drl_find(gerberLayer_drl):
    new_circles = []
    circles = []
    circles_r = []
    circles_core_points = []
    for drl in gerberLayer_drl.primitives:
        if type(drl) == gerber.primitives.Circle:
            circles.append([drl.position, drl.radius])
            circles_r.append(drl.radius)
            circles_core_points.append(drl.position)
    for circle1 in circles:
        m = 0
        for circle2 in circles:
            if (circle1 != circle2) and ((math.sqrt((circle1[0][1] - circle2[0][1]) ** 2 + (circle1[0][0] - circle2[0][0]) ** 2)) < 4 * min(circle1[1], circle2[1])):
                m = 1
        if m == 0:
            circle1.append('q')
        else:
            circle1.append('l')
    for i in range((len(circles) - 1), -1, -1):
        if circles[i][-1] == 'q':
            circles.pop(i)
        else:
            circles[i].pop()
    for circle in circles:
        circle.append('o')
    u = []
    for i in range(len(circles)):
        u.append(i)
    a = 1
    label = 0
    while a == 1:
        for i in u:
            for j in range(len(circles)):
                if type(circles[i][-1]) != int and type(circles[j][-1]) != int:
                    if (circles[i] != circles[j]) and (math.sqrt((circles[i][0][1] - circles[j][0][1]) ** 2 + (circles[i][0][0] - circles[j][0][0]) ** 2) < 4 * min(circles[i][1], circles[j][1])):
                        circles[i].append(label)
                        circles[j].append(label)
                        continue
                elif type(circles[i][-1]) == int and type(circles[j][-1]) != int:
                    if (circles[i] != circles[j]) and (math.sqrt((circles[i][0][1] - circles[j][0][1]) ** 2 + (circles[i][0][0] - circles[j][0][0]) ** 2) < 4 * min(circles[i][1], circles[j][1])):
                        circles[j].append(circles[i][-1])
                        continue
            label += 1
        labs = []
        for circle in circles:
            labs.append(circle[-1])
        if 'o' in labs:
            a = 1
        else:
            a = 0
    labels1 = []
    for circle in circles:
        labels1.append(circle[-1])
    labels1 = list(set(labels1))
    lab_circles = []
    for lab in labels1:
        z_circles = []
        for circle in circles:
            if circle[-1] == lab:
                z_circles.append(circle)
        lab_circles.append(z_circles)
    for i in range(len(lab_circles) - 1, -1, -1):
        if len(lab_circles[i]) > 1:
            pass
        else:
            lab_circles.pop(i)
    labels = []
    for i in range(len(lab_circles)):
        for j in range(len(lab_circles[i])):
            labels.append(lab_circles[i][j][-1])
    labels = list(set(labels))
    for lab in labels:
        z_circles = []
        for circle in circles:
            if circle[-1] == lab:
                z_circles.append(circle)
        z_x = []
        z_y = []
        for z in z_circles:
            z_x.append(z[0][0])
            z_y.append(z[0][1])
        for x in range((len(z_x) - 1), 0, -1):
            if abs(z_x[x] - z_x[x - 1]) < 0.01:
                z_x.pop(x)
        for y in range((len(z_y) - 1), 0, -1):
            if abs(z_y[y] - z_y[y - 1]) < 0.01:
                z_y.pop(y)
        if len(z_x) > 3:
            # print(1)
            for z in range((len(z_circles) - 1), -1, -1):
                if z_circles[z][0][0] == min(z_x) or z_circles[z][0][0] == max(z_x):
                    z_circles.pop(z)
        if len(z_y) > 3:
            # print(2)
            for z in range((len(z_circles) - 1), -1, -1):
                if z_circles[z][0][1] == min(z_y) or z_circles[z][0][1] == max(z_y):
                    z_circles.pop(z)
        for z in z_circles:
            new_circles.append(z)

    return new_circles


new_circles = drl_find(gerberLayer_drl)


def draw():
    for circle in new_circles:
        glBegin(GL_LINE_STRIP)
        for nc in range(20):
            glColor3f(0.0, 0.0, 1.0)
            xc = circle[0][0] + circle[1] * math.cos(2 * nc * math.pi / 20)
            yc = circle[0][1] + circle[1] * math.sin(2 * nc * math.pi / 20)
            glVertex2f(xc, yc)
        glEnd()

    glFlush()


show_x_min = (gerberLayer_drl.bounds[0][0] - 0.5)
show_x_max = (gerberLayer_drl.bounds[0][1] + 0.5)
show_y_min = (gerberLayer_drl.bounds[1][0] - 0.5)
show_y_max = (gerberLayer_drl.bounds[1][1] + 0.5)

if __name__ == '__main__':
    glutInit(sys.argv)  # 初始化
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)  # 设置显示模式
    glutInitWindowPosition(0, 0)  # 窗口打开的位置，左上角坐标在屏幕坐标
    glutInitWindowSize(800, 800)  # 窗口大小
    glutCreateWindow(b"Function Plotter")  # 窗口名字，二进制
    glutDisplayFunc(draw)  # 设置当前窗口的显示回调
    glClearColor(1.0, 1.0, 1.0, 1.0)  # 设置背景颜色
    gluOrtho2D(show_x_min, show_x_max, show_y_min, show_y_max)  # 设置显示范围
    glutMainLoop()  # 启动循环'''
