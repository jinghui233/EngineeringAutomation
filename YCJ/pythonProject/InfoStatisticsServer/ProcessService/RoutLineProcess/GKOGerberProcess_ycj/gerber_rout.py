import os
import gerber
from YCJ.pythonProject.InfoStatisticsServer.ProcessService.RoutLineProcess.GKOGerberProcess_ycj.new_find import GKOGerberProcess


class GkoToRout:
    def __init__(self, gong_lines):
        self.gong_lines = gong_lines

    def data(self, sets):
        writestr = "*\n%FSLAX26Y26*%\n%MOIN*%\n%ADD10C,0.007874*%\n%IPPOS*%\n%LNgko11.gbr*%\n%LPD*%\nG75*\nG54D10*\n"
        for lines in sets:
            for line in lines:
                line_start_X = str(int(line[0][0] * 10 ** 6))
                line_start_Y = str(int(line[0][1] * 10 ** 6))
                line_end_X = str(int(line[1][0] * 10 ** 6))
                line_end_Y = str(int(line[1][1] * 10 ** 6))
                writestr += 'X' + line_start_X + 'Y' + line_start_Y + 'D02*' + 'X' + line_end_X + 'Y' + line_end_Y + 'D01*\n'
        return writestr


if __name__ == '__main__':
    path = r"E:\GerberFile"
    drs = os.listdir(path)
    for v in range(len(drs)):
        print("v=", v)
        if 'gtl' in os.listdir("{}\\{}".format(path, drs[v])):
            with open("{}\\{}\\gtl".format(path, drs[v]), "r") as f1:
                data1 = f1.read()
                gerberLayer_gtl = gerber.loads(data1, 'gtl')
        else:
            gerberLayer_gtl = None
        with open("{}\\{}\\gko".format(path, drs[v]), "r") as f4:
            data4 = f4.read()
            gerberLayer_gko = gerber.loads(data4, 'gko')
        with open("{}\\{}\\drl".format(path, drs[v]), "r") as f6:
            data6 = f6.read()
            gerberLayer_drl = gerber.loads(data6, 'drl')
        findlines = GKOGerberProcess(gerberLayer_gko, gerberLayer_gtl, gerberLayer_drl)
        findlines.run()
        gong_lines, level_lines, vertical_lines, circles, writestr = findlines.gong_lines, findlines.level_lines, findlines.vertical_lines, findlines.circles, findlines.writestr
        with open("{}\\{}\\rout".format(path, drs[v]), "w") as f8:
            f8.write(writestr)
