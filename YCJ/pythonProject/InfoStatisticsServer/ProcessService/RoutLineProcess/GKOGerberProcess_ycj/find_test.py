import gerber
import math
import gerber
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import sys
import copy
import os
from YCJ.pythonProject.InfoStatisticsServer.ProcessService.RoutLineProcess.GKOGerberProcess_ycj.find import GKOGerberProcess


def draw():
    for line in all_lines:
        glColor3f(1.0, 0.0, 0.0)
        glBegin(GL_LINE_STRIP)
        glVertex2f(line[0][0], line[0][1])
        glVertex2f(line[1][0], line[1][1])
        glEnd()
    # for line in vertical_lines:
    #     glColor3f(1.0, 1.0, 1.0)
    #     glBegin(GL_LINE_STRIP)
    #     glVertex2f(line[0][0], line[0][1])
    #     glVertex2f(line[1][0], line[1][1])
    #     glEnd()
    # for line in level_lines:
    #     glColor3f(0.0, 1.0, 0.0)
    #     glBegin(GL_LINE_STRIP)
    #     glVertex2f(line[0][0], line[0][1])
    #     glVertex2f(line[1][0], line[1][1])
    #     glEnd()
    # for line in other_lines:
    #     glColor3f(1.0, 0.0, 0.0)
    #     glBegin(GL_LINE_STRIP)
    #     glVertex2f(line[0][0], line[0][1])
    #     glVertex2f(line[1][0], line[1][1])
    #     glEnd()
    # for node in NodesList:
    #     glPointSize(4)
    #     glColor3f(0.0, 0.0, 1.0)
    #     glBegin(GL_POINTS)
    #     glVertex2f(node[2], node[3])
    #     glEnd()
    glFlush()


def show(gerberLayer_gko):
    show_x_min = (gerberLayer_gko.bounds[0][0] - 0.5)
    show_x_max = (gerberLayer_gko.bounds[0][1] + 0.5)
    show_y_min = (gerberLayer_gko.bounds[1][0] - 0.5)
    show_y_max = (gerberLayer_gko.bounds[1][1] + 0.5)
    return show_x_min, show_x_max, show_y_min, show_y_max


if __name__ == "__main__":
    path = r"E:\False"
    drs = os.listdir(path)
    v = 0
    if 'gtl' in os.listdir("{}\\{}".format(path, drs[v])):
        with open("{}\\{}\\gtl".format(path, drs[v]), "r") as f1:
            data1 = f1.read()
            gerberLayer_gtl = gerber.loads(data1, 'gtl')
    else:
        gerberLayer_gtl = None
    with open("{}\\{}\\gko".format(path, drs[v]), "r") as f4:
        data4 = f4.read()
        gerberLayer_gko = gerber.loads(data4, 'gko')
    with open("{}\\{}\\drl".format(path, drs[v]), "r") as f6:
        data6 = f6.read()
        gerberLayer_drl = gerber.loads(data6, 'drl')
    find_lines = GKOGerberProcess(gerberLayer_gko, gerberLayer_gtl, gerberLayer_drl)
    all_lines = find_lines.find_all_line(gerberLayer_gko)
    vertical_lines, level_lines, other_lines = find_lines.pretreatment1(all_lines)
    NodesList = find_lines.NodesFound_lv1(level_lines, vertical_lines, other_lines)
    level_lines = find_lines.LineCut(level_lines, NodesList)
    vertical_lines = find_lines.LineCut(vertical_lines, NodesList)
    all_lines = find_lines.find_gong_lines(vertical_lines, level_lines, other_lines)
    border_lines, top_border_lines, right_border_lines, bottom_border_lines, left_border_lines = find_lines.find_border(all_lines)
    border_lines = find_lines.border_add(level_lines, vertical_lines, top_border_lines, right_border_lines, bottom_border_lines, left_border_lines, border_lines)
    for line in border_lines:
        all_lines.remove(line)

    glutInit(sys.argv)  # 初始化
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)  # 设置显示模式
    glutInitWindowPosition(0, 0)  # 窗口打开的位置，左上角坐标在屏幕坐标
    glutInitWindowSize(800, 800)  # 窗口大小
    glutCreateWindow(b"Function Plotter")  # 窗口名字，二进制
    glutDisplayFunc(draw)  # 设置当前窗口的显示回调
    glClearColor(1.0, 1.0, 1.0, 1.0)  # 设置背景颜色
    show_x_min, show_x_max, show_y_min, show_y_max = show(gerberLayer_gko)
    gluOrtho2D(show_x_min, show_x_max, show_y_min, show_y_max)  # 设置显示范围
    glutMainLoop()  # 启动循环'''