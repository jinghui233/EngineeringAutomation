import math
import gerber
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


def find_all_line(gerberLayer_gko2):
    start_points = []
    end_points = []
    rad = []
    angle = []
    all_lines = []
    for j in range(len(gerberLayer_gko2.primitives)):
        if type(gerberLayer_gko2.primitives[j]) == gerber.primitives.Line:
            start_points.append(gerberLayer_gko2.primitives[j].start)
            end_points.append(gerberLayer_gko2.primitives[j].end)
            rad.append(gerberLayer_gko2.primitives[j].aperture.radius)
            angle.append(gerberLayer_gko2.primitives[j].angle)
    for h in range(len(start_points)):
        all_lines.append([start_points[h], end_points[h], rad[h], angle[h]])
    return all_lines


def drl_find(gerberLayer_drl, all_lines):
    new_circles = []
    circles = []
    circles_r = []
    circles_core_points = []
    for drl in gerberLayer_drl.primitives:
        if type(drl) == gerber.primitives.Circle:
            circles.append([drl.position, drl.radius])
            circles_r.append(drl.radius)
            circles_core_points.append(drl.position)
    for circle1 in circles:
        m = 0
        for circle2 in circles:
            if (circle1 != circle2) and ((math.sqrt((circle1[0][1] - circle2[0][1]) ** 2 + (circle1[0][0] - circle2[0][0]) ** 2)) < 4 * min(circle1[1], circle2[1])):
                m = 1
        if m == 0:
            circle1.append('q')
        else:
            circle1.append('l')
    for i in range((len(circles) - 1), -1, -1):
        if circles[i][-1] == 'q':
            circles.pop(i)
        else:
            circles[i].pop()
    for circle in circles:
        circle.append('o')
    u = []
    for i in range(len(circles)):
        u.append(i)
    a = 1
    label = 0
    while a == 1:
        for i in u:
            for j in range(len(circles)):
                if type(circles[i][-1]) != int and type(circles[j][-1]) != int:
                    if (circles[i] != circles[j]) and (math.sqrt((circles[i][0][1] - circles[j][0][1]) ** 2 + (
                            circles[i][0][0] - circles[j][0][0]) ** 2) < 4 * min(circles[i][1], circles[j][1])):
                        circles[i][-1] = label
                        circles[j][-1] = label
                        continue
                elif type(circles[i][-1]) == int and type(circles[j][-1]) != int:
                    if (circles[i] != circles[j]) and (math.sqrt((circles[i][0][1] - circles[j][0][1]) ** 2 + (
                            circles[i][0][0] - circles[j][0][0]) ** 2) < 4 * min(circles[i][1], circles[j][1])):
                        circles[j][-1] = circles[i][-1]
                        continue
            label += 1
        labs = []
        for circle in circles:
            labs.append(circle[-1])
        if 'o' in labs:
            a = 1
        else:
            a = 0
    labels = []
    for circle in circles:
        labels.append(circle[-1])
    labels = list(set(labels))
    for lab in labels:
        z_circles = []
        for circle in circles:
            if circle[-1] == lab:
                z_circles.append(circle)
        size_z = len(z_circles)
        while len(z_circles) >= 3:
            z_x = []
            z_y = []
            for z in z_circles:
                z_x.append(z[0][0])
                z_y.append(z[0][1])
            for x in range((len(z_x) - 1), 0, -1):
                if abs(z_x[x] - z_x[x - 1]) < 0.01:
                    z_x.pop(x)
            for y in range((len(z_y) - 1), 0, -1):
                if abs(z_y[y] - z_y[y - 1]) < 0.01:
                    z_y.pop(y)
            if len(z_x) >= 3:
                for z in range((len(z_circles) - 1), -1, -1):
                    if z_circles[z][0][0] == min(z_x) or z_circles[z][0][0] == max(z_x):
                        z_circles.pop(z)
            elif len(z_y) >= 3:
                for z in range((len(z_circles) - 1), -1, -1):
                    if z_circles[z][0][1] == min(z_y) or z_circles[z][0][1] == max(z_y):
                        z_circles.pop(z)
            else:
                break
        size_z1 = len(z_circles)
        if size_z > size_z1 and len(z_circles) == 1:
            for z in z_circles:
                new_circles.append(z)
        if len(z_circles) == 2:
            new_circles.append([((z_circles[0][0][0]+z_circles[1][0][0])/2, (z_circles[0][0][1]+z_circles[1][0][1])/2), min(z_circles[0][1], z_circles[1][1]), z_circles[1][2]])
        for circle in new_circles:
            a = 0
            for line in all_lines:
                s_p = line[0]
                e_p = line[1]
                if s_p[0] == e_p[0]:
                    if (abs(circle[0][0]-s_p[0]) < circle[1]) and (min(s_p[1], e_p[1])<circle[0][1]<max(s_p[1], e_p[1])):
                        a = 1
                else:
                    k = (e_p[1]-s_p[1])/(e_p[0]-s_p[0])
                    b = e_p[1]-k*e_p[0]
                    d = abs(k*circle[0][0]-circle[0][1]+b)/math.sqrt(k**2+1)
                    if d<circle[1] and min(s_p[0], e_p[0])<circle[0][0]<max(s_p[0], e_p[0]):
                        a = 1
            if a == 1:
                pass
            else:
                new_circles.remove(circle)
    return new_circles


def ToGerberFile(circles):
    radius = []
    for circle in circles:
        radius.append(circle[1])
    for i in range(len(radius)-1, 0, -1):
        if radius[i] == radius[i-1]:
            radius.pop(i)
    str_c = []
    circle_news = []
    for c in range(len(radius)):
        n = c+10
        r = radius[c]
        str_c.append([n, r])
        z_circles = []
        for circle in circles:
            if circle[1] == r:
                z_circles.append(circle)
        circle_news.append([z_circles, n])
    writestr = "\n%FSLAX26Y26*%\n%MOIN*%\n"
    for s in str_c:
        writestr += f"%ADD{s[0]}C,{s[1]}*%\n"
    writestr += "%IPPOS*%\n%LNdrl*%\n%LPD*%\nG75*\n"
    for i in range(len(circle_news)):
        writestr += f"G54D{circle_news[i][1]}*\n"
        for circle in circle_news[i][0]:
            position_x = circle[0][0]
            position_y = circle[0][1]
            position_X = str(int(position_x * 10 ** 6))
            position_Y = str(int(position_y * 10 ** 6))
            writestr += 'X' + position_X + 'Y' + position_Y + 'D03*\n'
    return writestr


def draw():
    for circle in circles:
        glBegin(GL_LINE_STRIP)
        for nc in range(20):
            glColor3f(0.0, 0.0, 1.0)
            xc = circle[0][0] + circle[1] * math.cos(2 * nc * math.pi / 20)
            yc = circle[0][1] + circle[1] * math.sin(2 * nc * math.pi / 20)
            glVertex2f(xc, yc)
        glEnd()

    glFlush()


def show(gerberLayer_gko):
    show_x_min = (gerberLayer_gko.bounds[0][0] - 0.5)
    show_x_max = (gerberLayer_gko.bounds[0][1] + 0.5)
    show_y_min = (gerberLayer_gko.bounds[1][0] - 0.5)
    show_y_max = (gerberLayer_gko.bounds[1][1] + 0.5)
    return show_x_min, show_x_max, show_y_min, show_y_max


path = r"E:\GerberFile"

if __name__ == '__main__':
    drs = os.listdir(path)
    v = 11
    with open("{}\\{}\\drl".format(path, drs[v]), "r") as f1:
        data1 = f1.read()
        gerberLayer_drl = gerber.loads(data1, "drl")
    with open("{}\\{}\\gko".format(path, drs[v]), "r") as f2:
        data2 = f2.read()
        gerberLayer_gko = gerber.loads(data2, "gko")
    all_lines = find_all_line(gerberLayer_gko)
    circles = drl_find(gerberLayer_drl, all_lines)
    writestr = ToGerberFile(circles)
    with open("{}\\{}\\new_drl".format(path, drs[v]), "w") as f8:
        f8.write(writestr)
    glutInit(sys.argv)  # 初始化
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)  # 设置显示模式
    glutInitWindowPosition(0, 0)  # 窗口打开的位置，左上角坐标在屏幕坐标
    glutInitWindowSize(800, 800)  # 窗口大小
    glutCreateWindow(b"Function Plotter")  # 窗口名字，二进制
    glutDisplayFunc(draw)  # 设置当前窗口的显示回调
    glClearColor(1.0, 1.0, 1.0, 1.0)  # 设置背景颜色
    show_x_min, show_x_max, show_y_min, show_y_max = show(gerberLayer_gko)
    gluOrtho2D(show_x_min, show_x_max, show_y_min, show_y_max)  # 设置显示范围
    glutMainLoop()  # 启动循环'''

