import gerber
import math
import copy
with open(f"F:\\work\\GerberFile\\JP-1W1747554\gko", "r") as f4:
    data4 = f4.read()
    gerberLayer_gko = gerber.loads(data4, 'gko')
    afew = 0
gerberLayer_gko.primitives[0].start=(-2e-06, 4)
all_lines=[]
nodes=[]
arc_lines=[]
labels_true=[]
for primitive in gerberLayer_gko.primitives:
    if type(primitive) == gerber.primitives.Line:
        all_lines.append([primitive.start,primitive.end,primitive.angle])
for line in all_lines:
    if math.sqrt(
        (line[1][1] - line[0][1]) ** 2 + (line[1][0] - line[0][0]) ** 2)< 0.06:
        arc_lines.append(line)
        nodes.append(line[0])
        nodes.append(line[1])
        # labels_true.append(i)
        # labels_true.append(i+1)





import numpy as np
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler


# #############################################################################
# Generate sample data
centers = [[1, 1], [-1, -1], [1, -1]]
X, labels_true = make_blobs(
    n_samples=200, centers=centers, cluster_std=0.4, random_state=0
)

X = StandardScaler().fit_transform(X)
# X=nodes
# #############################################################################
# Compute DBSCAN
db = DBSCAN(eps=0.3, min_samples=10).fit(X)
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)

# print("Estimated number of clusters: %d" % n_clusters_)
# print("Estimated number of noise points: %d" % n_noise_)
# print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
# print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
# print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
# print("Adjusted Rand Index: %0.3f" % metrics.adjusted_rand_score(labels_true, labels))
# print(
#     "Adjusted Mutual Information: %0.3f"
#     % metrics.adjusted_mutual_info_score(labels_true, labels)
# )
# print("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(X, labels))

# #############################################################################
# Plot result
import matplotlib.pyplot as plt

# Black removed and is used for noise instead.
unique_labels = set(labels)
colors = [plt.cm.Spectral(each) for each in np.linspace(0, 1, len(unique_labels))]
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = [0, 0, 0, 1]

    class_member_mask = labels == k
    print(X[class_member_mask & core_samples_mask])
    xy = X[class_member_mask & core_samples_mask]
    plt.plot(
        xy[:, 0],
        xy[:, 1],
        "o",
        markerfacecolor=tuple(col),
        markeredgecolor="k",
        markersize=14,
    )

    xy = X[class_member_mask & ~core_samples_mask]
    plt.plot(
        xy[:, 0],
        xy[:, 1],
        "o",
        markerfacecolor=tuple(col),
        markeredgecolor="k",
        markersize=6,
    )

plt.title("Estimated number of clusters: %d" % n_clusters_)
plt.show()
