from matplotlib.pyplot import *
from collections import defaultdict
import random
import gerber
import math
import copy
with open(f"F:\\work\\GerberFile\\JP-1W1747554\gko", "r") as f4:#JP-2W2097778
    data4 = f4.read()
    gerberLayer_gko = gerber.loads(data4, 'gko')
    afew = 0
gerberLayer_gko.primitives[0].start=(-2e-06, 4)
all_lines=[]
nodes=[]
arc_lines=[]
labels_true=[]
for primitive in gerberLayer_gko.primitives:
    if type(primitive) == gerber.primitives.Line:
        all_lines.append([primitive.start,primitive.end,primitive.angle])
for line in all_lines:
    if math.sqrt(
        (line[1][1] - line[0][1]) ** 2 + (line[1][0] - line[0][0]) ** 2)< 0.06:
        arc_lines.append(line)
        nodes.append([line[0][0],line[0][1]])
        nodes.append([line[1][0],line[1][1]])

# function to calculate distance
def dist(p1, p2):
    return ((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2) ** (0.5)

    # randomly generate around 100 cartesian coordinates


all_points = nodes

# for i in range(100):
#     randCoord = [random.randint(1, 50), random.randint(1, 50)]
#     if not randCoord in all_points:
#         all_points.append(randCoord)

        # take radius = 8 and min. points = 8
E = 0.1
minPts = 10

# find out the core points
other_points = []
core_points = []
plotted_points = []
for point in all_points:
    point.append(0)  # assign initial level 0
    total = 0
    for otherPoint in all_points:
        distance = dist(otherPoint, point)
        if distance <= E:
            total += 1

    if total > minPts:
        core_points.append(point)
        plotted_points.append(point)
    else:
        other_points.append(point)

        # find border points
border_points = []
for core in core_points:
    for other in other_points:
        if dist(core, other) <= E:
            border_points.append(other)
            plotted_points.append(other)

            # implement the algorithm
cluster_label = 0

for point in core_points:
    if point[2] == 0:
        cluster_label += 1
        point[2] = cluster_label

    for point2 in plotted_points:
        distance = dist(point2, point)
        if point2[2] == 0 and distance <= E:

            point2[2] = point[2]

            # after the points are asssigned correnponding labels, we group them
cluster_list = defaultdict(lambda: [[], []])
for point in plotted_points:
    cluster_list[point[2]][0].append(point[0])
    cluster_list[point[2]][1].append(point[1])

# markers = ['.', '.', '.', '.', '.', '.', '.', '.', '.']

# plotting the clusters
i = 0

for value in cluster_list:
    cluster = cluster_list[value]
    plot(cluster[0], cluster[1], '.')
    i = i % 10 + 1

    # plot the noise points as well
# noise_points = []
# for point in all_points:
#     if not point in core_points and not point in border_points:
#         noise_points.append(point)
# noisex = []
# noisey = []
# for point in noise_points:
#     noisex.append(point[0])
#     noisey.append(point[1])
# plot(noisex, noisey, "x")


show_x_min = (gerberLayer_gko.bounds[0][0] - 0.5)
show_x_max = (gerberLayer_gko.bounds[0][1] + 0.5)
show_y_min = (gerberLayer_gko.bounds[1][0] - 0.5)
show_y_max = (gerberLayer_gko.bounds[1][1] + 0.5)


# title(str(len(cluster_list)) + " clusters created with E =" + str(E) + " Min Points=" + str(
#     minPts) + " total points=" + str(len(all_points)) + " noise Points = " + str(len(noise_points)))
axis((show_x_min, show_x_max, show_y_min, show_y_max))
show()