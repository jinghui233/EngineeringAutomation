import math
import gerber
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import sys
import copy
from ProcessService.RoutLineProcess.LinePiceSet import LinePice as LinePice_sd, LineSet as LineSet_sd

class GKOGerberProcess:
    def __init__(self, gerberLayer_gko, gerberLayer_gtl):
        self.gerberLayer_gko = gerberLayer_gko
        self.gerberLayer_gtl = gerberLayer_gtl
        self.NodesList1 = []
        self.Rectangle_gtl = []
        self.sets = []
        self.gong_lines=[]
        self.run
        pass

    def run(self):
        all_lines1 = self.find_all_line(self.gerberLayer_gko)
        border_lines1, top_border_lines, right_border_lines, bottom_border_lines, left_border_lines = self.find_border(all_lines1)
        all_lines2 = self.clr_border(border_lines1, all_lines1)
        vertical_lines1, level_lines1, line_points1, d_lines1 = self.pretreatment(all_lines2)
        border_lines2 = self.border_add(level_lines1, vertical_lines1, top_border_lines, right_border_lines, bottom_border_lines, left_border_lines, border_lines1)
        line_points2, level_lines2, vertical_lines2 = self.find_vlines(level_lines1, vertical_lines1, line_points1, border_lines2)
        gong_lines1 = self.find_gong_lines(line_points2, border_lines2, d_lines1)
        level_lines3 = self.LineRemove1(level_lines2)
        vertical_lines3 = self.LineRemove2(vertical_lines2)
        gong_lines2 = self.LineRemove3(level_lines3, vertical_lines3, gong_lines1)
        NodesList1 = self.NodesFound_lv1(level_lines3, vertical_lines3, gong_lines2)
        level_lines4 = self.LineCut(level_lines3, NodesList1)
        vertical_lines4 = self.LineCut(vertical_lines3, NodesList1)
        border_lines3 = self.border_add(level_lines4, vertical_lines4, top_border_lines, right_border_lines, bottom_border_lines, left_border_lines, border_lines2)
        gong_lines3 = self.find_gong_lines(line_points2, border_lines3, d_lines1)
        self.delVLine(level_lines4, vertical_lines4, border_lines3, gong_lines3, self.gerberLayer_gtl)
        gong_lines4 = self.addlines(level_lines4, vertical_lines4, gong_lines3)
        self.gong_lines = gong_lines4
        self.level_lines = level_lines4
        self.vertical_lines = vertical_lines4
        self.dataTrans()


    def dataTrans(self):
        sets = []
        i = 0
        for lineSet in self.gong_lines:
            lines = []
            for line in lineSet:
                lines.append(LinePice_sd((line[0][0], line[0][1]), (line[1][0], line[1][1]), 1))
            set = LineSet_sd()
            set.start1 = [line[0][0], line[0][1]]
            set.end1 = [line[1][0], line[1][1]]
            set.setLines(lines)
            sets.append(set)
            i += 1
        self.sets = sets

    def find_all_line(self, gerberLayer_gko2):
        start_points = []
        end_points = []
        rad = []
        angle = []
        all_lines = []
        for j in range(len(gerberLayer_gko2.primitives)):
            if type(gerberLayer_gko2.primitives[j]) == gerber.primitives.Line:
                start_points.append(gerberLayer_gko2.primitives[j].start)
                end_points.append(gerberLayer_gko2.primitives[j].end)
                rad.append(gerberLayer_gko2.primitives[j].aperture.radius)
                angle.append(gerberLayer_gko2.primitives[j].angle)
        for h in range(len(start_points)):
            all_lines.append([start_points[h], end_points[h], rad[h], angle[h]])
        return all_lines

    def find_border(self, all_lines):
        border_lines = []
        top_border_lines = []
        right_border_lines = []
        bottom_border_lines = []
        left_border_lines = []
        x_b = []
        y_b = []
        for a in range(len(all_lines)):
            x_b.append(all_lines[a][0][0])
            x_b.append(all_lines[a][1][0])
            y_b.append(all_lines[a][0][1])
            y_b.append(all_lines[a][1][1])
        # 上边框
        x_top = min(x_b)
        x_top += 0.004
        while x_top < max(x_b):
            # print(1)
            y_ss = []
            z_lines = []
            for a1 in range(len(all_lines)):
                left = min(all_lines[a1][0][0], all_lines[a1][1][0])
                right = max(all_lines[a1][0][0], all_lines[a1][1][0])
                if left < x_top <= right:
                    k = (all_lines[a1][1][1] - all_lines[a1][0][1]) / (all_lines[a1][1][0] - all_lines[a1][0][0])
                    b = all_lines[a1][1][1] - k * all_lines[a1][1][0]
                    y_s = (k * x_top + b)
                    y_ss.append(y_s)
                    z_lines.append([all_lines[a1], y_s])
            for z in range((len(z_lines) - 1), -1, -1):
                if abs(z_lines[z][1]-max(y_ss))<0.01:
                    top_border_lines.append(z_lines[z][0])
                    border_lines.append(z_lines[z].pop(0))
            if max(border_lines[-1][0][0], border_lines[-1][1][0]) > x_top:
                x_top = max(border_lines[-1][0][0], border_lines[-1][1][0])
                x_top += 0.004
            else:
                x_top += 0.004
        # 右边框
        y_right = min(y_b)
        y_right += 0.004
        a = 0
        while y_right < max(y_b):
            # print(2)
            x_ss = []
            z_lines = []
            for a1 in range(len(all_lines)):
                bottom = min(all_lines[a1][0][1], all_lines[a1][1][1])
                top = max(all_lines[a1][0][1], all_lines[a1][1][1])
                if bottom < y_right <= top:
                    k = (all_lines[a1][1][0] - all_lines[a1][0][0]) / (all_lines[a1][1][1] - all_lines[a1][0][1])
                    b = all_lines[a1][1][0] - k * all_lines[a1][1][1]
                    x_s = (k * y_right + b)
                    x_ss.append(x_s)
                    z_lines.append([all_lines[a1], x_s])
            for z in range((len(z_lines) - 1), -1, -1):
                if abs(z_lines[z][1] - max(x_ss))<0.01:
                    right_border_lines.append(z_lines[z][0])
                    border_lines.append(z_lines[z].pop(0))
            if y_right < max(border_lines[-1][0][1], border_lines[-1][1][1]):
                y_right = max(border_lines[-1][0][1], border_lines[-1][1][1])
                y_right += 0.004
            else:
                y_right += 0.004
        # 下边框
        x_bottom = min(x_b)
        x_bottom += 0.004
        while x_bottom < max(x_b):
            # print(3)
            y_ss = []
            z_lines = []
            for a1 in range(len(all_lines)):
                left = min(all_lines[a1][0][0], all_lines[a1][1][0])
                right = max(all_lines[a1][0][0], all_lines[a1][1][0])
                if left < x_bottom <= right:
                    k = (all_lines[a1][1][1] - all_lines[a1][0][1]) / (all_lines[a1][1][0] - all_lines[a1][0][0])
                    b = all_lines[a1][1][1] - k * all_lines[a1][1][0]
                    y_s = (k * x_bottom + b)
                    y_ss.append(y_s)
                    z_lines.append([all_lines[a1], y_s])
            for z in range((len(z_lines) - 1), -1, -1):
                if abs(z_lines[z][1] - min(y_ss))<0.01:
                    bottom_border_lines.append(z_lines[z][0])
                    border_lines.append(z_lines[z].pop(0))
            if max(border_lines[-1][0][0], border_lines[-1][1][0]) > x_bottom:
                x_bottom = max(border_lines[-1][0][0], border_lines[-1][1][0])
                x_bottom += 0.004
            else:
                x_bottom += 0.004
        # 左边框
        y_left = min(y_b)
        y_left += 0.004
        b = 0
        while y_left < max(y_b):
            # print(4)
            x_ss = []
            z_lines = []
            for a1 in range(len(all_lines)):
                bottom = min(all_lines[a1][0][1], all_lines[a1][1][1])
                top = max(all_lines[a1][0][1], all_lines[a1][1][1])
                if bottom < y_left <= top:
                    k = (all_lines[a1][1][0] - all_lines[a1][0][0]) / (all_lines[a1][1][1] - all_lines[a1][0][1])
                    b = all_lines[a1][1][0] - k * all_lines[a1][1][1]
                    x_s = (k * y_left + b)
                    x_ss.append(x_s)
                    z_lines.append([all_lines[a1], x_s])
            for z in range((len(z_lines) - 1), -1, -1):
                if abs(z_lines[z][1] - min(x_ss))<0.01:
                    left_border_lines.append(z_lines[z][0])
                    border_lines.append(z_lines[z].pop(0))
            if max(border_lines[-1][0][1], border_lines[-1][1][1]) > y_left:
                y_left = max(border_lines[-1][0][1], border_lines[-1][1][1])
                y_left += 0.004
            else:
                y_left += 0.004
        return border_lines, top_border_lines, right_border_lines, bottom_border_lines, left_border_lines

    def clr_border(self, border_lines, all_lines):
        for i in range(len(border_lines)):
            for j in range((len(all_lines) - 1), -1, -1):
                if all_lines[j] == border_lines[i]:
                    all_lines.pop(j)
        return all_lines

    def pretreatment(self, all_lines):
        line_points = []
        for l in range((len(all_lines) - 1), -1, -1):
            line_points.append(all_lines.pop(l))
        # 把所有长度大于0.06的line放入d_line中，弧线必为锣带
        d_lines = []
        for i in range((len(line_points) - 1), -1, -1):
            D = math.sqrt((line_points[i][1][1] - line_points[i][0][1]) ** 2 + (line_points[i][1][0] - line_points[i][0][0]) ** 2)
            if D > 0.06:
                d_lines.append(line_points.pop(i))
        # 把d_line中的线按水平与垂直分别放入leve_lines和vertical_lines中
        level_lines = []
        vertical_lines = []
        for k in range((len(d_lines) - 1), -1, -1):
            if abs(d_lines[k][0][1] - d_lines[k][1][1]) < 0.01:
                level_lines.append(d_lines.pop(k))
            elif abs(d_lines[k][0][0] - d_lines[k][1][0]) < 0.01:
                vertical_lines.append(d_lines.pop(k))
        return vertical_lines, level_lines, line_points, d_lines

        # 补充边框

    def border_add(self, level_lines1, vertical_lines1, top_border_lines, right_border_lines, bottom_border_lines, left_border_lines, border_lines):
        t_points = []
        for bt in range(len(top_border_lines)):
            t_points.append(top_border_lines[bt][0])
            t_points.append(top_border_lines[bt][1])
        b_points = []
        for bb in range(len(bottom_border_lines)):
            b_points.append(bottom_border_lines[bb][0])
            b_points.append(bottom_border_lines[bb][1])
        l_points = []
        for bl in range(len(left_border_lines)):
            l_points.append(left_border_lines[bl][0])
            l_points.append(left_border_lines[bl][1])
        r_points = []
        for br in range(len(right_border_lines)):
            r_points.append(right_border_lines[br][0])
            r_points.append(right_border_lines[br][1])
        if level_lines1 is not None:
            for l1 in range((len(level_lines1) - 1), -1, -1):
                ld_ps = []
                ld_pe = []
                for p in l_points:
                    ld_ps.append(math.sqrt((p[1] - level_lines1[l1][0][1]) ** 2 + (p[0] - level_lines1[l1][0][0]) ** 2))
                    ld_pe.append(math.sqrt((p[1] - level_lines1[l1][1][1]) ** 2 + (p[0] - level_lines1[l1][1][0]) ** 2))
                if (min(ld_ps) < 0.05) and (min(ld_pe) < 0.05):
                    border_lines.append(level_lines1.pop(l1))
                    continue
                rd_ps = []
                rd_pe = []
                for p in r_points:
                    rd_ps.append(math.sqrt((p[1] - level_lines1[l1][0][1]) ** 2 + (p[0] - level_lines1[l1][0][0]) ** 2))
                    rd_pe.append(math.sqrt((p[1] - level_lines1[l1][1][1]) ** 2 + (p[0] - level_lines1[l1][1][0]) ** 2))
                if (min(rd_ps) < 0.05) and (min(rd_pe) < 0.05):
                    border_lines.append(level_lines1.pop(l1))
        if vertical_lines1!=None:
            for v1 in range((len(vertical_lines1) - 1), -1, -1):
                td_ps = []
                td_pe = []
                for p in t_points:
                    td_ps.append(
                        math.sqrt((p[1] - vertical_lines1[v1][0][1]) ** 2 + (p[0] - vertical_lines1[v1][0][0]) ** 2))
                    td_pe.append(
                        math.sqrt((p[1] - vertical_lines1[v1][1][1]) ** 2 + (p[0] - vertical_lines1[v1][1][0]) ** 2))
                if (min(td_ps) < 0.05) and (min(td_pe) < 0.05):
                    border_lines.append(vertical_lines1.pop(v1))
                    continue
                bd_ps = []
                bd_pe = []
                for p in b_points:
                    bd_ps.append(
                        math.sqrt((p[1] - vertical_lines1[v1][0][1]) ** 2 + (p[0] - vertical_lines1[v1][0][0]) ** 2))
                    bd_pe.append(
                        math.sqrt((p[1] - vertical_lines1[v1][1][1]) ** 2 + (p[0] - vertical_lines1[v1][1][0]) ** 2))
                if (min(bd_ps) < 0.05) and (min(bd_pe) < 0.05):
                    border_lines.append(vertical_lines1.pop(v1))
        return border_lines

        # 假V割线

    def find_vlines(self, level_lines2, vertical_lines2, line_points, border_lines):
        y_bor = []
        x_bor = []
        for i in range(len(border_lines)):
            x_bor.append(border_lines[i][0][0])
            x_bor.append(border_lines[i][1][0])
            y_bor.append(border_lines[i][0][1])
            y_bor.append(border_lines[i][1][1])
        # 水平V割（含锣带）
        level_lines_new_ys = []
        for l2 in range(len(level_lines2)):
            level_lines_new_ys.append((level_lines2[l2][0][1] + level_lines2[l2][1][1]) / 2)
        level_lines_new_ys.sort()
        for y in range((len(level_lines_new_ys) - 1), 0, -1):
            if abs(level_lines_new_ys[y] - level_lines_new_ys[y - 1]) < 0.01:
                level_lines_new_ys.pop(y)
        if len(level_lines_new_ys) > 0:
            if level_lines_new_ys[0] == min(y_bor):
                level_lines_new_ys.pop(0)
            elif level_lines_new_ys[-1] == max(y_bor):
                level_lines_new_ys.pop(-1)
            # print(level_lines_new_ys)
        for y in range(len(level_lines_new_ys)):
            z_lines = []
            for l3 in range((len(level_lines2) - 1), -1, -1):
                level_line_y = (level_lines2[l3][0][1] + level_lines2[l3][1][1]) / 2
                if abs(level_line_y - level_lines_new_ys[y]) < 0.01:
                    z_lines.append(level_lines2.pop(l3))
            x_ss = []
            for b in range(len(border_lines)):
                case3 = max(border_lines[b][0][1], border_lines[b][1][1]) > min(border_lines[b][0][1],
                                                                                border_lines[b][1][1])
                if case3 and (min(border_lines[b][0][1], border_lines[b][1][1]) <= level_lines_new_ys[y] <= max(
                        border_lines[b][0][1], border_lines[b][1][1])):
                    k = (border_lines[b][1][0] - border_lines[b][0][0]) / (
                            border_lines[b][1][1] - border_lines[b][0][1])
                    m = border_lines[b][1][0] - k * border_lines[b][1][1]
                    x_s = (k * level_lines_new_ys[y] + m)
                    x_ss.append(x_s)
            x_ss.sort()
            while len(x_ss) > 2:
                x_ss.pop(-1)
                x_ss.pop(0)
            # print('1看', x_ss)
            if len(x_ss) > 2:
                point_x = []
                while len(x_ss) > 2:
                    point_x.append(x_ss[-1])
                    point_x.append(x_ss[0])
                    x_ss.pop(0)
                    x_ss.pop(-1)
                r = z_lines[0][2]
                angle = z_lines[0][3]
                # print('2看', x_ss)
                if (max(point_x) - x_ss[-1]) > 0.06:
                    for z in range((len(z_lines) - 1), -1, -1):
                        if abs(max(z_lines[z][0][0], z_lines[z][1][0]) - max(point_x)) < 0.05:
                            z_min = min(z_lines[z][0][0], z_lines[z][1][0])
                            z_lines.pop(z)
                            z_lines.append(
                                [(z_min, level_lines_new_ys[y]), (max(x_ss), level_lines_new_ys[y]), r, angle])
                            border_lines.append(
                                [(max(x_ss), level_lines_new_ys[y]), (max(point_x), level_lines_new_ys[y]), r, angle])
                    for zm in range((len(z_lines) - 1), -1, -1):
                        if abs(min(z_lines[zm][0][0], z_lines[zm][1][0]) - min(point_x)) < 0.05:
                            z_max = max(z_lines[zm][0][0], z_lines[zm][1][0])
                            z_lines.pop(zm)
                            z_lines.append(
                                [(min(x_ss), level_lines_new_ys[y]), (z_max, level_lines_new_ys[y]), r, angle])
                            border_lines.append(
                                [(min(point_x), level_lines_new_ys[y]), (min(x_ss), level_lines_new_ys[y]), r, angle])
            for z in range((len(z_lines) - 1), -1, -1):
                if len(x_ss) == 0:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        level_lines2.append(z_lines.pop(z1))
                    break
                elif abs(min(x_ss) - z_lines[z][0][0]) < 0.05:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        level_lines2.append(z_lines.pop(z1))
                    break
                elif abs(min(x_ss) - z_lines[z][1][0]) < 0.05:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        level_lines2.append(z_lines.pop(z1))
                    break
                elif abs(max(x_ss) - z_lines[z][1][0]) < 0.05:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        level_lines2.append(z_lines.pop(z1))
                    break
                elif abs(max(x_ss) - z_lines[z][0][0]) < 0.05:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        level_lines2.append(z_lines.pop(z1))
                    break
            if len(z_lines) > 0:
                for z2 in range(len(z_lines)):
                    line_points.append(z_lines[z2])
        # 竖直V割（含锣带）
        vertical_lines_new_xs = []
        for l2 in range(len(vertical_lines2)):
            vertical_lines_new_xs.append((vertical_lines2[l2][0][0] + vertical_lines2[l2][1][0]) / 2)
        vertical_lines_new_xs.sort()
        for x in range((len(vertical_lines_new_xs) - 1), 0, -1):
            if abs(vertical_lines_new_xs[x] - vertical_lines_new_xs[x - 1]) < 0.01:
                vertical_lines_new_xs.pop(x)
        # print(vertical_lines_new_xs)
        if len(vertical_lines_new_xs) > 0:
            if vertical_lines_new_xs[0] == min(x_bor):
                vertical_lines_new_xs.pop(0)
            elif vertical_lines_new_xs[-1] == max(x_bor):
                vertical_lines_new_xs.pop(-1)
        # print(vertical_lines_new_xs)
        for x in range(len(vertical_lines_new_xs)):
            z_lines = []
            for l3 in range((len(vertical_lines2) - 1), -1, -1):
                vertical_line_x = (vertical_lines2[l3][0][0] + vertical_lines2[l3][1][0]) / 2
                if abs(vertical_line_x - vertical_lines_new_xs[x]) < 0.01:
                    z_lines.append(vertical_lines2.pop(l3))
            y_ss = []
            for b in range(len(border_lines)):
                case4 = max(border_lines[b][0][0], border_lines[b][1][0]) > min(border_lines[b][0][0],
                                                                                border_lines[b][1][0])
                if case4 and (min(border_lines[b][0][0], border_lines[b][1][0]) <= vertical_lines_new_xs[x] <= max(
                        border_lines[b][0][0], border_lines[b][1][0])):
                    k = (border_lines[b][1][1] - border_lines[b][0][1]) / (
                            border_lines[b][1][0] - border_lines[b][0][0])
                    m = border_lines[b][1][1] - k * border_lines[b][1][0]
                    y_s = (k * vertical_lines_new_xs[x] + m)
                    y_ss.append(y_s)
            y_ss.sort()
            while len(y_ss) > 2:
                y_ss.pop(-1)
                y_ss.pop(0)
            if len(y_ss) > 2:
                point_y = []
                while len(y_ss) > 2:
                    point_y.append(y_ss[-1])
                    point_y.append(y_ss[0])
                    y_ss.pop(0)
                    y_ss.pop(-1)
                r = z_lines[0][2]
                angle = z_lines[0][3]
                if (max(point_y) - y_ss[-1]) > 0.06:
                    for z in range((len(z_lines) - 1), -1, -1):
                        if abs(max(z_lines[z][0][1], z_lines[z][1][1]) - max(point_y)) < 0.05:
                            z_min = min(z_lines[z][0][1], z_lines[z][1][1])
                            z_lines.pop(z)
                            z_lines.append(
                                [(vertical_lines_new_xs[x], z_min), (vertical_lines_new_xs[x], max(y_ss)), r, angle])
                            border_lines.append(
                                [(vertical_lines_new_xs[x], max(y_ss)), (vertical_lines_new_xs[x], max(point_y)), r,
                                 angle])
                    for zm in range((len(z_lines) - 1), -1, -1):
                        if abs(min(z_lines[zm][0][1], z_lines[zm][1][1]) - min(point_y)) < 0.05:
                            z_max = max(z_lines[zm][0][1], z_lines[zm][1][1])
                            z_lines.pop(zm)
                            z_lines.append(
                                [(vertical_lines_new_xs[x], min(y_ss)), (vertical_lines_new_xs[x], z_max), r, angle])
                            border_lines.append(
                                [(vertical_lines_new_xs[x], min(point_y)), (vertical_lines_new_xs[x], min(y_ss)), r,
                                 angle])
            for z in range((len(z_lines) - 1), -1, -1):
                if len(y_ss) == 0:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        vertical_lines2.append(z_lines.pop(z1))
                    break
                elif abs(min(y_ss) - z_lines[z][0][1]) < 0.03:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        vertical_lines2.append(z_lines.pop(z1))
                    break
                elif abs(min(y_ss) - z_lines[z][1][1]) < 0.03:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        vertical_lines2.append(z_lines.pop(z1))
                    break
                elif abs(max(y_ss) - z_lines[z][1][1]) < 0.03:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        vertical_lines2.append(z_lines.pop(z1))
                    break
                elif abs(max(y_ss) - z_lines[z][0][1]) < 0.03:
                    for z1 in range((len(z_lines) - 1), -1, -1):
                        vertical_lines2.append(z_lines.pop(z1))
                    break
            if len(z_lines) > 0:
                for z2 in range(len(z_lines)):
                    line_points.append(z_lines[z2])
        return line_points, level_lines2, vertical_lines2

    def find_gong_lines(self, line_points, border_lines, d_lines):
        gong_lines = []
        for o in range(len(line_points)):
            gong_lines.append(line_points[o])
        for o1 in range(len(d_lines)):
            gong_lines.append(d_lines[o1])
        for o2 in range(len(border_lines)):
            gong_lines.append(border_lines[o2])
        return gong_lines

    def LineRemove1(self,LinesList):
        level_Lines = []
        lines_v = []
        for n in range(len(LinesList)):
            lines_v.append((LinesList[n][0][1] + LinesList[n][1][1]) / 2)
        lines_v.sort()
        # print(len(lines_v))
        for y1 in range((len(lines_v) - 1), 0, -1):
            if abs(lines_v[y1] - lines_v[y1 - 1]) < 0.01:
                lines_v.pop(y1)
        # print(len(lines_v))

        for n in range(len(lines_v)):
            lines = []
            for Line in LinesList:
                if abs((Line[0][1] + Line[1][1]) / 2 - lines_v[n]) < 0.01:
                    lines.append(Line)
            i = 0
            # print(level)
            # 水平去重
            while (i < len(lines)) and (len(lines) > 0):
                q_l_min = min(lines[i][0][0], lines[i][1][0])
                q_l_max = max(lines[i][0][0], lines[i][1][0])
                j = 0
                while j < len(lines):
                    if i == j:
                        j += 1
                        continue
                    q3_min = min(lines[j][0][0], lines[j][1][0])
                    q3_max = max(lines[j][0][0], lines[j][1][0])
                    condition1 = (q_l_max < q3_min) and (abs(q3_min - q_l_max) > 0.01)
                    condition2 = (q3_max < q_l_min) and (abs(q_l_min - q3_max) > 0.01)
                    if not (condition1 or condition2):
                        x_min_new = min(lines[i][0][0], lines[i][1][0], lines[j][0][0],
                                        lines[j][1][0])
                        x_max_new = max(lines[i][0][0], lines[i][1][0], lines[j][0][0],
                                        lines[j][1][0])
                        lines[i] = [[x_min_new, lines[j][0][1]],
                                    [x_max_new, lines[j][0][1]],
                                    lines[j][2], lines[j][3]]
                        q_l_min = min(lines[i][0][0], lines[i][1][0])
                        q_l_max = max(lines[i][0][0], lines[i][1][0])
                        lines.pop(j)
                        if i > j:
                            i -= 1
                        if j == len(lines) - 1:
                            break
                        j = 0
                    j += 1
                    # print(lines[i])
                i += 1
            Lines = copy.deepcopy(lines)
            # print(len(lines))
            for line in Lines:
                level_Lines.append(line)
        return level_Lines

    def LineRemove2(self,LinesList):
        v_Lines = []
        lines_v = []
        for n in range(len(LinesList)):
            lines_v.append((LinesList[n][0][0] + LinesList[n][1][0]) / 2)
        lines_v.sort()
        for x1 in range((len(lines_v) - 1), 0, -1):
            if abs(lines_v[x1] - lines_v[x1 - 1]) < 0.01:
                lines_v.pop(x1)
        # print(len(lines_v))

        for n in range(len(lines_v)):
            lines = []
            for Line in LinesList:
                if abs((Line[0][0] + Line[1][0]) / 2 - lines_v[n]) < 0.01:
                    lines.append(Line)
            i = 0
            # 水平去重
            while (i < len(lines)) and (len(lines) > 0):
                q_l_min = min(lines[i][0][1], lines[i][1][1])
                q_l_max = max(lines[i][0][1], lines[i][1][1])
                j = 0
                while j < len(lines):
                    if i == j:
                        j += 1
                        continue
                    q3_min = min(lines[j][0][1], lines[j][1][1])
                    q3_max = max(lines[j][0][1], lines[j][1][1])
                    condition1 = (q_l_max < q3_min) and (abs(q3_min - q_l_max) > 0.01)
                    condition2 = (q3_max < q_l_min) and (abs(q_l_min - q3_max) > 0.01)
                    if not (condition1 or condition2):
                        y_min_new = min(lines[i][0][1], lines[i][1][1], lines[j][0][1],
                                        lines[j][1][1])
                        y_max_new = max(lines[i][0][1], lines[i][1][1], lines[j][0][1],
                                        lines[j][1][1])
                        lines[i] = [[lines[j][0][0], y_min_new],
                                    [lines[j][0][0], y_max_new],
                                    lines[j][2], lines[j][3]]
                        q_l_min = min(lines[i][0][1], lines[i][1][1])
                        q_l_max = max(lines[i][0][1], lines[i][1][1])
                        lines.pop(j)
                        if i > j:
                            i -= 1
                        if j == len(lines) - 1:
                            break
                        j = 0
                    j += 1
                i += 1
            Lines = copy.deepcopy(lines)
            # print(len(lines))
            for line in Lines:
                v_Lines.append(line)

        return v_Lines

    def LineRemove3(self,LinesList1, LinesList2, LinesList3):
        LinesList = LinesList1 + LinesList2
        n = 0.0039
        for i in range(len(LinesList)):
            for j in range(len(LinesList3) - 1, 10, -1):
                # for j in range(3, 2, -1):
                if min(LinesList[i][0][0], LinesList[i][1][0]) - n < LinesList3[j][0][0] < max(LinesList[i][0][0],
                                                                                               LinesList[i][1][
                                                                                                   0]) + n and min(
                        LinesList[i][0][1], LinesList[i][1][1]) - n < LinesList3[j][0][1] < (
                        max(LinesList[i][0][1], LinesList[i][1][1]) + n) \
                        and min(LinesList[i][0][0], LinesList[i][1][0]) - n < LinesList3[j][1][0] < max(
                    LinesList[i][0][0], LinesList[i][1][0]) + n and min(LinesList[i][0][1], LinesList[i][1][1]) - n < \
                        LinesList3[j][1][1] < (max(LinesList[i][0][1], LinesList[i][1][1]) + n):
                    # if (LinesList3[0][0][0]-LinesList3[0][1][0]) < 0.01 :
                    # print(LinesList3[j][1][1],bool(LinesList3[j][1][1]<(max(LinesList[i][0][1],LinesList[i][1][1])+n)))
                    # print(LinesList3[j],LinesList[i])
                    LinesList3.pop(j)
                # if min(line1[0][0],line1[1][0])-n<line3[0][0]<max(line1[0][0],line1[1][0])+n and min(line1[0][1],line1[1][1])-n<line3[0][1]<max(line1[0][1],line1[1][1])+n:
                #     LinesList3.remove(line3)
        # print(LinesList3)
        return LinesList3

    def distance(self,x1, y1, x2, y2):
        return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

    def NodesFound_lv1(self,level_lines, vertical_lines, gong_lines):
        NodesList1 = []
        i = 0
        for liney in level_lines:
            j = 0
            for linex in vertical_lines:
                if (min(liney[0][0], liney[1][0]) - 0.01 < linex[0][0] < max(liney[0][0], liney[1][0]) + 0.01) and (
                        min(linex[0][1], linex[1][1]) - 0.01 < liney[0][1] < max(linex[0][1], linex[1][1]) + 0.01):
                    NodesList1.append(
                        [i, -1, linex[0][0], liney[0][1], self.distance(linex[0][0], liney[0][1], liney[0][0], liney[0][1])])
                    NodesList1.append(
                        [-1, j, linex[0][0], liney[0][1], self.distance(linex[0][0], liney[0][1], linex[0][0], linex[0][1])])
                j += 1
            i += 1
        n = 0.011
        i = 0
        for liney in level_lines:
            for line in gong_lines:
                if (min(liney[0][0], liney[1][0]) - 0.01 < line[0][0] < max(liney[0][0], liney[1][0]) + 0.01) and abs(
                        line[0][1] - liney[0][1]) < n:
                    NodesList1.append(
                        [i, -1, line[0][0], liney[0][1], self.distance(liney[0][0], liney[0][1], line[0][0], liney[0][1])])
                elif (min(liney[0][0], liney[1][0]) - 0.01 < line[1][0] < max(liney[0][0], liney[1][0]) + 0.01) and abs(
                        line[1][1] - liney[1][1]) < n:
                    NodesList1.append(
                        [i, -1, line[1][0], liney[0][1], self.distance(liney[0][0], liney[0][1], line[1][0], liney[0][1])])
            i += 1
        j = 0
        for liney in vertical_lines:
            for line in gong_lines:
                if (min(liney[0][1], liney[1][1]) - 0.01 < line[0][1] < max(liney[0][1], liney[1][1]) + 0.01) and abs(
                        line[0][0] - liney[0][0]) < n:
                    NodesList1.append([-1, j, liney[0][0], line[0][1], abs(liney[0][1] - line[0][1])])
                    # print(100000)
                elif (min(liney[0][1], liney[1][1]) - 0.01 < line[1][1] < max(liney[0][1], liney[1][1]) + 0.01) and abs(
                        line[1][0] - liney[1][0]) < n:
                    NodesList1.append([-1, j, liney[1][0], line[1][1], abs(liney[0][1] - line[1][1])])
                    # print(100000)
            j += 1

        for i in range(len(NodesList1) - 1, -1, -1):
            for j in range(len(NodesList1) - 1, -1, -1):

                if i != j and ((NodesList1[i][2] == NodesList1[j][2] and NodesList1[i][3] == NodesList1[j][3] and
                                NodesList1[i][4] == NodesList1[j][4]) and (
                                       (NodesList1[i][0] == NodesList1[j][0] and NodesList1[i][0] != -1) or (
                                       NodesList1[i][1] == NodesList1[j][1] and NodesList1[i][0] != -1))):
                    NodesList1.pop(j)
                if i ==len(NodesList1)-1:break
        self.NodesList1=NodesList1
        return NodesList1

    def LineCut(self,Lines, NodesList1):
        k = len(Lines)
        Line = Lines
        # print(Lin)
        NewList = []
        NewLineList = []
        if Line == []: return
        if abs(Line[0][0][0] - Line[0][1][0]) < 0.01:
            NodesList1.sort(key=lambda x: (-x[1], -x[4]), reverse=True)
            print(NodesList1)
            for i in range(k):
                NewList.append([Line[i][0][0], Line[i][0][1], 0])
                for n in NodesList1:
                    X = n[1]
                    if int(X) == i:
                        NewList.append([n[2], n[3], 1])
                        NewList.append([n[2], n[3], 1])
                NewList.append([Line[i][1][0], Line[i][1][1], 0])
            i = 0
        elif abs(Line[0][0][1] - Line[0][1][1]) < 0.01:
            NodesList1.sort(key=lambda x: (-x[0], -x[4]), reverse=True)
            # print(Line)
            # print(500,NodesList1)
            for i in range(k):
                NewList.append([Line[i][0][0], Line[i][0][1], 0])
                for n in NodesList1:
                    X = n[0]
                    if int(X) == i:
                        NewList.append([n[2], n[3], 1])
                        NewList.append([n[2], n[3], 1])
                        # print([n[2], n[3], 1])
                NewList.append([Line[i][1][0], Line[i][1][1], 0])
        i = 0
        while i < len(NewList):
            NewLineList.append([NewList[i], NewList[i + 1]])
            i += 2
        i = 0
        # print(200,NewLineList)
        while i < (len(NewLineList)):
            # if NewLineList[i][0]=NewLineList[i][1]
            if [NewLineList[i][0][0], NewLineList[i][0][1]] == [NewLineList[i][1][0], NewLineList[i][1][1]]:
                # if NewLineList[i][0][2] == 0:
                NewLineList.pop(i)
                continue
                # print(000)
            i += 1
        i = j = 0
        while i < len(NewLineList):
            while j < (len(NewLineList) - 1):
                if i != j and [NewLineList[i][0], NewLineList[i][1]] == [NewLineList[j][0], NewLineList[j][1]]:
                    NewLineList.pop(j)
                else:
                    j += 1
            i += 1
        return NewLineList

    def delVLine(self,level_lines, vertical_lines, border_lines, gong_lines, gerberLayer_gtl):
        l_lines = copy.deepcopy(level_lines)
        v_lines = copy.deepcopy(vertical_lines)
        '去除水平微割线'
        if level_lines != None:
            level_y = []
            for i in range(len(l_lines)):
                level_y.append((l_lines[i][0][1] + l_lines[i][1][1]) / 2)
            list=[]
            for line in border_lines:
                list.append(line[0][1])
                list.append(line[1][1])
            level_y.append(min(list))
            level_y.append(max(list))
            level_y.sort()
            for i in range((len(level_y) - 1), 0, -1):
                if abs(level_y[i] - level_y[i - 1]) < 0.01:
                    level_y.pop(i)
            Rectangle_y = []
            for i in range(len(level_lines)):
                for j in range(len(level_y)):
                    if abs(level_lines[i][0][1] - level_y[j]) < 0.01:
                        n = j + 1
                        m = j - 1

                        if (abs(level_lines[i][0][1] - level_y[len(level_y) - 1]) < 0.01) or (
                                abs(level_lines[i][0][1] - level_y[0]) < 0.01) or n > len(level_y) - 1: n = m = j
                        if m < 0: m = 0
                        Rectangle_y.append([[min(level_lines[i][0][0], level_lines[i][1][0]),
                                             max(level_lines[i][0][0], level_lines[i][1][0]),
                                             min(level_lines[i][0][1], level_y[n]),
                                             max(level_lines[i][0][1], level_y[n])], \
                                            [min(level_lines[i][0][0], level_lines[i][1][0]),
                                             max(level_lines[i][0][0], level_lines[i][1][0]),
                                             min(level_lines[i][0][1], level_y[m]),
                                             max(level_lines[i][0][1], level_y[m])], i])

            Rectangle_level = []
            for i in range(len(Rectangle_y)):
                Rectangle_1 = []
                l = (abs(Rectangle_y[i][0][1]) - abs(Rectangle_y[i][0][0])) / 3
                Rectangle_1 = [[Rectangle_y[i][0][0], Rectangle_y[i][0][0] + l, Rectangle_y[i][0][2], Rectangle_y[i][0][3]], \
                               [Rectangle_y[i][0][0] + l, Rectangle_y[i][0][0] + 2 * l, Rectangle_y[i][0][2],
                                Rectangle_y[i][0][3]], \
                               [Rectangle_y[i][0][0] + 2 * l, Rectangle_y[i][0][1], Rectangle_y[i][0][2],
                                Rectangle_y[i][0][3]]]

                Rectangle_2 = [[Rectangle_y[i][1][0], Rectangle_y[i][1][0] + l, Rectangle_y[i][1][2], Rectangle_y[i][1][3]], \
                               [Rectangle_y[i][1][0] + l, Rectangle_y[i][1][0] + 2 * l, Rectangle_y[i][1][2],
                                Rectangle_y[i][1][3]], \
                               [Rectangle_y[i][1][0] + 2 * l, Rectangle_y[i][1][1], Rectangle_y[i][1][2],
                                Rectangle_y[i][1][3]]]
                # print(Rectangle_1)
                Rectangle_1_y = []
                for rectangle in Rectangle_1:
                    gong_y = []
                    for line in gong_lines:

                        if not (max(line[0][0], line[1][0]) <= rectangle[0] or min(line[0][0], line[1][0]) >= rectangle[1]) \
                                and (rectangle[2] <= line[0][1] < rectangle[3] and rectangle[2] <= line[1][1] < rectangle[3]):
                            gong_y.append(abs(line[0][1] - rectangle[2]))
                            gong_y.append(abs(line[1][1] - rectangle[2]))
                            # print(2037,line)
                            # print(abs(line[0][1]-rectangle[2]),abs(line[1][1]-rectangle[2]))
                    if gong_y != []:
                        Rectangle_1_y.append(min(gong_y))
                    else:
                        Rectangle_1_y.append(rectangle[3] - rectangle[2])

                Rectangle_2_y = []
                for rectangle in Rectangle_2:
                    gong_y = []
                    for line in gong_lines:
                        if not (max(line[0][0], line[1][0]) <= rectangle[0] or min(line[0][0], line[1][0]) >= rectangle[1]) \
                                and (rectangle[2] <= line[0][1] < rectangle[3] and rectangle[2] < line[1][1] <= rectangle[3]):
                            gong_y.append(abs(line[0][1] - rectangle[3]))
                            gong_y.append(abs(line[1][1] - rectangle[3]))
                            # print(abs(line[0][1]-rectangle[2]),abs(line[1][1]-rectangle[2]))
                    if gong_y != []:
                        Rectangle_2_y.append(min(gong_y))
                    else:
                        Rectangle_2_y.append(rectangle[3] - rectangle[2])
                # print(Rectangle_2_y)
                Rectangle_level.append([[[Rectangle_y[i][0][0], Rectangle_y[i][0][0] + l, Rectangle_y[i][0][2],
                                          Rectangle_y[i][0][2] + Rectangle_1_y[0]], \
                                         [Rectangle_y[i][0][0] + l, Rectangle_y[i][0][0] + 2 * l, Rectangle_y[i][0][2],
                                          Rectangle_y[i][0][2] + Rectangle_1_y[1]], \
                                         [Rectangle_y[i][0][0] + 2 * l, Rectangle_y[i][0][1], Rectangle_y[i][0][2],
                                          Rectangle_y[i][0][2] + Rectangle_1_y[2]]], \
                                        [[Rectangle_y[i][0][0], Rectangle_y[i][0][0] + l,
                                          Rectangle_y[i][0][2] - Rectangle_2_y[0], Rectangle_y[i][0][2]], \
                                         [Rectangle_y[i][0][0] + l, Rectangle_y[i][0][0] + 2 * l,
                                          Rectangle_y[i][0][2] - Rectangle_2_y[1], Rectangle_y[i][0][2]], \
                                         [Rectangle_y[i][0][0] + 2 * l, Rectangle_y[i][0][1],
                                          Rectangle_y[i][0][2] - Rectangle_2_y[2], Rectangle_y[i][0][2]]]])
            # print(Rectangle_level)
            Rectangle_gtl = []
            for primitive in gerberLayer_gtl.primitives:
                if primitive.level_polarity == 'clear':
                    continue
                # Rectangle_gtl.append(primitive.bounding_box)
                if type(primitive) == gerber.primitives.Line :
                    Rectangle_gtl.append([[primitive.start[0]- 0.004,primitive.start[0]+0.004],[primitive.start[1]-0.004,primitive.start[1]+0.004]])
                # elif type(primitive) == gerber.primitives.Region :
                #     Rectangle_gtl.append([[primitive.primitives[0].start[0] - 0.004, primitive.primitives[0].start[0] + 0.004],[primitive.primitives[0].start[1] - 0.004, primitive.primitives[0].start[1] + 0.004]])
                #     print([[primitive.start[0],primitive.start[0]],[primitive.start[1]-0.004,primitive.start[1]+0.004]])
                #     Rectangle_gtl.append([[primitive.end[0] - 0.004, primitive.end[0] + 0.004],[primitive.end[1] - 0.004, primitive.end[1] + 0.004]])

            num = []
            for i in range(len(Rectangle_level)):
                num_up = 0
                num_down = 0
                for j in range(len(Rectangle_gtl)):
                    if (Rectangle_gtl[j][0][0] > Rectangle_level[i][0][0][0] and Rectangle_gtl[j][0][1] <
                        Rectangle_level[i][0][0][1] \
                        and Rectangle_gtl[j][1][0] > Rectangle_level[i][0][0][2] and Rectangle_gtl[j][1][1] < \
                        Rectangle_level[i][0][0][3]) or (
                            Rectangle_gtl[j][0][0] > Rectangle_level[i][0][1][0] and Rectangle_gtl[j][0][1] <
                            Rectangle_level[i][0][1][1] \
                            and Rectangle_gtl[j][1][0] > Rectangle_level[i][0][1][2] and Rectangle_gtl[j][1][1] < \
                            Rectangle_level[i][0][1][3]) or (
                            Rectangle_gtl[j][0][0] > Rectangle_level[i][0][2][0] and Rectangle_gtl[j][0][1] <
                            Rectangle_level[i][0][2][1] \
                            and Rectangle_gtl[j][1][0] > Rectangle_level[i][0][2][2] and Rectangle_gtl[j][1][1] < \
                            Rectangle_level[i][0][2][3]):
                        num_up = 1
                    # print(Rectangle_gtl[j][1][0],Rectangle_level[i][1][0][2])
                    if (Rectangle_gtl[j][0][0] > Rectangle_level[i][1][0][0] and Rectangle_gtl[j][0][1] <
                        Rectangle_level[i][1][0][1] \
                        and Rectangle_gtl[j][1][0] > Rectangle_level[i][1][0][2] and Rectangle_gtl[j][1][1] <
                        Rectangle_level[i][1][0][3]) \
                            or ((Rectangle_gtl[j][0][0] > Rectangle_level[i][1][1][0] and Rectangle_gtl[j][0][1] <
                                 Rectangle_level[i][1][1][1] \
                                 and Rectangle_gtl[j][1][0] > Rectangle_level[i][1][1][2] and Rectangle_gtl[j][1][1] <
                                 Rectangle_level[i][1][1][3])) or \
                            (Rectangle_gtl[j][0][0] > Rectangle_level[i][0][2][0] and Rectangle_gtl[j][0][1] <
                             Rectangle_level[i][0][2][1] and Rectangle_gtl[j][1][0] > Rectangle_level[i][1][2][2] and
                             Rectangle_gtl[j][1][1] <
                             Rectangle_level[i][1][2][3]):
                        num_down = 1
                if num_up + num_down == 2: num.append(i)
            # print(len(Rectangle_level))
            for i in range(len(level_lines)-1, -1, -1):
                if i in num:
                    level_lines.pop(i)
                    Rectangle_y.pop(i)
            self.Rectangle_gtl=Rectangle_gtl
            #去支撑柱
            num=[]
            g1=[]
            i=0
            for rectangle in Rectangle_y:
                gong_1 = []
                gong_2=[]
                # if rectangle[2]!=35:continue
                # print(rectangle)
                g2=[]
                for line in gong_lines:
                    # if line in border_lines:
                    #     # print(100)
                    #     continue
                    if not (max(line[0][0], line[1][0]) <= rectangle[0][0]+1/7*(rectangle[0][1]-rectangle[0][0]) or min(line[0][0], line[1][0]) >= rectangle[0][1]-1/7*(rectangle[0][1]-rectangle[0][0])) \
                            and (rectangle[0][2] <= line[0][1] <rectangle[0][3] and rectangle[0][2] <= line[1][1]< rectangle[0][3] ):
                        if line in border_lines:gong_1.append(10)
                        else:
                            gong_1.append(abs(line[0][1] - rectangle[0][2]))
                            gong_1.append(abs(line[1][1] - rectangle[0][2]))
                    elif not (max(line[0][0], line[1][0]) <= rectangle[1][0]+1/7*(rectangle[1][1]-rectangle[1][0]) or min(line[0][0], line[1][0]) >= rectangle[1][1]-1/7*(rectangle[1][1]-rectangle[1][0])) \
                            and ( rectangle[1][2]<line[0][1] <= rectangle[1][3] and  rectangle[1][2]<line[1][1] <= rectangle[1][3]):
                        if line in border_lines: gong_1.append(10)
                        else:
                            gong_2.append(abs(line[0][1] - rectangle[1][3]))
                            gong_2.append(abs(line[1][1] - rectangle[1][3]))
                        # print(abs(line[0][1]-rectangle[2]),abs(line[1][1]-rectangle[2]))
                if gong_1 != []:
                    g1=min(gong_1)
                else:
                    g1=10
                if gong_2 != []:
                    g2=min(gong_2)
                else:
                    g2=10
                L1_y=10
                L2_y=10
                for j in range(len(Rectangle_gtl)):
                    if (Rectangle_gtl[j][0][0] > rectangle[0][0] and Rectangle_gtl[j][0][1] <
                            rectangle[0][1] \
                            and Rectangle_gtl[j][1][0] > rectangle[0][2] and Rectangle_gtl[j][1][1] < \
                            rectangle[0][3]):
                        # print(Rectangle_gtl[j])
                        if abs(Rectangle_gtl[j][1][0]-rectangle[0][2])<L1_y: L1_y=abs(Rectangle_gtl[j][1][0]-rectangle[0][2])
                        if abs(Rectangle_gtl[j][1][1] - rectangle[0][2]) < L1_y: L1_y = abs(Rectangle_gtl[j][1][1] - rectangle[0][2])

                    elif (Rectangle_gtl[j][0][0] > rectangle[1][0] and Rectangle_gtl[j][0][1] <
                            rectangle[1][1] \
                            and Rectangle_gtl[j][1][0] > rectangle[1][2] and Rectangle_gtl[j][1][1] < \
                            rectangle[1][3]):
                        if abs(Rectangle_gtl[j][1][0] - rectangle[1][3]) < L2_y: L2_y = abs(Rectangle_gtl[j][1][0] - rectangle[1][3])
                        if abs(Rectangle_gtl[j][1][1] - rectangle[1][3]) < L2_y: L2_y = abs(Rectangle_gtl[j][1][1] - rectangle[1][3])
                    # if Rectangle_gtl[j][0][0]>rectangle[1][0]-0.5and Rectangle_gtl[j][0][0]<rectangle[1][0] and Rectangle_gtl[j][1][1]>rectangle[1][1]-1and Rectangle_gtl[j][1][1]<rectangle[1][1]:
                    #     L2_y=100
                # print(g1,L1_y,g2,L2_y,Rectangle_y[i])
                # print(bool((g1 < L1_y and g2 >= L2_y) or (g1 >= L1_y and g2 < L2_y)or \
                #         (abs(level_lines[i][1][0]-level_lines[i][0][0])<0.1)))
                # if (g1 <= L1_y and g2 >= L2_y) or (g1 >= L1_y and g2 <= L2_y)
                if (((g1 < L1_y and g2 >= L2_y) or (g1 >= L1_y and g2 < L2_y))or \
                        (abs(level_lines[i][1][0]-level_lines[i][0][0])<0.1) ): pass
                # if (g1 < L1_y or  g2 < L2_y):pass
                else:   num.append(i)
                i+=1
            # print(num)
            for i in range(len(level_lines) - 1, -1, -1):
                if i in num:
                    level_lines.pop(i)
                    Rectangle_y.pop(i)
            # print(Rectangle_y)
        # print(num)


        '去除竖直微割线'
        if vertical_lines != None:
            vertical_x = []
            # print(200,v_lines)
            for i in range(len(v_lines)):
                vertical_x.append((v_lines[i][0][0] + v_lines[i][1][0]) / 2)
            list = []
            for line in border_lines:
                list.append(line[0][0])
                list.append(line[1][0])
            vertical_x.append(min(list))
            vertical_x.append(max(list))

            vertical_x.sort()
            for i in range((len(vertical_x) - 1), 0, -1):
                if abs(vertical_x[i] - vertical_x[i - 1]) < 0.01:
                    vertical_x.pop(i)
            Rectangle_x = []
            # print(10000, vertical_x)
            for i in range(len(vertical_lines)):
                # print(vertical_lines[i])
                for j in range(len(vertical_x)):
                    if abs(vertical_lines[i][0][0] - vertical_x[j]) < 0.01:


                        n = j + 1
                        m = j - 1
                        if (abs(vertical_lines[i][0][0] - vertical_x[len(vertical_x) - 1]) < 0.01) or (
                                abs(vertical_lines[i][0][0] - vertical_x[0]) < 0.01) or n > len(vertical_x) - 1: n = m = j
                        # print(level_y[0],(abs(level_lines[i][0][1]-level_y[0])< 0.01))
                        if m < 0: m = 0
                        Rectangle_x.append(
                            [[min(vertical_lines[i][0][0], vertical_x[n]), max(vertical_lines[i][0][0], vertical_x[n]), \
                              min(vertical_lines[i][0][1], vertical_lines[i][1][1]),
                              max(vertical_lines[i][0][1], vertical_lines[i][1][1])], \
                             [min(vertical_lines[i][0][0], vertical_x[m]), max(vertical_lines[i][0][0], vertical_x[m]), \
                              min(vertical_lines[i][0][1], vertical_lines[i][1][1]),
                              max(vertical_lines[i][0][1], vertical_lines[i][1][1])], i])
                        # print(j,n,m,level_y[j],Rectangle_x)
                        # print(Rectangle_x[i])
            Rectangle_vertical = []
            for i in range(len(Rectangle_x)):  # len(Rectangle_x)
                l = (abs(Rectangle_x[i][0][3]) - abs(Rectangle_x[i][0][2])) / 3
                Rectangle_1 = [[Rectangle_x[i][0][0], Rectangle_x[i][0][1], Rectangle_x[i][0][2], Rectangle_x[i][0][2] + l], \
                               [Rectangle_x[i][0][0], Rectangle_x[i][0][1], Rectangle_x[i][0][2] + l,
                                Rectangle_x[i][0][2] + 2 * l], \
                               [Rectangle_x[i][0][0], Rectangle_x[i][0][1], Rectangle_x[i][0][2] + 2 * l,
                                Rectangle_x[i][0][3]]]

                Rectangle_2 = [[Rectangle_x[i][1][0], Rectangle_x[i][1][1], Rectangle_x[i][1][2], Rectangle_x[i][1][2] + l], \
                               [Rectangle_x[i][1][0], Rectangle_x[i][1][1], Rectangle_x[i][1][2] + l,
                                Rectangle_x[i][1][2] + 2 * l], \
                               [Rectangle_x[i][1][0], Rectangle_x[i][1][1], Rectangle_x[i][1][2] + 2 * l,
                                Rectangle_x[i][1][3]]]
                # print(Rectangle_2)
                Rectangle_1_x = []
                for rectangle in Rectangle_1:
                    gong_x = []
                    for line in gong_lines:

                        if not (max(line[0][1], line[1][1]) <= rectangle[2] or min(line[0][1], line[1][1]) >= rectangle[3]) \
                                and (rectangle[0] <= line[0][0] < rectangle[1] and rectangle[0] < line[1][0] <= rectangle[1]):
                            gong_x.append(abs(line[0][0] - rectangle[0]))
                            gong_x.append(abs(line[1][0] - rectangle[0]))
                            # print(2037,line)
                            # print(abs(line[0][1]-rectangle[2]),abs(line[1][1]-rectangle[2]))
                    if gong_x != []:
                        Rectangle_1_x.append(min(gong_x))
                    else:
                        Rectangle_1_x.append(rectangle[1] - rectangle[0])


                Rectangle_2_x = []
                for rectangle in Rectangle_2:
                    gong_x = []
                    for line in gong_lines:
                        if not (max(line[0][1], line[1][1]) <= rectangle[2] or min(line[0][1], line[1][1]) >= rectangle[3]) \
                                and (rectangle[0] <= line[0][0] < rectangle[1] and rectangle[0] < line[1][0] <= rectangle[1]):
                            gong_x.append(abs(line[0][0] - rectangle[1]))
                            gong_x.append(abs(line[1][0] - rectangle[1]))
                            # print(abs(line[0][1]-rectangle[2]),abs(line[1][1]-rectangle[2]))
                    if gong_x != []:
                        Rectangle_2_x.append(min(gong_x))
                    else:
                        Rectangle_2_x.append(rectangle[1] - rectangle[0])
                # print(Rectangle_2_y)

                Rectangle_vertical.append([[[Rectangle_x[i][0][0], Rectangle_x[i][0][0] + Rectangle_1_x[0],
                                             Rectangle_x[i][0][2], Rectangle_x[i][0][2] + l], \
                                            [Rectangle_x[i][0][0], Rectangle_x[i][0][0] + Rectangle_1_x[1],
                                             Rectangle_x[i][0][2] + l, Rectangle_x[i][0][2] + 2 * l], \
                                            [Rectangle_x[i][0][0], Rectangle_x[i][0][0] + Rectangle_1_x[2],
                                             Rectangle_x[i][0][2] + 2 * l, Rectangle_x[i][0][3]]], \
                                           [[Rectangle_x[i][1][1] - Rectangle_2_x[0], Rectangle_x[i][1][1],
                                             Rectangle_x[i][1][2], Rectangle_x[i][1][2] + l], \
                                            [Rectangle_x[i][1][1] - Rectangle_2_x[1], Rectangle_x[i][1][1],
                                             Rectangle_x[i][1][2] + l, Rectangle_x[i][1][2] + 2 * l], \
                                            [Rectangle_x[i][1][1] - Rectangle_2_x[2], Rectangle_x[i][1][1],
                                             Rectangle_x[i][1][2] + 2 * l, Rectangle_x[i][1][3]]]])
                # print(Rectangle_vertical[i])

            num = []
            for i in range(len(Rectangle_vertical)):#
                num_up = 0
                num_down = 0
                for j in range(len(Rectangle_gtl)):
                    if (Rectangle_gtl[j][0][0] > Rectangle_vertical[i][0][0][0] and Rectangle_gtl[j][0][1] <
                        Rectangle_vertical[i][0][0][1] \
                        and Rectangle_gtl[j][1][0] > Rectangle_vertical[i][0][0][2] and Rectangle_gtl[j][1][1] < \
                        Rectangle_vertical[i][0][0][3]) or (
                            Rectangle_gtl[j][0][0] > Rectangle_vertical[i][0][1][0] and Rectangle_gtl[j][0][1] <
                            Rectangle_vertical[i][0][1][1] \
                            and Rectangle_gtl[j][1][0] > Rectangle_vertical[i][0][1][2] and Rectangle_gtl[j][1][1] < \
                            Rectangle_vertical[i][0][1][3]) or (
                            Rectangle_gtl[j][0][0] > Rectangle_vertical[i][0][2][0] and Rectangle_gtl[j][0][1] <
                            Rectangle_vertical[i][0][2][1] \
                            and Rectangle_gtl[j][1][0] > Rectangle_vertical[i][0][2][2] and Rectangle_gtl[j][1][1] < \
                            Rectangle_vertical[i][0][2][3]):
                        num_up = 1
                    # print(Rectangle_gtl[j][1][0],Rectangle_level[i][1][0][2])
                    if (Rectangle_gtl[j][0][0] > Rectangle_vertical[i][1][0][0] and Rectangle_gtl[j][0][1] <
                        Rectangle_vertical[i][1][0][1] \
                        and Rectangle_gtl[j][1][0] > Rectangle_vertical[i][1][0][2] and Rectangle_gtl[j][1][1] <
                        Rectangle_vertical[i][1][0][3]) \
                            or ((Rectangle_gtl[j][0][0] > Rectangle_vertical[i][1][1][0] and Rectangle_gtl[j][0][1] <
                                 Rectangle_vertical[i][1][1][1] \
                                 and Rectangle_gtl[j][1][0] > Rectangle_vertical[i][1][1][2] and Rectangle_gtl[j][1][1] <
                                 Rectangle_vertical[i][1][1][3])) or \
                            (Rectangle_gtl[j][0][0] > Rectangle_vertical[i][1][2][0] and Rectangle_gtl[j][0][1] <
                             Rectangle_vertical[i][1][2][1] and Rectangle_gtl[j][1][0] > Rectangle_vertical[i][1][2][2] and
                             Rectangle_gtl[j][1][1] <Rectangle_vertical[i][1][2][3]):

                        num_down = 1

                if num_up + num_down == 2: num.append(i)

            for i in range(len(vertical_lines), -1, -1):
                if i in num:
                    vertical_lines.pop(i)
                    Rectangle_x.pop(i)
            # print(len(vertical_lines),len(Rectangle_x))
            # 去支撑柱
            num = []
            g1 = []
            i = 0
            for rectangle in Rectangle_x:
                gong_1 = []
                gong_2 = []
                g2 = []
                for line in gong_lines:
                    if not (max(line[0][1], line[1][1]) < rectangle[0][2]+1/4*(rectangle[0][3]-rectangle[0][2]) or min(line[0][1], line[1][1]) >
                            rectangle[0][3]-1/4*(rectangle[0][3]-rectangle[0][2]))\
                            and (rectangle[0][0] <= line[0][0] < rectangle[0][1] and rectangle[0][0] <= line[1][0] <
                                 rectangle[0][1]):
                        if line in border_lines:gong_1.append(10)
                        else:
                            gong_1.append(abs(line[0][0] - rectangle[0][0]))
                            gong_1.append(abs(line[1][0] - rectangle[0][0]))
                    elif not (max(line[0][1], line[1][1]) <= rectangle[1][2]+1/4*(rectangle[0][3]-rectangle[0][2]) or min(line[0][1], line[1][1]) >=
                            rectangle[1][3]-1/4*(rectangle[0][3]-rectangle[0][2])) \
                            and (rectangle[1][0] < line[0][0] <= rectangle[1][1] and rectangle[1][0] < line[1][0] <=
                                 rectangle[1][1]):
                        if line in border_lines:gong_1.append(10)
                        else:
                            gong_2.append(abs(line[0][0] - rectangle[1][1]))
                            gong_2.append(abs(line[1][0] - rectangle[1][1]))
                        # print(abs(line[0][1]-rectangle[2]),abs(line[1][1]-rectangle[2]))
                # print(gong_1)
                if gong_1 != []:
                    g1 = min(gong_1)
                else:
                    g1 = 10
                # print(g1)
                if gong_2 != []:
                    g2 = min(gong_2)
                else:
                    g2 = 10
                L1_y = 10
                L2_y = 10
                for j in range(len(Rectangle_gtl)):
                    if (Rectangle_gtl[j][0][0] > rectangle[0][0] and Rectangle_gtl[j][0][1] <
                            rectangle[0][1] \
                            and Rectangle_gtl[j][1][0] > rectangle[0][2] and Rectangle_gtl[j][1][1] < \
                            rectangle[0][3]):
                        # print(Rectangle_gtl[j])
                        if abs(Rectangle_gtl[j][0][0]-rectangle[0][0])<L1_y: L1_y = abs(Rectangle_gtl[j][0][0]-rectangle[0][0])
                        if abs(Rectangle_gtl[j][0][1] - rectangle[0][0]) < L1_y: L1_y = abs(Rectangle_gtl[j][0][1] - rectangle[0][0])
                    elif (Rectangle_gtl[j][0][0] > rectangle[1][0] and Rectangle_gtl[j][0][1] <
                          rectangle[1][1] \
                          and Rectangle_gtl[j][1][0] > rectangle[1][2] and Rectangle_gtl[j][1][1] < \
                          rectangle[1][3]):
                        if abs(Rectangle_gtl[j][0][0] - rectangle[1][1]) < L2_y:
                            L2_y = abs(Rectangle_gtl[j][0][0] - rectangle[1][1])
                        if abs(Rectangle_gtl[j][0][1] - rectangle[1][1]) < L2_y:
                            L2_y = abs(Rectangle_gtl[j][0][1] - rectangle[1][1])
                print(g1, L1_y, g2, L2_y, rectangle)
                # print(bool(g1 < L1_y or g2 < L2_y))
                if ((g1 < L1_y and g2 >= L2_y) or (g1 >= L1_y and g2 < L2_y)):pass
                else:    num.append(i)
                i += 1
            for i in range(len(vertical_lines) - 1, -1, -1):
                if i in num:
                    Rectangle_x.pop(i)

    def addlines(self, level_lines, vertical_lines, gong_lines):
        if level_lines != None:
            for line in level_lines:
                gong_lines.append(line)
        if vertical_lines != None:
            for line in vertical_lines:
                gong_lines.append(line)
        gong_lines1 = []
        for line in gong_lines:
            gong_lines1.append([line])
        return gong_lines1


def draw():
    for line in gong_lines:
        glColor3f(1.0, 0.0, 0.0)
        glBegin(GL_LINE_STRIP)
        glVertex2f(line[0][0][0], line[0][0][1])
        glVertex2f(line[0][1][0], line[0][1][1])
        glEnd()
    # print(len(level_lines))
    # for line in level_lines:
    #     glColor3f(0.2 + random.randint(0, 1), 0.1 + random.randint(0, 1), 0.1 + random.randint(0, 1))
    #     glBegin(GL_LINE_STRIP)
    #     glVertex2f(line[0][0], line[0][1])
    #     glVertex2f(line[1][0], line[1][1])
    #     glEnd()

    # for line in vertical_lines:
    #     glColor3f(0.2 + random.randint(0, 1), 0.1 + random.randint(0, 1), 0.1 + random.randint(0, 1))
    #     glBegin(GL_LINE_STRIP)
    #     glVertex2f(line[0][0], line[0][1])
    #     glVertex2f(line[1][0], line[1][1])
    #     glEnd()

    # for line in border_lines:
    #     glColor3f(1.0, 1.0, 1.0)
    #     glBegin(GL_LINE_STRIP)
    #     glVertex2f(line[0][0], line[0][1])
    #     glVertex2f(line[1][0], line[1][1])
    #     glEnd()

    # for i in range(len(findlines.Rectangle_gtl)):#len(findlines.Rectangle_gtl)
    #     glColor3f(1.0, 1.0, 1.0)
    #     glBegin(GL_POLYGON)
    #     glVertex2f(findlines.Rectangle_gtl[i][0][0], findlines.Rectangle_gtl[i][1][1])
    #     glVertex2f(findlines.Rectangle_gtl[i][0][1], findlines.Rectangle_gtl[i][1][1])
    #     glVertex2f(findlines.Rectangle_gtl[i][0][1], findlines.Rectangle_gtl[i][1][0])
    #     glVertex2f(findlines.Rectangle_gtl[i][0][0], findlines.Rectangle_gtl[i][1][0])
    #     glEnd()
    #
    # for line in new_vertical_lines:
    #     glColor3f(1.0, 1.0, 1.0)
    #     glBegin(GL_LINE_STRIP)
    #     glVertex2f(line[0][0], line[0][1])
    #     glVertex2f(line[1][0], line[1][1])
    #     glEnd()

    # glBegin(GL_LINE_STRIP)
    # glColor3f(1.0, 1.0, 1.0)
    # glVertex2f(0.19685,0.8189)
    # glVertex2f(0.19685, 1.29134)
    # glEnd()

    # glBegin(GL_POLYGON)
    # glColor3f(1.0, 1.0, 1.0)
    # glVertex2f(0.292714, 0)
    # glVertex2f(0.292714, 0.23622)
    # glVertex2f(0, 0.23622)
    # glVertex2f(0,0 )
    # glEnd()
    #
    # glBegin(GL_POLYGON)
    # glColor3f(1.0, 1.0, 1.0)
    # glVertex2f(1.776246, 1.3372433333333333)
    # glVertex2f(1.776246, 1.3109966666666668)
    # glVertex2f(1.96851, 1.3109966666666668)
    # glVertex2f(1.96851,1.3372433333333333 )
    # glEnd()
    #
    # glBegin(GL_POLYGON)
    # glColor3f(1.0, 1.0, 1.0)
    # glVertex2f(1.81103, 1.3372433333333333)
    # glVertex2f(1.81103, 1.36349)
    # glVertex2f(1.96851, 1.36349)
    # glVertex2f(1.96851, 1.3109966666666668)
    # glEnd()
    # print(len(findlines.NodesList1))

    # for node in findlines.NodesList1:
    #     glPointSize(4)
    #     glColor3f(0.0, 1.0, 0.0)
    #     glBegin(GL_POINTS)
    #     glVertex2f(node[2], node[3])
    #     glEnd()
    glFlush()


def show(gerberLayer_gko):
    show_x_min = (gerberLayer_gko.bounds[0][0] - 0.5)
    show_x_max = (gerberLayer_gko.bounds[0][1] + 0.5)
    show_y_min = (gerberLayer_gko.bounds[1][0] - 0.5)
    show_y_max = (gerberLayer_gko.bounds[1][1] + 0.5)
    return show_x_min, show_x_max, show_y_min, show_y_max


if __name__ == '__main__':
    with open(f"C:\\Users\\dkm\\Desktop\\GerberFile\\ALL-2C2167292\\gtl", "r") as f1:
        #ALL-1W2308512,ALL-2C2316028,ALL-2W1556365,ALL-2W2108193,ALL-2W2108846,ALL-2W2173486,ALL-2W2270924,ALL-2W2277545,
        data1 = f1.read()
        gerberLayer_gtl = gerber.loads(data1, 'gko')
    # with open(f"C:\\Users\\zhang\\Desktop\\Zou\\TestData123\\JP-2W2473623_2021101915103985734949\\gko", "r") as f4:
    with open(f"C:\\Users\\dkm\\Desktop\\GerberFile\\ALL-2C2167292\\gko", "r") as f4:
        data4 = f4.read()
        gerberLayer_gko = gerber.loads(data4, 'gko')
        afew = 0
    findlines = GKOGerberProcess(gerberLayer_gko,gerberLayer_gtl)
    findlines.run()
    gong_lines, level_lines, vertical_lines = findlines.gong_lines, findlines.level_lines, findlines.vertical_lines
    # print(gong_lines)
    # print(100,vertical_lines)
    glutInit(sys.argv)  # 初始化
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)  # 设置显示模式
    glutInitWindowPosition(0, 0)  # 窗口打开的位置，左上角坐标在屏幕坐标
    glutInitWindowSize(800, 700)  # 窗口大小
    glutCreateWindow(b"Function Plotter")  # 窗口名字，二进制
    glutDisplayFunc(draw)  # 设置当前窗口的显示回调
    glClearColor(1.0, 1.0, 1.0, 1.0)  # 设置背景颜色
    show_x_min, show_x_max, show_y_min, show_y_max=show(gerberLayer_gko)
    gluOrtho2D(show_x_min, show_x_max, show_y_min, show_y_max)  # 设置显示范围
    glutMainLoop()  # 启动循环'''
