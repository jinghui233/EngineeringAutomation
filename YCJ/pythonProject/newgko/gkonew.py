import math
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import gerber
from ProcessService.RoutLineProcess.RoutLineProcess import RoutLineProcessBase
from ProcessService.RoutLineProcess.GKOGerberProcess2.LinePiceSet import LinePice, LineSet


class NewGko:
    def __init__(self, gerberLayer_gko):
        self.gerberLayer_gko = gerberLayer_gko
        self.run

    def run(self):
        all_lines = self.find_all_line(gerberLayer_gko)
        long_lines, short_lines = self.l_s_lines(all_lines)
        self.all_lines = all_lines

    def find_all_line(self, gerberLayer_gko):
        start_points = []
        end_points = []
        all_lines = []
        for j in range(len(gerberLayer_gko.primitives)):
            if type(gerberLayer_gko.primitives[j]) == gerber.primitives.Line:
                start_points.append(gerberLayer_gko.primitives[j].start)
                end_points.append(gerberLayer_gko.primitives[j].end)
        for h in range(len(start_points)):
            all_lines.append([start_points[h], end_points[h]])
        return all_lines

    def l_s_lines(self, all_lines):
        long_lines = []
        short_lines = []
        for i in range(len(all_lines)-1, -1, -1):
            D = math.sqrt((all_lines[i][0][1]-all_lines[i][1][1])**2 + (all_lines[i][0][0]-all_lines[i][1][0])**2)
            if D > 0.05:
                long_lines.append(all_lines.pop(i))
            else:
                short_lines.append(all_lines.pop(i))
        return long_lines, short_lines

    def line_dispose(self, any_lines):
        d = 0.008
        i = 0
        j = 0
        for i in range(len(any_lines)-1, -1, -1):
            s_pi = any_lines[i][0]
            e_pi = any_lines[i][1]
            for j in range(len(any_lines)):
                s_pj = any_lines[j][0]
                e_pj = any_lines[j][1]
                if (s_pi[0] == e_pi[0]) and (s_pj[0] == e_pj[0]) and (abs(s_pi[0]-s_pj[0]) < d):
                    if not ((max(s_pi[1], e_pi[1]) < (min(s_pj[1], e_pj[1]) - d)) and ((min(s_pi[1], e_pi[1])-d) > max(s_pj[1], e_pj[1]))):
                        s_p = (s_pi[0], min(s_pi[1], e_pi[1], s_pj[1], e_pj[1]))
                        e_p = (s_pj[0], max(s_pi[1], e_pi[1], s_pj[1], e_pj[1]))


def draw():
    for line in lines:
        glColor3f(1.0, 0.0, 0.0)
        glBegin(GL_LINE_STRIP)
        glVertex2f(line[0][0], line[0][1])
        glVertex2f(line[1][0], line[1][1])
        glEnd()
    glFlush()


def show(gk):
    show_x_min = (gk.bounds[0][0] - 0.5)
    show_x_max = (gk.bounds[0][1] + 0.5)
    show_y_min = (gk.bounds[1][0] - 0.5)
    show_y_max = (gk.bounds[1][1] + 0.5)
    return show_x_min, show_x_max, show_y_min, show_y_max


path = r"E:\\GerberFile\\ALL-1W2308512"
with open("{}\\gko".format(path), "r") as f1:
    data1 = f1.read()
    gerberLayer_gko = gerber.loads(data1, 'gko')
if __name__ == '__main__':
    gko = NewGko(gerberLayer_gko)
    lines = gko.find_all_line(gerberLayer_gko)
    set = LineSet()
    for line in lines:
        set.addLine(LinePice(line[0], line[1], 0.003937))
    routdata = RoutLineProcessBase.ToGerberFile2([set], gerberLayer_gko.settings)
    with open("gko", "w") as f:
        f.write(routdata)
    glutInit(sys.argv)  # 初始化
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB)  # 设置显示模式
    glutInitWindowPosition(0, 0)  # 窗口打开的位置，左上角坐标在屏幕坐标
    glutInitWindowSize(800, 800)  # 窗口大小
    glutCreateWindow(b"Function Plotter")  # 窗口名字，二进制
    glutDisplayFunc(draw)  # 设置当前窗口的显示回调
    glClearColor(1.0, 1.0, 1.0, 1.0)  # 设置背景颜色
    show_x_min, show_x_max, show_y_min, show_y_max = show(gerberLayer_gko)
    gluOrtho2D(show_x_min, show_x_max, show_y_min, show_y_max)  # 设置显示范围
    glutMainLoop()  # 启动循环'''
