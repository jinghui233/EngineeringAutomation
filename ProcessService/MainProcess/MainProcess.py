import os
from ProcessService.SupportFuncs.DataContainer import GroupData
from ProcessService.RoutLineProcess.RoutLineProcess import RoutLineProcess
from ProcessService.SupportFuncs.PinBan.PinBan import PinBan, ToGerberFile, ToGerberFile_Arc
from ProcessService.SupportFuncs.PinBan.OrderSet import OrderSet
from ProcessService.SupportFuncs.PinBan.Stephdr import Stephdr
from ProcessService.RoutLineProcess.ArcProcess.ArcProcess import ArcProcess
from ProcessService.RoutLineProcess.LinePiceSet import LinePice, LineSet


class MainProcess:
    def __init__(self, path: str, groupNo: str):
        self.groupData = GroupData(path, groupNo)
        self.routLineProcessList = []
        self.orderSets = []

    def process(self):
        for orderData in self.groupData.orders.values():
            curRoutLineProcess = RoutLineProcess(orderData)
            self.routLineProcessList.append(curRoutLineProcess)
            curRoutLineProcess.process()
            orderSet = OrderSet(curRoutLineProcess.sets, orderData.orderNo, orderData.offset, Stephdr(orderData.stephdr))
            if curRoutLineProcess.success:
                arcProcess = ArcProcess(orderSet.lines)
                arcProcess.PreProc()
                orderSet.lines = arcProcess.GetReturnVal()
            self.orderSets.append(orderSet)
        pinBan = PinBan(self.orderSets, Stephdr(self.groupData.stephdr))
        self.lines = pinBan.Pin()


if __name__ == "__main__":
    path = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile4"
    resultPath = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile5"
    groupDirs = os.listdir(path)
    totle = len(groupDirs)
    index = 0
    for groupDir in groupDirs:
        index = index + 1
        print(f"{index}\\{groupDir}")
        if index < 0:
            continue
        # if groupDir != "JP-2Y2445010":
        #     continue
        mainProcess = MainProcess(f"{path}\{groupDir}", groupDir)
        mainProcess.process()
        gerberstr = ToGerberFile(mainProcess.lines)
        if not os.path.exists(f"{resultPath}\{groupDir}"):
            os.makedirs(f"{resultPath}\{groupDir}")
        open(f"{resultPath}\{groupDir}\{groupDir}_rout", "w").write(gerberstr)

# JP-2W2451167有问题JP-2W2445467 JP-2Y2445010
