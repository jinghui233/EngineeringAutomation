class Singleton(object):
    def __init__(self, cls):
        self._cls = cls
        self._instance = {}

    def __call__(self):
        if self._cls not in self._instance:
            self._instance[self._cls] = self._cls()
        return self._instance[self._cls]


@Singleton
class Cls2(object):
    def __init__(self):
        self.count = 0
        self.sum = 0
        self.list=[]
# 总数:7549  最小距离和:112510.89283422905  平均最小距离:14.904079061362967
