from gerber.gerber_statements import CoordStmt
def ToGerberFile(lineSets, settings):
    # settings = FileSettings()
    primitives = []
    statements = []
    for set in lineSets:
        for pice in set.GetLineSet():
            coord = {"function": None, "x": '%f10' % pice.start[0], "y": '%f10' % pice.start[1], "i": None, "j": None, "op": "D02"}
            coordstmt = CoordStmt.from_dict(coord, settings)
            statements.append(coordstmt)
            coord = {"function": None, "x": '%f10' % pice.end[0], "y": '%f10' % pice.end[1], "i": None, "j": None, "op": "D01"}
            coordstmt = CoordStmt.from_dict(coord, settings)
            statements.append(coordstmt)
            primitives.append(pice.gbLine)
    # gerberFile = GerberFile(statements, self.gkoGbrPcs.gerberLayer.settings, primitives, None)
    writestr = "*\n%FSLAX26Y26*%\n%MOIN*%\n%ADD10C,0.007874*%\n%IPPOS*%\n%LNgko11.gbr*%\n%LPD*%\nG75*\nG54D10*\n"
    for statement in statements:
        strr = statement.to_gerber(settings) + "\n"
        writestr += strr
    return writestr