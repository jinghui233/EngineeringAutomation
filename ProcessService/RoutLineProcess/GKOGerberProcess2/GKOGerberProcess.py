import os
import pickle

import cv2

from ProcessService.RoutLineProcess.GKOGerberProcess2.Debug.DebugFuncs import ToGerberFile
from ProcessService.RoutLineProcess.GKOGerberProcess2.GKOGerberProcessBase import GKOGerberProcessBase
from ProcessService.SupportFuncs.DataContainer import OrderData
from ProcessService.SupportFuncs.ImageGenerater import ImageGenerater
from ProcessService.RoutLineProcess.GKOGerberProcess2.AnalyticGeometry import AnalyticGeometry as AGFuncs
from ProcessService.RoutLineProcess.LinePiceSet import LinePice, LineSet


class GKOGerberProcess(GKOGerberProcessBase):
    def __init__(self, orderData: OrderData):
        GKOGerberProcessBase.__init__(self, orderData)

    def saveData(self, path):
        str = pickle.dumps(self.sets)
        with open(f"{path}/GKOGerberProcessCacheData", "wb") as f:
            f.write(str)

    def loadData(self, path):
        if os.path.exists(f"{path}/GKOGerberProcessCacheData"):
            with open(f"{path}/GKOGerberProcessCacheData", "rb") as f:
                str = f.read()
                self.sets = pickle.loads(str)
                return True
        return False

    def pre_process(self):
        sets = self.sets
        # self.OverLapping_Set(sets, 0)
        # sets = self.Regenerate(sets)
        self.CutSet_Set(sets)
        sets = self.Regenerate(sets)
        self.CutLine_Set(sets)
        sets = self.Regenerate(sets)
        self.OverLapping_Set(sets, 0)
        sets = self.Regenerate(sets)
        self.CutSet_Set(sets)
        sets = self.Regenerate(sets)
        self.CutLine_Set(sets)
        sets = self.Regenerate(sets)
        self.OverLapping_Set(sets, 0)
        sets = self.Regenerate(sets)
        self.CombineSets(sets, 1)
        # self.MovePoints(sets)
        self.sets = sets
        pass

    def edgeOffset(self, vCutSupportArea):
        pass

    def LastProc(self):
        self.CombineSets2(self.sets, 1)
        self.CombineSets2(self.sets, 2)
        self.CombineSets2(self.sets, 4)
        largestbondingBox = ((0, 0), (0, 0))
        additionalest = LineSet()
        for set in self.sets:
            preLine = None
            for line in set.GetLineSet():
                if preLine == None:  # 上一条线为空--继续
                    preLine = line
                    continue
                curLine = line
                if preLine.end == curLine.start:  # 上一条线与当前线完美首尾相连
                    preLine = curLine
                    continue
                if AGFuncs.HVLevel(preLine.angle) > 0.01 and AGFuncs.HVLevel(curLine.angle) > 0.01:  # 是否水平垂直
                    middlePoint = ((preLine.end[0] + curLine.start[0]) / 2, ((preLine.end[1] + curLine.start[1]) / 2))
                    preLine.end = middlePoint
                    curLine.start = middlePoint
                else:  # 是水平垂直的线
                    # if AGFuncs.P_to_P_Distance(preLine.end, curLine.start) >= (preLine.radius + curLine.radius):
                    if preLine.LineLength > curLine.LineLength:
                        curLine.start = preLine.end
                    else:
                        preLine.end = curLine.start
                preLine = curLine
            if set.linesNum < 3:
                return False
            distance = AGFuncs.P_to_P_Distance(set.start, set.end)
            if not distance < set.FirstLine.radius * 10:
                return False
            middlePoint = ((set.end[0] + set.start[0]) / 2, ((set.end[1] + set.start[1]) / 2))
            set.start = middlePoint
            set.end = middlePoint
            if AGFuncs.Maxbonding_box(set.bounding_box, largestbondingBox):
                largestbondingBox = set.bounding_box

        for set in self.sets:
            if not AGFuncs.box1_in_box2(set.bounding_box, largestbondingBox):
                return False
        self.ConLines()
        return True

    def ConLines(self):
        for set in self.sets:
            lines = set.GetLineSet()
            preLine = None
            for j in range(len(lines) - 1, -1, -1):
                curLine = lines[j]
                if preLine == None:  # 上一条线为空--继续
                    preLine = curLine
                    continue
                if abs(preLine.angle - curLine.angle) < 0.0001:
                    lines.remove(preLine)
                    set.linesNum -= 1
                    curLine.end = preLine.end
                preLine = curLine
            if abs(set.FirstLine.angle - set.LastLine.angle) < 0.0001:
                set.start = set.LastLine.start
                lines.remove(set.LastLine)
                set.linesNum -= 1


indexset = [12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 30, 31, 32, 36, 39]
if __name__ == '__main__':
    resultSavePath = f"D:\\ProjectFile\\EngineeringAutomation\\TestData\\TestResult"
    gerberFilePath = f"D:\\ProjectFile\\EngineeringAutomation\\TestData\\GerberFile4"
    index = 0
    orderDirs = os.listdir(f"{gerberFilePath}")
    for orderDir in orderDirs:
        index += 1
        print(f"{index}\\{orderDir}")
        # if index < 11:
        #     continue
        # if not indexset.__contains__(index) and index < 39:
        #     continue
        # if orderDir != "ALL-4W2181383":
        #     continue
        gerbers = dataPrepar(f"{gerberFilePath}\{orderDir}")
        imageGenerater = ImageGenerater(gerbers)
        lineset = GKOGerberProcess(imageGenerater.gerberLayers["gko"])
        lineset.pre_process()
        filestr = ToGerberFile(lineset.sets, lineset.gerberLayer.settings)
        with open(f"{gerberFilePath}\\{orderDir}\\prepcs", 'w') as f:
            f.write(filestr)
        image = imageGenerater.DrawShow(lineset.sets, False, -1)
        image = cv2.flip(image, 0)
        cv2.imwrite(f"{resultSavePath}\\{orderDir}.png", image)
        cv2.imshow("test", image)
        kval = 13
        while kval == 13:
            kval = cv2.waitKey(-1)

