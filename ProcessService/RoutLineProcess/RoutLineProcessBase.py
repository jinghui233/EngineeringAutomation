import os
from typing import List

import cv2
from gerber.gerber_statements import CoordStmt
from gerber.rs274x import FileSettings
from ProcessService.RoutLineProcess.GKOGerberProcess2.LinePiceSet import LineSet
from ProcessService.RoutLineProcess.GKOGerberProcess2.GKOGerberProcess import GKOGerberProcess
from ProcessService.RoutLineProcess.GKOImageProcess2.GKOImageProcess import GKOImageProcess
from ProcessService.SupportFuncs.ImageGenerater import ImageGenerater, dataPrepar
from ProcessService.RoutLineProcess.Debug.SingletonClas import Cls2


class RoutLineProcessBase:
    def __init__(self, imageGenerater: ImageGenerater):
        self.imageGenerater = imageGenerater

    def getLineSets(self, lineSets: List[LineSet], pointPairs):
        newLineSets = []
        for pointPair in pointPairs:
            if len(pointPair) == 2:
                lineset = self.__findset(pointPair, lineSets)
                if lineset != None:
                    newLineSets.append(lineset)
                    lineSets.remove(lineset)
        for lineSet in lineSets:
            if lineSet.IsClose2():
                newLineSets.append(lineSet)
            else:
                aaa = 0
        return newLineSets

    def ToGerberFile(self, sets=None):
        settings = FileSettings()
        settings = self.imageGenerater.gerberLayers["gko"].settings
        if sets is not None:
            lineSets = sets
        else:
            lineSets = self.gkoGbrPcs.sets
        primitives = []
        statements = []
        for set in lineSets:
            for pice in set.GetLineSet():
                coord = {"function": None, "x": '%f10' % pice.start[0], "y": '%f10' % pice.start[1], "i": None, "j": None, "op": "D02"}
                coordstmt = CoordStmt.from_dict(coord, settings)
                statements.append(coordstmt)
                coord = {"function": None, "x": '%f10' % pice.end[0], "y": '%f10' % pice.end[1], "i": None, "j": None, "op": "D01"}
                coordstmt = CoordStmt.from_dict(coord, settings)
                statements.append(coordstmt)
                primitives.append(pice.gbLine)
        # gerberFile = GerberFile(statements, self.gkoGbrPcs.gerberLayer.settings, primitives, None)
        writestr = "*\n%FSLAX26Y26*%\n%MOIN*%\n%ADD10C,0.007874*%\n%IPPOS*%\n%LNgko11.gbr*%\n%LPD*%\nG75*\nG54D10*\n"
        for statement in statements:
            strr = statement.to_gerber(settings) + "\n"
            writestr += strr
        return writestr
    @staticmethod
    def ToGerberFile2(sets,settings):
        settings = settings
        lineSets = sets
        primitives = []
        statements = []
        for set in lineSets:
            for pice in set.GetLineSet():
                coord = {"function": None, "x": '%f10' % pice.start[0], "y": '%f10' % pice.start[1], "i": None, "j": None, "op": "D02"}
                coordstmt = CoordStmt.from_dict(coord, settings)
                statements.append(coordstmt)
                coord = {"function": None, "x": '%f10' % pice.end[0], "y": '%f10' % pice.end[1], "i": None, "j": None, "op": "D01"}
                coordstmt = CoordStmt.from_dict(coord, settings)
                statements.append(coordstmt)
                primitives.append(pice.gbLine)
        # gerberFile = GerberFile(statements, self.gkoGbrPcs.gerberLayer.settings, primitives, None)
        writestr = "*\n%FSLAX26Y26*%\n%MOIN*%\n%ADD10C,0.007874*%\n%IPPOS*%\n%LNgko11.gbr*%\n%LPD*%\nG75*\nG54D10*\n"
        for statement in statements:
            strr = statement.to_gerber(settings) + "\n"
            writestr += strr
        return writestr
    def calcdistance(self, pointPair, lineSet: [LineSet], offset, ratek):
        d_start1 = abs(pointPair[0][0] - (lineSet.start[0] - offset[0]) * ratek) + abs(pointPair[0][1] - (lineSet.start[1] - offset[1]) * ratek)
        d_end1 = abs(pointPair[1][0] - (lineSet.end[0] - offset[0]) * ratek) + abs(pointPair[1][1] - (lineSet.end[1] - offset[1]) * ratek)
        d_start2 = abs(pointPair[0][0] - (lineSet.end[0] - offset[0]) * ratek) + abs(pointPair[0][1] - (lineSet.end[1] - offset[1]) * ratek)
        d_end2 = abs(pointPair[1][0] - (lineSet.start[0] - offset[0]) * ratek) + abs(pointPair[1][1] - (lineSet.start[1] - offset[1]) * ratek)
        distance = min((d_start1 + d_end1), (d_start2 + d_end2))
        return distance

    def __findset(self, pointPair, lineSets: List[LineSet]) -> LineSet:
        ratek = self.imageGenerater.ratek
        offset = self.imageGenerater.offset
        mindistance = 10000000000
        minlineSet = None
        for lineSet in lineSets:
            curdistance = self.calcdistance(pointPair, lineSet, offset, ratek)
            if lineSet.linesNum == 1:
                curdistance = curdistance + 16
            if curdistance < mindistance:
                mindistance = curdistance
                minlineSet = lineSet
        # if minlineSet is not None and minlineSet.linesNum == 1:
        #     mindistance2 = 1000000000
        #     minlineSet2 = None
        #     for lineSet in lineSets:
        #         curdistance = self.calcdistance(pointPair, lineSet, offset, ratek)
        #         if curdistance < mindistance2 and curdistance >= mindistance and lineSet.linesNum > 1:
        #             mindistance2 = curdistance
        #             minlineSet2 = lineSet
        #     if minlineSet2 is not None and mindistance2 - mindistance < 16:
        #         minlineSet = minlineSet2
        # clas2 = Cls2()
        # clas2.count = clas2.count + 1
        # clas2.sum = clas2.sum + mindistance
        # print(f"{clas2.count}--{clas2.sum}--{clas2.sum / clas2.count}")
        # print(mindistance)
        # clas2.list.append(mindistance)
        return minlineSet
