import copy
import math
import pickle
from collections import Counter
import os
import cv2
import numpy as np
from ProcessService.SupportFuncs.ImageGenerater import ImageGenerater
from ProcessService.RoutLineProcess.GKOImageProcess2.GKOImageProcessBase import GKOImageProcessBase
from skimage import morphology


def SaveImage(path, image):
    if __name__ == "__main__":
        cv2.imwrite(path, image)


def ShowImage(image):
    if __name__ == '__main__':
        cv2.imshow("test", image)
        cv2.waitKey(-1)


class GKOImageProcess(GKOImageProcessBase):
    def __init__(self, imageGenerater: ImageGenerater):
        GKOImageProcessBase.__init__(self, imageGenerater)
        self.__init()

    def __init(self):
        self.__img_gko = self.imageGenerater.getlayerimg2_gko()
        self.__img_drl = self.imageGenerater.getlayerimg2_drl()
        self.__img_gtl = self.imageGenerater.getlayerimg("gtl")
        self.__img_gbl = self.imageGenerater.getlayerimg("gbl")
        self.__img_gtbl = None
        if self.__img_gbl is not None:
            self.__img_gtbl = self.__img_gbl
        if self.__img_gtl is not None:
            if self.__img_gtbl is not None:
                self.__img_gtbl = cv2.bitwise_or(self.__img_gtl, self.__img_gbl)
            else:
                self.__img_gtbl = self.__img_gtl
        if self.__img_gtbl is None:
            self.solid_len = 0  # 锣带长度
            self.solid_area = 0  # 板面积
            self.rate = 0
        else:
            SaveImage(r"Debug/img_gtbl.png", self.__img_gtbl)  # 线路图片保存

    def saveData(self, path):
        str = pickle.dumps(self.line_dict)
        with open(f"{path}/GKOImageProcessCacheData", "wb") as f:
            f.write(str)

    def loadData(self, path):
        if os.path.exists(f"{path}/GKOImageProcessCacheData"):
            with open(f"{path}/GKOImageProcessCacheData", "rb") as f:
                str = f.read()
                self.line_dict = pickle.loads(str)
                return True
        return False

    def process(self):
        if self.__img_gtbl is None:
            self.line_dict = {}
            return
        SaveImage(r"Debug/gkoImage.png", self.__img_gko)  # 所有板区域图片
        # 外轮廓长度  外轮廓填充图片      外轮廓图片
        outer_len, outer_full_image, outer_image = self.calc_outer_contour(self.__img_gko)
        # 行索引 列索引
        rows, cols = self.get_v_cut()
        rows, cols = self.calc_v_cut(self.__img_gko, outer_full_image, rows, cols)
        # 计算连通域标记图片
        ret, gko_bin_img = cv2.threshold(self.__img_gko, 250, 1, type=cv2.THRESH_BINARY_INV)
        region_num, labels, stats, centroids = cv2.connectedComponentsWithStats(gko_bin_img, connectivity=4)
        labels = np.uint16(labels)
        SaveImage(r"Debug/labels.png", np.uint8(labels))  # 标记图片保存
        SaveImage(r"Debug/__img_drl.png", self.__img_drl)  # 线路图片保存
        OutreachRegion_indexlist = self.OutreachRegion(labels)  # 计算与外界相连的区域
        LinerRegion_indexlist = self.LinerRegion(labels, region_num, self.__img_gtbl)  # 计算包含线路的区域
        StampHole_indexlist = self.StampHoleRegion(labels, region_num, self.__img_drl)  # 计算邮票孔区域
        vCutSupportRegion_indexlist = self.vCutSupportRegion(stats, cols, rows)  # 计算v割构成支撑柱区域
        self.vCutSupportArea = []
        for vCutSupportRegion_index in vCutSupportRegion_indexlist:
            self.vCutSupportArea.append(stats[vCutSupportRegion_index])
        indexlist = OutreachRegion_indexlist
        indexlist.extend(LinerRegion_indexlist)
        indexlist.extend(StampHole_indexlist)
        indexlist.extend(vCutSupportRegion_indexlist)
        indexlist = list(set(indexlist))
        if indexlist.__contains__(1):
            indexlist.remove(1)
        drawImage = self.DrawImage(labels, indexlist)
        contours, hierarchy = cv2.findContours(drawImage, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
        self.contours = contours
        crossPointImage, line_img = self.GetCrossPoint2(self.__img_gko, drawImage, outer_full_image)
        SaveImage(r"Debug/line_img.png", line_img)  # 所有版区域外轮廓
        SaveImage(r"Debug/crossPointImage.png", crossPointImage)  # gko层所有交叉点
        intersection_lp = cv2.bitwise_and(line_img, crossPointImage * 255)  # 锣带线断开
        SaveImage(r"Debug/intersection_lp.png", intersection_lp)
        seg_line_img = line_img - intersection_lp  # 锣带线端点
        SaveImage(r"Debug/seg_line_img.png", seg_line_img)
        end_points_img = self.GetEndPoint(np.uint8(seg_line_img / 255))
        SaveImage(r"Debug/end_points_img.png", end_points_img * 128)
        pointpire = self.GetPointPair(end_points_img)
        self.end_points_img = end_points_img
        self.line_dict = pointpire
        return True

    def process2_outerLinePointPair(self):
        SaveImage(r"Debug/gkoImage.png", self.__img_gko)  # 所有板区域图片
        # 外轮廓长度  外轮廓填充图片      外轮廓图片
        outer_len, outer_full_image, outer_image = self.calc_outer_contour(self.__img_gko)
        # 行索引 列索引
        rows, cols = self.calc_v_cut(self.__img_gko, outer_full_image)
        # 计算连通域标记图片
        ret, gko_bin_img = cv2.threshold(self.__img_gko, 250, 1, type=cv2.THRESH_BINARY_INV)
        region_num, labels, stats, centroids = cv2.connectedComponentsWithStats(gko_bin_img, connectivity=4)
        labels = np.uint16(labels)
        # SaveImage(r"../Debug/labels.png", np.uint8(labels))  # 标记图片保存
        # SaveImage(r"../Debug/__img_drl.png", self.__img_drl)  # 线路图片保存
        # OutreachRegion_indexlist = self.OutreachRegion(labels)  # 计算与外界相连的区域
        # LinerRegion_indexlist = self.LinerRegion(labels, region_num, self.img_gtbl)  # 计算包含线路的区域
        # StampHole_indexlist = self.StampHoleRegion(labels, region_num, self.__img_drl)  # 计算邮票孔区域
        # vCutSupportRegion_indexlist = self.vCutSupportRegion(stats, cols, rows)  # 计算v割构成支撑柱区域
        # indexlist = OutreachRegion_indexlist
        # indexlist.extend(LinerRegion_indexlist)
        # indexlist.extend(StampHole_indexlist)
        # indexlist.extend(vCutSupportRegion_indexlist)
        # indexlist = list(set(indexlist))
        # if indexlist.__contains__(1):
        #     indexlist.remove(1)
        # drawImage = self.DrawImage(labels, indexlist)
        # SaveImage(r"../Debug/drawImage.png", drawImage)  # 所有板区域图片
        # process_img = cv2.dilate(drawImage, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, ksize=(5, 5)))  # 膨胀腐蚀去掉v割缝
        # process_img = cv2.erode(process_img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, ksize=(5, 5)))  # 膨胀腐蚀去掉v割缝
        process_img = outer_full_image * 255
        SaveImage(r"Debug/inner_process_img.png", process_img)  # 所有板区域图片
        line_img = cv2.morphologyEx(process_img, cv2.MORPH_GRADIENT, cv2.getStructuringElement(cv2.MORPH_RECT, ksize=(2, 2)))
        SaveImage(r"Debug/line_img.png", line_img)  # 所有板区域边缘图片
        self.line_img = cv2.dilate(line_img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, ksize=(3, 3)))
        crossPointImage = self.GetCrossPoint(self.__img_gko)
        crossPointImage = cv2.dilate(crossPointImage, cv2.getStructuringElement(shape=cv2.MORPH_ELLIPSE, ksize=(5, 5)))
        SaveImage(r"Debug/crossPointImage.png", crossPointImage * 255)  # gko层所有交叉点
        intersection_lp = cv2.bitwise_and(line_img, crossPointImage * 255)  # 锣带线断开

        SaveImage(r"Debug/intersection_lp.png", intersection_lp)
        seg_line_img = line_img - intersection_lp  # 锣带线端点
        SaveImage(r"Debug/seg_line_img.png", seg_line_img)
        end_points_img = self.GetEndPoint(np.uint8(seg_line_img / 255))
        SaveImage(r"Debug/end_points_img.png", end_points_img * 128)
        pointpire = self.GetPointPair(end_points_img)
        self.line_dict = pointpire
        return True


indexset = [12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 30, 31, 32, 36, 39]
if __name__ == '__main__':
    cv2.namedWindow("test", 0)
    cv2.resizeWindow("test", int(1728 / 100 * 100), int(972 / 100 * 100))
    cv2.moveWindow("test", 0, 0)
    gerberFilePath = f"D:\ProjectFile\EngineeringAutomation\TestData\GerberFile"
    groupDirs = os.listdir(gerberFilePath)
    index = 0
    for orderDir in groupDirs:
        index += 1
        print(f"{index}\\{orderDir}")
        # if index == 5:
        #     continue
        # if not indexset.__contains__(index) and index < 39:
        #     continue
        if index < 2:
            continue
        if orderDir != "ALL-4W2181383":
            continue
        gerbers = dataPrepar(f"{gerberFilePath}\\{orderDir}")
        imageGenerater = ImageGenerater(gerbers)
        gkoImageProcess = GKOImageProcess(imageGenerater)
        gkoImageProcess.process()
        if hasattr(gkoImageProcess, 'line_img'):
            image = gkoImageProcess.end_points_img
        cv2.imshow("test", image)
        kval = 13
        while kval == 13:
            kval = cv2.waitKey(-1)
