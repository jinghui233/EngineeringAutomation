import copy
import math
from collections import Counter
import os
from typing import List

import cv2
import numpy as np
from ProcessService.SupportFuncs.ImageGenerater import ImageGenerater
from skimage import morphology


def SaveImage(path, image):
    return
    cv2.imwrite(path, image)


class GKOImageProcessBase:
    def __init__(self, imageGenerater: ImageGenerater):
        self.imageGenerater = imageGenerater

    def calc_outer_contour(self, gko_img):  # 计算外轮廓图片及外轮廓填充图片
        ret, bin_img = cv2.threshold(gko_img, 250, 255, type=cv2.THRESH_BINARY_INV)
        outer_image = 255 - bin_img
        contours, hierarchy = cv2.findContours(outer_image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
        max_area = 0
        max_index = 0
        for i in range(len(contours)):
            cur_area = cv2.contourArea(contours[i])
            if cur_area > max_area:
                max_area = cur_area
                max_index = i
        max_len = len(contours[max_index])
        outer_image[:, :] = 0
        cv2.drawContours(outer_image, contours, max_index, color=255, thickness=-1)
        outer_image = cv2.erode(outer_image, cv2.getStructuringElement(cv2.MORPH_RECT, ksize=(7, 7)))
        outer_image = cv2.dilate(outer_image, cv2.getStructuringElement(cv2.MORPH_RECT, ksize=(7, 7)))
        contours, hierarchy = cv2.findContours(outer_image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
        outer_image[:, :] = 0
        cv2.drawContours(outer_image, contours, 0, color=255, thickness=1)
        expanded_outer_image = cv2.copyMakeBorder(outer_image, 1, 1, 1, 1, cv2.BORDER_CONSTANT, 0)
        cv2.floodFill(expanded_outer_image, None, (0, 0), 255)
        outer_full_image = cv2.bitwise_not(expanded_outer_image)
        outer_full_image = outer_full_image[1:-1, 1:-1]
        outer_full_image = cv2.dilate(outer_full_image, cv2.getStructuringElement(cv2.MORPH_RECT, ksize=(3, 3)))
        #      外轮廓长度         外轮廓填充图片                       外轮廓图片
        return max_len, np.uint8(outer_full_image / 255), np.uint8(outer_image / 255)

    def calc_v_cut(self, gko_img, outer_full_image, rows, cols):  # 找v割线
        _, gko_bin_img = cv2.threshold(gko_img, 250, 1, cv2.THRESH_BINARY)
        # rows = []
        for rowindex in range(gko_bin_img.shape[0]):
            gko_sum = gko_bin_img[rowindex, :].sum()
            outer_full_sum = outer_full_image[rowindex, :].sum()
            if gko_sum >= outer_full_sum and gko_sum != 0:
                rows.append(rowindex)
        # cols = []
        for colindex in range(gko_bin_img.shape[1]):
            gko_sum = gko_bin_img[:, colindex].sum()
            outer_full_sum = outer_full_image[:, colindex].sum()
            if gko_sum >= outer_full_sum and gko_sum != 0:
                cols.append(colindex)
        #     行索引 列索引
        return rows, cols

    def get_v_cut(self):
        rows, cols = [], []
        for vcutLine in self.imageGenerater.orderData.vCutLines:
            if vcutLine.start[0] == vcutLine.end[0]:
                cols.append(round((vcutLine.start[0] - self.imageGenerater.offset[0]) * self.imageGenerater.ratek))
            if vcutLine.start[1] == vcutLine.end[1]:
                rows.append(round((vcutLine.start[1] - self.imageGenerater.offset[1]) * self.imageGenerater.ratek))
        return rows, cols

    def OutreachRegion(self, labels) -> List[int]:
        dilate_img = cv2.dilate(labels, kernel=cv2.getStructuringElement(cv2.MORPH_RECT, ksize=(9, 9)))
        left_side = dilate_img[0, :]
        right_side = dilate_img[-1, :]
        up_side = dilate_img[:, 0]
        bottom_side = dilate_img[:, -1]
        left_conter = Counter(left_side)
        right_conter = Counter(right_side)
        up_conter = Counter(up_side)
        bottom_conter = Counter(bottom_side)
        OutreachRegion_indexlist = list(left_conter.keys()) + list(right_conter.keys()) + list(up_conter.keys()) + list(bottom_conter.keys())
        # OutreachRegion_indexlist.append(1)  # 加最外圈border区域index
        return OutreachRegion_indexlist

    def LinerRegion(self, labels, region_num, gtl_img) -> List[int]:
        ret, l_bin_img = cv2.threshold(gtl_img, 250, 1, type=cv2.THRESH_BINARY)
        intersection_kl = labels * l_bin_img
        hist_l = cv2.calcHist([intersection_kl], [0], None, [region_num], [0, region_num])
        LinerRegion_indexlist = list(np.where(hist_l > 0)[0])
        LinerRegion_indexlist.remove(0)
        return LinerRegion_indexlist

    def StampHoleRegion(self, labels, region_num, drl_img) -> List[int]:
        ret, l_bin_img = cv2.threshold(drl_img, 250, 1, type=cv2.THRESH_BINARY)
        intersection_kl = labels * l_bin_img
        hist_l = cv2.calcHist([intersection_kl], [0], None, [region_num], [0, region_num])
        StampHole_indexlist = list(np.where(hist_l > 0)[0])
        StampHole_indexlist.remove(0)
        return StampHole_indexlist

    def vCutSupportRegion(self, stats, cols, rows) -> List[int]:
        vCutSupportRegion_indexlist = []
        if rows.__len__() > 0 and cols.__len__() > 0:
            rows, cols = np.array(rows), np.array(cols)
            for statindex in range(len(stats)):
                stat = stats[statindex]
                if math.fabs(stat[4] - stat[2] * stat[3]) < 10:  # 判断是矩形
                    sx, sy, ex, ey = stat[0], stat[1], stat[0] + stat[2], stat[1] + stat[3]
                    dsx = abs(cols - sx).min()
                    dsy = abs(rows - sy).min()
                    dex = abs(cols - ex).min()
                    dey = abs(rows - ey).min()
                    if dsx + dsy + dex + dey < 5:
                        vCutSupportRegion_indexlist.append(statindex)
        return vCutSupportRegion_indexlist

    def DrawImage(self, labels, indexlist):
        height, width = labels.shape
        inner_region_img = np.zeros(shape=(height, width), dtype=np.uint8)
        for i in range(len(indexlist)):
            index = indexlist[i]
            mask = cv2.inRange(labels, int(index), int(index))
            inner_region_img = cv2.bitwise_or(mask, inner_region_img)
        return inner_region_img

    def RemoveVcutLine(self, image):
        image = image / 255
        kernel_element = np.array((
            [1, 1, 1, 1, 1],
            [1, -2, -2, -2, 1],
            [1, -2, -2, -2, 1],
            [1, -2, -2, -2, 1],
            [1, 1, 1, 1, 1]), dtype="float32")
        temp_img = cv2.filter2D(image, -1, kernel_element)
        _, temp_img = cv2.threshold(temp_img, 9, 1, cv2.THRESH_BINARY)
        SaveImage(r"Debug/temp_img.png", temp_img * 255)
        resimg = cv2.bitwise_or(image, temp_img)
        SaveImage(r"Debug/resimg.png", resimg * 255)
        kernel_element = np.array((
            [1, 1, 1],
            [1, -9, 1],
            [1, 1, 1]), dtype="float32")
        temp_img = cv2.filter2D(resimg, -1, kernel_element)
        _, temp_img = cv2.threshold(temp_img, 5, 1, cv2.THRESH_BINARY)
        # SaveImage(r"../Debug/resimg.png", temp_img * 255)
        resimg = cv2.bitwise_or(resimg, temp_img)
        SaveImage(r"Debug/resimg.png", resimg * 255)

    def GetCrossPoint(self, image):
        ret, gko_bin_img = cv2.threshold(image, 250, 1, type=cv2.THRESH_BINARY_INV)
        # ###########################segment line################################################
        gko_bin_img = 1 - gko_bin_img
        sketech_img = morphology.skeletonize(gko_bin_img)
        sketech_img = np.uint8(sketech_img)
        kernel_element = np.array((
            [1, 1, 1],
            [1, 0, 1],
            [1, 1, 1]), dtype="float32")
        neighbor_img = cv2.filter2D(sketech_img, -1, kernel_element)
        neighbor_img = sketech_img * neighbor_img
        ret, cross_region_points_img = cv2.threshold(neighbor_img, 2, 1, cv2.THRESH_BINARY)
        return cross_region_points_img

    def Gradient(self, image, upper=False):
        if upper:
            return cv2.dilate(image, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, ksize=(3, 3))) - image
        return image - cv2.erode(image, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, ksize=(3, 3)))

    def GetCrossPoint2(self, img_gko, drawImage, outer_full_image):
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, ksize=(3, 3))
        # ↑
        process_img = cv2.dilate(drawImage, kernel, iterations=2)
        # ↑
        process_img_dilate = cv2.dilate(process_img, kernel, iterations=2)
        line_img = self.Gradient(process_img_dilate)
        SaveImage(r"Debug/line_img.png", line_img)
        crossPointImage = cv2.bitwise_and(line_img, img_gko)
        # ↑
        process_img_dilate = cv2.dilate(process_img_dilate, kernel)
        line_img = self.Gradient(process_img_dilate)
        SaveImage(r"Debug/line_img.png", line_img)
        crossPointImage = cv2.bitwise_or(crossPointImage, cv2.bitwise_and(line_img, img_gko))
        # ↓
        process_img = process_img * outer_full_image
        process_img_erode = cv2.erode(process_img, kernel, iterations=1)
        line_img_result = self.Gradient(process_img_erode)
        # ↓
        process_img_erode = cv2.erode(process_img_erode, kernel, iterations=3)
        line_img = self.Gradient(process_img_erode)
        SaveImage(r"Debug/line_img.png", line_img)
        crossPointImage = cv2.bitwise_or(crossPointImage, cv2.bitwise_and(line_img, img_gko))
        # ↓
        process_img_erode = cv2.erode(process_img_erode, kernel)
        line_img = self.Gradient(process_img_erode)
        SaveImage(r"Debug/line_img.png", line_img)
        crossPointImage = cv2.bitwise_or(crossPointImage, cv2.bitwise_and(line_img, img_gko))
        crossPointImage = cv2.bitwise_or(self.GetCrossPoint(img_gko) * 255, crossPointImage)
        crossPointImage = cv2.dilate(crossPointImage, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, ksize=(5, 5)))
        crossPointImage = cv2.dilate(crossPointImage, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, ksize=(3, 3)))
        return crossPointImage, line_img_result

    def GetEndPoint(self, image):
        image_sk = np.uint8(morphology.skeletonize(image))
        kernel_element = np.array((
            [1, 1, 1],
            [1, 0, 1],
            [1, 1, 1]), dtype="float32")
        # get line end points
        line_num, line_label_img = cv2.connectedComponents(image_sk)
        neighbor_line_img = cv2.filter2D(image_sk, -1, kernel=kernel_element)
        SaveImage(r"Debug/neighbor_line_img.png", neighbor_line_img * 64)
        ret, end_points_region = cv2.threshold(neighbor_line_img, 1, 0, type=cv2.THRESH_TOZERO_INV)
        SaveImage(r"Debug/end_points_region.png", end_points_region * 255)
        SaveImage(r"Debug/line_label_img.png", line_label_img * 8)
        end_points_img = end_points_region * line_label_img
        return end_points_img

    def GetPointPair(self, end_points_img):
        line_dict = {}
        height, width = end_points_img.shape
        for i in range(height):
            for j in range(width):
                if end_points_img[i][j] > 0:
                    line_dict[end_points_img[i][j]] = []

        for i in range(height):
            for j in range(width):
                if end_points_img[i][j] > 0:
                    line_dict[end_points_img[i][j]].append([j, i])
        return line_dict
