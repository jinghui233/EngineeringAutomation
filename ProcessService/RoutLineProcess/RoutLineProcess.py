import os

import cv2

from ProcessService.RoutLineProcess.GKOGerberProcess2.GKOGerberProcess import GKOGerberProcess
from ProcessService.RoutLineProcess.GKOImageProcess2.GKOImageProcess import GKOImageProcess
from ProcessService.RoutLineProcess.RoutLineProcessBase import RoutLineProcessBase
from ProcessService.SupportFuncs.DataContainer import GroupData, OrderData
from ProcessService.SupportFuncs.ImageGenerater import ImageGenerater


class RoutLineProcess(RoutLineProcessBase):
    def __init__(self, orderData: OrderData):
        RoutLineProcessBase.__init__(self, orderData)

    def process(self):
        # 图像处理部分
        self.gkoImgPcs = GKOImageProcess(ImageGenerater(self.orderData))
        if  not self.gkoImgPcs.loadData(self.orderData.filePath):  # 尝试加载缓存数据
            self.gkoImgPcs.process()  # 计算数据
            self.gkoImgPcs.saveData(self.orderData.filePath)  # 保存缓存数据
        # 线层去重切割分组预处理
        self.gkoGbrPcs = GKOGerberProcess(self.orderData)
        setsold = self.gkoGbrPcs.sets.copy()
        if  not self.gkoGbrPcs.loadData(self.orderData.filePath):  # 尝试加载缓存数据
            self.gkoGbrPcs.pre_process()  # 计算数据
            self.gkoGbrPcs.saveData(self.orderData.filePath)  # 保存缓存数据
        # 线线对应
        lineSets = self.getLineSets(self.gkoGbrPcs.sets, self.gkoImgPcs.line_dict.values())
        self.gkoGbrPcs.sets = lineSets
        # rout层后处理
        if self.gkoGbrPcs.LastProc():
            self.sets = self.gkoGbrPcs.sets
            self.success = True
        else:
            self.sets = setsold
            self.success = False


ordersList = ["ALL-2W2173486", "ALL-2W2316728", "ALL-4W2061560", "ALL-4W2181383", "ALL-4W2151623"]
indexset = [12, 13, 14, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 28, 30, 31, 32, 36, 39]
if __name__ == '__main__':
    path = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile4"
    resultPath = "D:\ProjectFile\EngineeringAutomation\TestData\TestResult"
    groupDirs = os.listdir(path)
    totle = len(groupDirs)
    index = 0
    for groupDir in groupDirs:
        index = index + 1
        print(f"{index}\\{groupDir}")
        if index < 0:
            continue
        # if groupDir != "ALL-1W2316018":
        #     continue
        groupData = GroupData(f"{path}\{groupDir}", groupDir)
        for orderData in groupData.orders.values():
            routLineProcess = RoutLineProcess(orderData)
            routLineProcess.process()
            image = routLineProcess.gkoImgPcs.imageGenerater.DrawShow(routLineProcess.sets, False, -1)
            cv2.imwrite(f"{resultPath}\\{groupDir}_{orderData.orderNo}.png", image)
            # cv2.imshow("test", image)
            # kval = 13
            # while kval == 13:
            #     kval = cv2.waitKey(-1)
