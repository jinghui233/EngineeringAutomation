import os
import shutil
from ProcessService.SupportFuncs.DataDownLoad.DataDownLoadBase import DataDownLoadBase

savePath = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile4"
Path = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile5"
GroupDirs = os.listdir(Path)
for groupDir in GroupDirs:
    subDirs = os.listdir(f"{Path}\{groupDir}")
    for subDir in subDirs:
        if len(subDir.split("_")) < 2:
            continue
        curPath = f"{Path}\{groupDir}\{subDir}"
        if not os.path.isdir(curPath):
            # if not os.path.exists(f"{savePath}\{groupDir}"):
            #     os.makedirs(f"{savePath}\{groupDir}")
            shutil.copyfile(curPath, f"{savePath}\{groupDir}\{subDir}")
# for groupDir in GroupDirs:
#     if not os.path.exists(f"{savePath}\{groupDir}"):
#         os.makedirs(f"{savePath}\{groupDir}")
#     DataDownLoadBase.DownLoadLdFile_with_UnZip(groupDir,f"{savePath}\{groupDir}")
