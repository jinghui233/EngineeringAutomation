import os
import shutil

import cv2
from ProcessService.SupportFuncs.ImageGenerater import ImageGenerater


def removetxt(txt):
    gko = txt
    lines = gko.split("\n")
    largest = ""
    delete = False
    newLines = []
    for line in lines:
        if line[:4] == "G36*":
            return txt
        if line[:4] == "%ADD":
            largest = f"G54D{line[4:6]}*"
        if line == largest:
            delete = True
        if delete and line[:4] == "G54D" and line != largest:
            delete = False
        if not delete:
            newLines.append(line)
    return "\n".join(newLines)


def processOrders():
    path = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile2"
    orderDirs = os.listdir(f"{path}")
    for orderDir in orderDirs:
        print(orderDir)
        if not os.path.isdir(f"{path}\\{orderDir}"):
            continue
        with open(f"{path}\\{orderDir}\\ok\\gko", 'r') as f:
            data = f.read()
            data = removetxt(data)
        with open(f"{path}\\{orderDir}\\ok\\gko", 'w') as f:
            f.write(data)
        # img_gko = imageGnrt.getlayerimg("gko")
        # cv2.imwrite(f"{distDir}\\{orderDir}.png", img_gko)
        # cv2.imshow("test", img_gko)
        # kval = 13
        # while kval == 13:
        #     kval = cv2.waitKey(-1)


def processGroups():
    path = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile4"
    groupDirs = os.listdir(path)
    for groupDir in groupDirs:
        print(groupDir)
        ordersPath = f"{path}\{groupDir}\Orders"
        orderDirs = os.listdir(ordersPath)
        for orderDir in orderDirs:
            gkoPath = ""
            if os.path.exists(f"{ordersPath}\{orderDir}\ok"):
                gkoPath = f"{ordersPath}\{orderDir}\ok"
            else:
                subDirs = os.listdir(f"{ordersPath}\{orderDir}")
                gkoPath = f"{ordersPath}\{orderDir}\{subDirs[0]}\ok"
            files = os.listdir(gkoPath)
            for file in files:
                if file == "gko" or file.split(".")[-1] == "gko":
                    data = open(f"{gkoPath}\{file}", "r").read()
                    data = removetxt(data)
                    open(f"{gkoPath}\{file}", "w").write(data)


if __name__ == "__main__":
    processGroups()
