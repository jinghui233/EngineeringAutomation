import os
from typing import List


class StepRepeat:
    def __init__(self, lines: List[str]):
        self.name = None
        self.x = None
        self.y = None
        self.dx = None
        self.dy = None
        self.nx = None
        self.ny = None
        self.angle = None
        self.mirror = None
        self.unit = "inch"
        self.Load(lines)

    def calcTranslations(self) -> List[tuple]:
        translations = []
        for nx in range(self.nx):
            for ny in range(self.ny):
                offset = (self.x + nx * self.dx, self.y + ny * self.dy)
                translations.append(offset)
        return translations

    def Load(self, lines: List[str]):
        for line in lines:
            self.SetAttr(line)

    def SetAttr(self, line: str):
        line_sp = line.split("=")
        if len(line_sp) != 2:
            return
        attrName, attrVal = line_sp[0].strip(), line_sp[1].strip()
        if attrName == "NAME":
            self.name = attrVal
        elif attrName == "X":
            self.x = float(attrVal)
        elif attrName == "Y":
            self.y = float(attrVal)
        elif attrName == "DX":
            self.dx = float(attrVal)
        elif attrName == "DY":
            self.dy = float(attrVal)
        elif attrName == "NX":
            self.nx = int(attrVal)
        elif attrName == "NY":
            self.ny = int(attrVal)
        elif attrName == "ANGLE":
            self.angle = int(attrVal)
        elif attrName == "MIRROR":
            self.mirror = attrVal != "No"

    def to_inch(self):
        if self.unit == "mm":
            self.unit = "inch"
            self.x = self.x * 0.03937008
            self.y = self.y * 0.03937008
            self.dx = self.dx * 0.03937008
            self.dy = self.dy * 0.03937008

    def to_mm(self):
        if self.unit == "inch":
            self.unit = "mm"
            self.x = self.x * 25.4
            self.y = self.y * 25.4
            self.dx = self.dx * 25.4
            self.dy = self.dy * 25.4


class Stephdr:
    def __init__(self, str_data: str):
        self.stepRepeats = []
        self.Load(str_data)

    @staticmethod
    def Read(path: str):
        str_data = open(path, "r").read()
        return Stephdr(str_data)

    def Load(self, str_data: str):
        strp = False
        strp_strs = []
        for line in str_data.split("\n"):
            if len(line) == 0:
                continue
            lastchar = line[-1]
            if lastchar == "}":
                self.stepRepeats.append(StepRepeat(strp_strs))
                strp_strs.clear()
                strp = False
            if strp:
                strp_strs.append(line)
            else:
                self.SetAttr(line)
            if lastchar == "{":
                strp = True

    def SetAttr(self, line: str):
        line_sp = line.split("=")
        if len(line_sp) != 2:
            return
        attrName, attrVal = line_sp[0].strip(), line_sp[1].strip()
        if attrName == "X_DATUM":
            self.X_DATUM = float(attrVal)
        elif attrName == "Y_DATUM":
            self.Y_DATUM = float(attrVal)
        elif attrName == "X_ORIGIN":
            self.X_ORIGIN = float(attrVal)
        elif attrName == "Y_ORIGIN":
            self.Y_ORIGIN = float(attrVal)
        elif attrName == "TOP_ACTIVE":
            self.TOP_ACTIVE = attrVal
        elif attrName == "BOTTOM_ACTIVE":
            self.BOTTOM_ACTIVE = attrVal
        elif attrName == "RIGHT_ACTIVE":
            self.RIGHT_ACTIVE = attrVal
        elif attrName == "LEFT_ACTIVE":
            self.LEFT_ACTIVE = attrVal
        elif attrName == "ONLINE_DRC_NAME":
            self.ONLINE_DRC_NAME = attrVal
        elif attrName == "ONLINE_DRC_MODE":
            self.ONLINE_DRC_MODE = attrVal
        elif attrName == "ONLINE_DRC_STAT":
            self.ONLINE_DRC_STAT = attrVal
        elif attrName == "ONLINE_DRC_TIME":
            self.ONLINE_DRC_TIME = attrVal
        elif attrName == "ONLINE_DRC_BEEP_VOL":
            self.ONLINE_DRC_BEEP_VOL = attrVal
        elif attrName == "ONLINE_DRC_BEEP_TONE":
            self.ONLINE_DRC_BEEP_TONE = attrVal
        elif attrName == "ONLINE_NET_MODE":
            self.ONLINE_NET_MODE = attrVal
        elif attrName == "ONLINE_NET_STAT":
            self.ONLINE_NET_STAT = attrVal
        elif attrName == "ONLINE_NET_TIME":
            self.ONLINE_NET_TIME = attrVal
        elif attrName == "ONLINE_NET_BEEP_VOL":
            self.ONLINE_NET_BEEP_VOL = attrVal
        elif attrName == "ONLINE_NET_BEEP_TONE":
            self.ONLINE_NET_BEEP_TONE = attrVal

    def to_mm(self):
        for stepRepeat in self.stepRepeats:
            stepRepeat.to_mm()

    def to_inch(self):
        for stepRepeat in self.stepRepeats:
            stepRepeat.to_inch()


if __name__ == "__main__":
    path = "D:\ProjectFile\EngineeringAutomation\TestData\PinGerberFile\JP-1S2538064\jp-1s2538064\steps\panel\stephdr"
    str_data = open(path, "r").read()
    stephdr_reader1 = Stephdr(str_data)
    sdfasd = 0
