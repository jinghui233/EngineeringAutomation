import math
from typing import List

from gerber.gerber_statements import CoordStmt

from ProcessService.RoutLineProcess.LinePiceSet import LineSet, LinePice
from ProcessService.RoutLineProcess.ArcProcess.ArcSet import ArcBase
from ProcessService.SupportFuncs.DataContainer import GroupData
from ProcessService.SupportFuncs.PinBan.Stephdr import Stephdr, StepRepeat
from ProcessService.SupportFuncs.PinBan.OrderSet import OrderSet
from ProcessService.RoutLineProcess.ArcProcess.ArcSet import ArcBase


# JP-1W2538073 JP-2W2535774 JP-1S2538064 JP-1W2537553
class PinBan:
    def __init__(self, orderSets: List[OrderSet], stephdr: Stephdr):
        self.orderSets = orderSets
        self.stephdr = stephdr

    def Pin(self):
        setsnew = []
        for stepRepet in self.stephdr.stepRepeats:
            for orderSet in self.orderSets:
                if stepRepet.name.__contains__(orderSet.orderNo) or len(self.orderSets) < 2:
                    newOrderSet = self.PinOne(stepRepet, orderSet.lines, orderSet.offset)
                    setsnew.extend(newOrderSet)
        return setsnew

    def PinOne(self, stepRepet: StepRepeat, lines, offset: (tuple)):
        setsnew = []
        for t in stepRepet.calcTranslations():
            setsnew.extend(self.Trans_and_Rotate_and_offset_new(lines, t, stepRepet.angle, offset))
        return setsnew

    def Trans_and_Rotate_and_offset_new(self, lines, dxy: (tuple), angle, offset: (tuple)):
        linesnew = []
        for line in lines:
            start = self.__offset(line.start, offset)
            end = self.__offset(line.end, offset)
            start = self.__rotate(start, angle)
            end = self.__rotate(end, angle)
            start = self.__translation(start, dxy)
            end = self.__translation(end, dxy)
            if isinstance(line, ArcBase):
                center = self.__offset(line.center, offset)
                center = self.__rotate(center, angle)
                center = self.__translation(center, dxy)
                linesnew.append(ArcBase(start, end, center, line.radius, line.direction))
            else:
                linesnew.append(LinePice(start, end, line.radius))
        return linesnew

    def Offset(self, sets, offset: tuple):
        for set in sets:
            for line in set.GetLineSet():
                line.start = self.__offset(line.start, offset)
                line.end = self.__offset(line.end, offset)

    def __offset(self, point: tuple, offset: tuple):
        return (point[0] - offset[0], point[1] - offset[1])

    def Translation(self, sets, dxy: (float, float)):
        for set in sets:
            for line in set.GetLineSet():
                line.start = self.__translation(line.start, dxy)
                line.end = self.__translation(line.end, dxy)

    def __translation(self, point: tuple, dxy: (float, float)) -> (float, float):
        return (point[0] + dxy[0], point[1] + dxy[1])

    def Rotate(self, sets, angle):
        for set in sets:
            for line in set.GetLineSet():
                line.start = self.__rotate(line.start, angle)
                line.end = self.__rotate(line.end, angle)

    def __rotate(self, point: tuple, angle) -> (float, float):
        point_x = point[0] * math.cos(angle / 180 * math.pi) + point[1] * math.sin(angle / 180 * math.pi)
        point_y = -point[0] * math.sin(angle / 180 * math.pi) + point[1] * math.cos(angle / 180 * math.pi)
        return (point_x, point_y)


def ConvertArc(arc: ArcBase):
    Arc_start_X = str(int(arc.start[0] * 10 ** 6))
    Arc_start_Y = str(int(arc.start[1] * 10 ** 6))
    Arc_end_X = str(int(arc.end[0] * 10 ** 6))
    Arc_end_Y = str(int(arc.end[1] * 10 ** 6))
    Arc_end_I = str(int(arc.I * 10 ** 6))
    Arc_end_J = str(int(arc.J * 10 ** 6))
    if arc.direction == 'clockwise':
        direction = 'G02'
    else:
        direction = 'G03'
    txt = f"X{Arc_start_X}Y{Arc_start_Y}D02*\n{direction}X{Arc_end_X}Y{Arc_end_Y}I{Arc_end_I}J{Arc_end_J}D01*\n"
    return txt


def ConvertLine(line):
    startX = str(int(line.start[0] * 10 ** 6))
    startY = str(int(line.start[1] * 10 ** 6))
    endX = str(int(line.end[0] * 10 ** 6))
    endY = str(int(line.end[1] * 10 ** 6))
    txt = f"X{startX}Y{startY}D02*\nX{endX}Y{endY}D01*\n"
    return txt


def ToGerberFile_Arc(arcs: List[ArcBase]):
    txt = ""
    for arc in arcs:
        txt += ConvertArc(arc)
    return txt


def ToGerberFile(lines):
    txt = "*\n%FSLAX26Y26*%\n%MOIN*%\n%ADD10C,0.007874*%\n%IPPOS*%\n%LNgko11.gbr*%\n%LPD*%\nG75*\nG54D10*\n"
    for pice in lines:
        if isinstance(pice, LinePice):
            txt += ConvertLine(pice)
        else:
            txt += ConvertArc(pice)
    return txt


def ToGerberFile2(lines, settings):
    statements = []
    arcs = []
    for pice in lines:
        if isinstance(pice, LinePice):
            coord = {"function": None, "x": '%f10' % pice.start[0], "y": '%f10' % pice.start[1], "i": None, "j": None, "op": "D02"}
            coordstmt = CoordStmt.from_dict(coord, settings)
            statements.append(coordstmt)
            coord = {"function": None, "x": '%f10' % pice.end[0], "y": '%f10' % pice.end[1], "i": None, "j": None, "op": "D01"}
            coordstmt = CoordStmt.from_dict(coord, settings)
            statements.append(coordstmt)
        else:
            arcs.append(pice)
    writestr = "*\n%FSLAX26Y26*%\n%MOIN*%\n%ADD10C,0.007874*%\n%IPPOS*%\n%LNgko11.gbr*%\n%LPD*%\nG75*\nG54D10*\n"
    for statement in statements:
        strr = statement.to_gerber(settings) + "\n"
        writestr += strr
    for arc in arcs:
        writestr += ConvertArc(arc)
    return writestr


if __name__ == "__main__":
    path1 = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile2\ALL-1W2308512"
    groupData = GroupData(path1, "ALL-1W2308512")
    pinBan = PinBan(groupData)
    sets = pinBan.Pin()
    filestr = ToGerberFile(sets, groupData.orders["ALL-1W2308512"].gbLayer("gko").settings)
    with open(f"rout", 'w') as f:
        f.write(filestr)
