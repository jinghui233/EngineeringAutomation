import math
from typing import List

from ProcessService.RoutLineProcess.LinePiceSet import LinePice, LineSet


class OrderSet:
    def __init__(self, sets: List[LineSet], orderNo: str, offset: tuple, stephdr):
        self.lines = []
        for set in sets:
            for line in set.GetLineSet():
                self.lines.append(line)
        self.orderNo = orderNo
        self.offset = offset
        self.offset = (stephdr.X_DATUM, stephdr.Y_DATUM)

    def AppleAffset(self, offset: tuple):
        for set in self.sets:
            for line in set.GetLineSet():
                line.start = (line.start[0] - offset[0], line.start[1] - offset[1])
                line.end = (line.end[0] - offset[0], line.end[1] - offset[1])

    def Trans_and_Rotate_new(self, dxy, angle):
        sets = []
        for set in self.sets:
            newLineSet = LineSet()
            for line in set.GetLineSet():
                start = self.__rotate(line.start, angle)
                end = self.__rotate(line.end, angle)
                start = self.__translation(start, dxy)
                end = self.__translation(end, dxy)
                newLineSet.addLine(LinePice(start, end, line.radius))
            sets.append(newLineSet)
        return OrderSet(sets, self.orderNo)

    def Translation(self, dxy: (float, float)):
        for set in self.sets:
            for line in set.GetLineSet():
                line.start = self.__translation(line.start, dxy)
                line.end = self.__translation(line.end, dxy)

    def Rotate(self, angle):
        for set in self.sets:
            for line in set.GetLineSet():
                line.start = self.__rotate(line.start, angle)
                line.end = self.__rotate(line.end, angle)

    def __translation(self, point: tuple, dxy: (float, float)) -> (float, float):
        return (point[0] + dxy[0], point[1] + dxy[1])

    def __rotate(self, point: tuple, angle) -> (float, float):
        point_x = point[0] * math.cos(angle / 180 * math.pi) + point[1] * math.sin(angle / 180 * math.pi)
        point_y = -point[0] * math.sin(angle / 180 * math.pi) + point[1] * math.cos(angle / 180 * math.pi)
        return (point_x, point_y)
