import os

from ProcessService.SupportFuncs.DataDownLoad.DataDownLoadBase import DataDownLoadBase


class DataDownLoad(DataDownLoadBase):
    @staticmethod
    def down_with_orderNo(orderNo):
        pass


failedGroupNos = ["JP-1W1146387", "JP-1W1148207", "JP-1W1721748-4", "JP-2W1396032", "ALL-2W2119894", "JP-2W2449079", "JP-2Y2460801"]
path = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile"
distPath = "D:\ProjectFile\EngineeringAutomation\TestData\GerberFile3"
orders = os.listdir(path)
totleNum = len(orders)
index = 0
for orderNo in orders:
    if orderNo == "00":
        continue
    index = index + 1
    if index < 114:
        continue
    groupNo = DataDownLoadBase.GetOrderInfo(orderNo)["GroupNo"]
    print(f"{totleNum}-{index}  downLoad:{groupNo}")
    if failedGroupNos.__contains__(groupNo):
        continue
    DataDownLoadBase.DownLoad_PbGc_File(groupNo, f"{distPath}")
