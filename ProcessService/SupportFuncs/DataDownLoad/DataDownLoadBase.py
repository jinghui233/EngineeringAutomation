import urllib.request
import urllib.parse
import json
from typing import List
from ProcessService.SupportFuncs.DataDownLoad.Verify import Verify
import zipfile
import os
import tarfile


class DataDownLoadBase:

    @staticmethod
    def GetOrderInfo(orderNo: str) -> dict:
        '''获取料号详细信息'''
        timeStamp, VerifyCode = Verify.GetVerifyCode()
        resp = urllib.request.urlopen(
            f"http://pcbapi.jiepei.com/api/Order/GetProOrderPageList?page=1&pageSize=1&search.orderNo={orderNo}&Timestamp={timeStamp}&VerifyCode={VerifyCode}")
        res = json.loads(resp.read().decode("UTF-8"))
        return res["data"]["Item1"][0]

    @staticmethod
    def GetOrderInfos(groupNo: str) -> List[dict]:
        '''根据拼版号获取所有子料号详细信息'''
        timeStamp, VerifyCode = Verify.GetVerifyCode()
        resp = urllib.request.urlopen(
            f"http://pcbapi.jiepei.com/api/Order/GetProOrderPageList?page=1&pageSize=99&search.searchGroupNo={groupNo}&Timestamp={timeStamp}&VerifyCode={VerifyCode}")
        res = json.loads(resp.read().decode("UTF-8"))
        return res["data"]["Item1"]

    @staticmethod
    def DownLoadGc(bussinessOrderNo: str, path: str, saveName: str):
        '''下载工程文件'''
        timeStamp, VerifyCode = Verify.GetVerifyCode()
        url = f"http://pcbapi.jiepei.com/api/Cam/DownloadGcFile?UserName=sudong&BusinessOrderNo={bussinessOrderNo}&Timestamp={timeStamp}&VerifyCode={VerifyCode}"
        resp = urllib.request.urlopen(url)
        data = resp.read()
        if len(data) < 1024:
            return False
        with open(f"{path}/{saveName}.zip", "wb") as f:
            f.write(data)
        return True

    @staticmethod
    def DownLoadPb(groupNo: str, path: str, saveName: str):
        '''下载拼版文件'''
        timeStamp, VerifyCode = Verify.GetVerifyCode()
        url = f"http://pcbapi.jiepei.com/api/Cam/DownloadPbFile?UserName=sudong&PinBanNos={groupNo}&Timestamp={timeStamp}&VerifyCode={VerifyCode}&TypeKey=CESHI"
        resp = urllib.request.urlopen(url)
        data = resp.read()
        if len(data) < 1024:
            return False
        with open(f"{path}/{saveName}.tgz", "wb") as f:
            f.write(data)
        return True

    @staticmethod
    def DownLoadLd(groupNo: str, path: str, saveName: str):
        '''下载锣带文件'''
        timeStamp, VerifyCode = Verify.GetVerifyCode()
        url = f"http://pcbapi.jiepei.com/api/Cam/DownloadPbFile?UserName=sudong&PinBanNos={groupNo}&Timestamp={timeStamp}&VerifyCode={VerifyCode}&TypeKey=LUODAIMAKE"
        resp = urllib.request.urlopen(url)
        data = resp.read()
        if len(data) < 1024:
            return False
        with open(f"{path}/{saveName}.zip", "wb") as f:
            f.write(data)
        return True

    @staticmethod
    def Unzip(srcPath: str, distPath: str):
        '''解压zip文件'''
        with zipfile.ZipFile(srcPath) as zf:
            zf.extractall(distPath)

    @staticmethod
    def Untgz(srcPath: str, distPath: str):
        '''解压tgz文件'''
        tar = tarfile.open(srcPath)
        tar.extractall(distPath)

    @staticmethod
    def DownLoadGcFile_with_UnZip(orderNo: str, path: str):
        '''下载工程文件并解压'''
        orderInfo = DataDownLoadBase.GetOrderInfo(orderNo)
        if not DataDownLoadBase.DownLoadGc(orderInfo["BusinessOrderNo"], f"{path}", orderNo):
            return False
        DataDownLoadBase.Unzip(f"{path}/{orderNo}.zip", path)
        os.remove(f"{path}/{orderNo}.zip")
        if os.path.exists(f"{path}/ok"):
            subdirs = os.listdir(f"{path}/ok")
            for subdir in subdirs:
                if subdir.split(".")[-1] == "tgz":
                    DataDownLoadBase.Untgz(f"{path}/ok/{subdir}", f"{path}/ok")
                    os.remove(f"{path}/ok/{subdir}")
        else:
            nextdirs = os.listdir(f"{path}")
            if nextdirs.__len__() > 0:
                if os.path.exists(f"{path}/{nextdirs[0]}/ok"):
                    subdirs = os.listdir(f"{path}/{nextdirs[0]}/ok")
                    for subdir in subdirs:
                        if subdir.split(".")[-1] == "tgz":
                            DataDownLoadBase.Untgz(f"{path}/{nextdirs[0]}/ok/{subdir}", f"{path}/{nextdirs[0]}/ok")
                            os.remove(f"{path}/{nextdirs[0]}/ok/{subdir}")
        return True

    @staticmethod
    def DownLoadPbFile_with_UnZip(groupNo: str, path: str):
        '''下载拼版文件并解压'''
        if not DataDownLoadBase.DownLoadPb(groupNo, f"{path}", groupNo):
            return False
        DataDownLoadBase.Untgz(f"{path}/{groupNo}.tgz", path)
        os.remove(f"{path}/{groupNo}.tgz")
        return True
    @staticmethod
    def DownLoadLdFile_with_UnZip(groupNo: str, path: str):
        '''下载锣带文件并解压'''
        if not DataDownLoadBase.DownLoadLd(groupNo, f"{path}", groupNo):
            return False
        DataDownLoadBase.Unzip(f"{path}/{groupNo}.zip", path)
        os.remove(f"{path}/{groupNo}.zip")
        return True
    @staticmethod
    def DownLoad_PbGc_File(groupNo: str, path: str):
        '''下载拼版和子工程文件'''
        if os.path.exists(f"{path}/{groupNo}"):
            return False
        if not os.path.exists(f"{path}/{groupNo}"):
            os.makedirs(f"{path}/{groupNo}")
        if not DataDownLoadBase.DownLoadPbFile_with_UnZip(groupNo, f"{path}/{groupNo}"):
            os.rmdir(f"{path}/{groupNo}")
            return False
        orderInfos = DataDownLoadBase.GetOrderInfos(groupNo)
        for orderInfo in orderInfos:
            orderNo = orderInfo["OrderNo"]
            if not os.path.exists(f"{path}/{groupNo}/Orders/{orderNo}"):
                os.makedirs(f"{path}/{groupNo}/Orders/{orderNo}")
            DataDownLoadBase.DownLoadGcFile_with_UnZip(orderNo, f"{path}/{groupNo}/Orders/{orderNo}")
        return True


if __name__ == "__main__":
    DataDownLoadBase.DownLoad_PbGc_File("JP-2W2548417", "gerberfile")
