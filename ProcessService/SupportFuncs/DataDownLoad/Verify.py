import hashlib
import time


class Verify:
    @staticmethod
    def GetTimeStamp() -> str:
        return str(round(time.time() * 1000))

    @staticmethod
    def GetMD5Hash(timpStamp: str, passCode: str = 'kw3j5k32ur38rnerkKJHk83') -> str:
        m = hashlib.md5()
        m.update((passCode + timpStamp).encode(encoding='utf-8'))
        return m.hexdigest()

    @staticmethod
    def GetVerifyCode():
        timeStamp = Verify.GetTimeStamp()
        return timeStamp, Verify.GetMD5Hash(timeStamp)
