import math
import gerber
from gerber.primitives import Circle

def find_all_line(gko):
    start_points = []
    end_points = []
    gko_lines = []
    for j in range(len(gko.primitives)):
        if type(gko.primitives[j]) == gerber.primitives.Line:
            start_points.append(gko.primitives[j].start)
            end_points.append(gko.primitives[j].end)
    for h in range(len(start_points)):
        gko_lines.append([start_points[h], end_points[h]])
    return gko_lines


def drl_find(drl, lines):
    new_circles = []
    all_circles = []
    for drl in drl.primitives:
        if type(drl) == gerber.primitives.Circle:
            all_circles.append([drl.position, drl.radius])
    for circle1 in all_circles:
        m = 0
        for circle2 in all_circles:
            if (circle1 != circle2) and (2*min(circle1[1], circle2[1]) < (math.sqrt((circle1[0][1] - circle2[0][1]) ** 2 + (circle1[0][0] - circle2[0][0]) ** 2)) < 4 * min(circle1[1], circle2[1])):
                m = 1
        if m == 0:
            circle1.append('q')
        else:
            circle1.append('l')
    for i in range((len(all_circles) - 1), -1, -1):
        if all_circles[i][-1] == 'q':
            all_circles.pop(i)
        else:
            all_circles[i].pop()
    for circle in all_circles:
        circle.append('o')
    label = 0
    a = 1
    while a == 1:
        for i in range(len(all_circles)):
            for j in range(len(all_circles)):
                if i != j and type(all_circles[i][-1]) != int and type(all_circles[j][-1]) != int:
                    if math.sqrt((all_circles[i][0][1] - all_circles[j][0][1]) ** 2 + (all_circles[i][0][0] - all_circles[j][0][0]) ** 2) < 4 * min(all_circles[i][1], all_circles[j][1]):
                        all_circles[i][-1] = label
                        all_circles[j][-1] = label
                        continue
                elif i != j and type(all_circles[i][-1]) == int and type(all_circles[j][-1]) != int:
                    if 2*min(all_circles[i][1], all_circles[j][1]) < math.sqrt((all_circles[i][0][1] - all_circles[j][0][1]) ** 2 + (all_circles[i][0][0] - all_circles[j][0][0]) ** 2) < 4 * min(all_circles[i][1], all_circles[j][1]):
                        all_circles[j][-1] = all_circles[i][-1]
                        continue
                elif i != j and type(all_circles[i][-1]) == int and type(all_circles[j][-1]) == int:
                    if 2*min(all_circles[i][1], all_circles[j][1]) < math.sqrt((all_circles[i][0][1] - all_circles[j][0][1]) ** 2 + (all_circles[i][0][0] - all_circles[j][0][0]) ** 2) < 4 * min(all_circles[i][1], all_circles[j][1]):
                        if all_circles[j][-1] < all_circles[i][-1]:
                            all_circles[i][-1] = all_circles[j][-1]
                        elif all_circles[j][-1] > all_circles[i][-1]:
                            all_circles[j][-1] = all_circles[i][-1]
                        else:
                            pass
            label += 1
        labs = []
        for circle in all_circles:
            labs.append(circle[-1])
        if 'o' in labs:
            a = 1
        else:
            a = 0
    labels = []
    for circle in all_circles:
        labels.append(circle[-1])
    labels = list(set(labels))
    for lab in labels:
        z_circles = []
        for circle in all_circles:
            if circle[-1] == lab:
                z_circles.append(circle)
        size_z = len(z_circles)
        while len(z_circles) > 3:
            z_x = []
            z_y = []
            for z in z_circles:
                z_x.append(z[0][0])
                z_y.append(z[0][1])
            for x in range((len(z_x) - 1), 0, -1):
                if z_x[x] == z_x[x - 1]:
                    z_x.pop(x)
            for y in range((len(z_y) - 1), 0, -1):
                if z_y[y] == z_y[y - 1]:
                    z_y.pop(y)
            if len(z_x) > 3:
                for z in range((len(z_circles) - 1), -1, -1):
                    if z_circles[z][0][0] == min(z_x) or z_circles[z][0][0] == max(z_x):
                        z_circles.pop(z)
            elif len(z_y) > 3:
                for z in range((len(z_circles) - 1), -1, -1):
                    if z_circles[z][0][1] == min(z_y) or z_circles[z][0][1] == max(z_y):
                        z_circles.pop(z)
            else:
                break
        size_z1 = len(z_circles)
        if size_z > size_z1 and len(z_circles) == 2:
            new_circles.append([((z_circles[0][0][0]+z_circles[1][0][0])/2, (z_circles[0][0][1]+z_circles[1][0][1])/2), z_circles[1][1]])
        if len(z_circles) == 3:
            z_circles[1].pop(-1)
            new_circles.append(z_circles[1])
        for circle in new_circles:
            a = 0
            for line in lines:
                s_p = line[0]
                e_p = line[1]
                if s_p[0] == e_p[0]:
                    if (abs(circle[0][0]-s_p[0]) < 1.5*circle[1]) and (min(s_p[1], e_p[1])<circle[0][1]<max(s_p[1], e_p[1])):
                        a = 1
                else:
                    k = (e_p[1]-s_p[1])/(e_p[0]-s_p[0])
                    b = e_p[1]-k*e_p[0]
                    d = abs(k*circle[0][0]-circle[0][1]+b)/math.sqrt(k**2+1)
                    if d<1.5*circle[1] and min(s_p[0], e_p[0])<circle[0][0]<max(s_p[0], e_p[0]):
                        a = 1
            if a == 1:
                pass
            else:
                new_circles.remove(circle)
    primitives_new = []
    for z in new_circles:
        primitives_new.append(Circle(z[0], z[1] * 2))
    return primitives_new


def ToGerberFile(circles):
    radius = []
    for circle in circles:
        radius.append(circle[1])
    radius.sort()
    for i in range(len(radius)-1, 0, -1):
        if radius[i] == radius[i-1]:
            radius.pop(i)
    str_c = []
    circle_news = []
    for c in range(len(radius)):
        n = c+10
        r = radius[c]
        str_c.append([n, 2*r])
        z_circles = []
        for circle in circles:
            if circle[1] == r:
                z_circles.append(circle)
        circle_news.append([z_circles, n])
    writestr = "\n%FSLAX26Y26*%\n%MOIN*%\n"
    for s in str_c:
        writestr += f"%ADD{s[0]}C,{s[1]}*%\n"
    writestr += "%IPPOS*%\n%LNdrl*%\n%LPD*%\nG75*\n"
    for i in range(len(circle_news)):
        writestr += f"G54D{circle_news[i][1]}*\n"
        for circle in circle_news[i][0]:
            position_x = circle[0][0]
            position_y = circle[0][1]
            position_X = str(int(position_x * 10 ** 6))
            position_Y = str(int(position_y * 10 ** 6))
            writestr += 'X' + position_X + 'Y' + position_Y + 'D03*\n'
    return writestr
