import os
from typing import List

import gerber
from ProcessService.RoutLineProcess.LinePiceSet import LinePice
from gerber.primitives import Line as Gbt_Line
from gerber.cam import CamFile


def findLayerName(fileName: str):
    readlayers = ["gko", "drl", "gbs", "gbo", "gbl", "gts", "gto", "gtl"]
    fileNamesp = fileName.split(".")
    if len(fileNamesp) > 1:
        fileName = fileNamesp[-1]
    fileName = fileName.lower()
    if readlayers.__contains__(fileName):
        return fileName
    return None


def dataPrepar(path):
    readlayers = ["gko", "drl", "gbs", "gbo", "gbl", "gts", "gto", "gtl"]
    layers = os.listdir(path)
    gerbers = {}
    layerNames = []
    vCutData = ""
    for layer in layers:
        layername = findLayerName(layer)
        if layername is None and not os.path.isdir(f"{path}\\{layer}"):
            continue
        if os.path.isdir(f"{path}\\{layer}"):
            vCutpath = f"{path}\\{layer}\\steps\\set\\layers\\v-cut\\features"
            if os.path.exists(vCutpath):
                vCutData = open(vCutpath, "r").read()
            vCutpath = f"{path}\\{layer}\\steps\\edit\\layers\\v-cut\\features"
            if os.path.exists(vCutpath):
                vCutData = open(vCutpath, "r").read()
        else:
            layerNames.append(layername)
            with open(f"{path}\\{layer}", "rU") as fp:
                data = fp.read()
                gerbers[layername] = data
    return gerbers, layerNames, vCutData


class OrderData:
    def __init__(self, path: str, orderNo: str):
        self.orderNo = orderNo
        self.filePath = f"{path}/ok"
        if not os.path.exists(self.filePath):
            self.filePath = f"{path}/{os.listdir(path)[0]}/ok"
        self.__gerbers, self.layerNames, self.vCutData = dataPrepar(self.filePath)
        self.__gbLayers = {}
        self.hasGKO = self.layerNames.__contains__("gko")
        self.ratek = 300
        if self.hasGKO:
            self.__gbLayers["gko"] = gerber.loads(self.__gerbers["gko"], "gko")
            self.settings = self.__gbLayers["gko"].settings
            self.gkobounds = self.__gbLayers["gko"].bounds
            self.offset = (self.gkobounds[0][0], self.gkobounds[1][0])
            self.width, self.height = self.gkobounds[0][1] - self.gkobounds[0][0], self.gkobounds[1][1] - self.gkobounds[1][0]
            self.__gkoLines = []
        self.loadv_cut(self.vCutData)

    def LoadStephdr(self, path):
        stephdr = None
        if os.path.exists(f"{path}/{self.orderNo}.edit"):
            stephdr = open(f"{path}/{self.orderNo}.edit/stephdr", "r").read()
        if os.path.exists(f"{path}/{self.orderNo}.set"):
            stephdr = open(f"{path}/{self.orderNo}.set/stephdr", "r").read()
        if os.path.exists(f"{path}/edit"):
            stephdr = open(f"{path}/edit/stephdr", "r").read()
        if os.path.exists(f"{path}/set"):
            stephdr = open(f"{path}/set/stephdr", "r").read()
        self.stephdr = stephdr

    def loadv_cut(self, vCutData: str):
        self.vCutLines = []
        for cur_line in vCutData.split("\n"):
            if cur_line.__contains__("L "):
                cur_linesp = cur_line.split(" ")
                startPoint = (float(cur_linesp[1]), float(cur_linesp[2]))
                endPoint = (float(cur_linesp[3]), float(cur_linesp[4]))
                self.vCutLines.append(LinePice(startPoint, endPoint, 0.007874))

    def gbLayer(self, layerName: str, unit: str = "inch") -> CamFile:
        if layerName not in self.layerNames:
            return None
        if layerName in self.layerNames:
            self.__gbLayers[layerName] = gerber.loads(self.__gerbers[layerName], layerName)
        if unit == "inch":
            self.__gbLayers[layerName].to_inch()
        return self.__gbLayers[layerName]

    @property
    def gkoLines(self) -> List[LinePice]:
        if self.layerNames.__contains__("gko"):
            if len(self.__gkoLines) > 0:
                return self.__gkoLines
            for primitive in self.__gbLayers["gko"].primitives:
                if not isinstance(primitive, Gbt_Line):
                    continue
                self.__gkoLines.append(LinePice(primitive.start, primitive.end, primitive.aperture.radius))
            return self.__gkoLines

    @gkoLines.setter
    def gkoLines(self, value: List[LinePice]):
        self.__gkoLines = value


class GroupData:
    def __init__(self, path: str, groupNo: str):
        self.groupNo = groupNo
        self.stephdr = open(f"{path}/{self.groupNo}/steps/panel/stephdr").read()
        self.profile = open(f"{path}/{self.groupNo}/steps/panel/profile").read()
        orderDirs = os.listdir(f"{path}/Orders")
        self.orders = {}
        for orderDir in orderDirs:
            orderData = OrderData(f"{path}/Orders/{orderDir}", orderDir)
            self.settings = orderData.settings
            self.orders[orderDir] = orderData
            orderData.LoadStephdr(f"{path}/{self.groupNo}/steps")
        self.load_profileData(self.profile)

    def load_profileData(self, proFile_data: str):
        self.lines = []
        prePoint = (0, 0)
        for cur_line in proFile_data.split("\n"):
            if cur_line.__contains__("OB"):
                cur_linesp = cur_line.split(" ")
                curPoint = (float(cur_linesp[1]), float(cur_linesp[2]))
                prePoint = curPoint
            if cur_line.__contains__("OS"):
                cur_linesp = cur_line.split(" ")
                curPoint = (float(cur_linesp[1]), float(cur_linesp[2]))
                self.lines.append(LinePice(prePoint, curPoint, 0.007874))
                prePoint = curPoint
